<?php
/*
Plugin Name: countdown
Plugin URI: http://www.myoffersarena.in/
Description: Display a Countdown timer for ad expiration date 
Version: 1.0.0
Author: DigitalSense
Author URI: http://www.digitalsense.in/
Short Name: countdown
Plugin update URI: countdown
*/


    function countdown_plugin_info() {
        return array(
             'name'        => 'countdown'
            ,'version'     => '1.0.1'
            ,'description' => 'Countdown Pluin by DigitalSense'
            ,'author_name' => 'DigitalSense'
            ,'author_url'  => 'http://www.digitalsense.in/'
            ,'locations'   => array()
        );
    }

    function countdown_install() {
    }

    function countdown_uninstall() {
    }

    function countdown_splash() { ?> 


    <div class="box-timer">
             <div class="countbox_<?php echo osc_item_id(); ?> timer-grid"></div>
                    </div>
                    <script type="text/javascript">
var dthen<?php echo osc_item_id(); ?> = new Date("<?php echo date('m/d/Y H:m:s',strtotime(osc_item_dt_expiration())); ?>");
    start = new Date("<?php echo date('m/d/Y H:m:s'); ?>");
    start_date = Date.parse(start);
    var dnow<?php echo osc_item_id(); ?> = new Date(start_date);
    if (CountStepper > 0)
    ddiff = new Date((dnow<?php echo osc_item_id(); ?>) - (dthen<?php echo osc_item_id(); ?>));
    else
    ddiff = new Date((dthen<?php echo osc_item_id(); ?>) - (dnow<?php echo osc_item_id(); ?>));
    gsecs<?php echo osc_item_id(); ?> = Math.floor(ddiff.valueOf() / 1000);
    
    
    CountBack_slider(gsecs<?php echo osc_item_id(); ?>, "countbox_<?php echo osc_item_id(); ?>", 1);
        
        </script>
        <?php
    }

      function ex_load_scripts() {
       osc_register_script('Countdown', osc_base_url() . 'oc-content/plugins/counter/js/countdown.js', 'jquery');
       osc_enqueue_script('Countdown');
       osc_enqueue_style('countdowncss', osc_base_url() . 'oc-content/plugins/counter/css/countdown.css');
   }


    /**
     * ADD HOOKS
     */
    osc_register_plugin(osc_plugin_path(__FILE__), 'countdown_install');
    osc_add_hook(osc_plugin_path(__FILE__)."_uninstall", 'countdown_uninstall');

    osc_add_hook('cnt_clr', 'countdown_splash');
       osc_add_hook('init', 'ex_load_scripts');

?>
