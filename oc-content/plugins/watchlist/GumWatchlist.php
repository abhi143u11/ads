<?php

class GumWatchlist {

    public function __construct()
    {
        // plugin watchlist
        if(function_exists('watchlist')) {
            osc_add_hook('ajax_watchlist',      array(&$this, 'watchlist'));
            osc_register_script('gum-watchlist', osc_current_web_theme_url() . 'class/watchlist/watchlist.js', 'jquery');
            osc_register_script('gum-watchlist-search', osc_current_web_theme_url() . 'class/watchlist/watchlist_search.js', 'jquery');

            if(osc_is_search_page()) {
                osc_enqueue_script('gum-watchlist-search');
            }else if(osc_is_list_items()){
                osc_enqueue_script('gum-watchlist');
            } 

            osc_add_hook('init', function() {
                osc_remove_hook('user_menu', 'watchlist_user_menu');
                osc_add_hook('user_menu', function() {
                    $active = '';
                    if(Params::getParam('file')=='watchlist/watchlist.php') {
                        $active = 'active';
                    }
                    echo '<li class="'.$active.'" ><a href="' . osc_render_file_url( 'watchlist/watchlist.php') . '" >' . __('Watchlist', 'gum') . '</a></li>';
                });
            });

            osc_add_hook('custom_controller', function() {
                if(Params::getParam('file')=='watchlist/watchlist.php') {
                    View::newInstance()->_exportVariableToView('file', Params::getParam('file'));
                    osc_current_web_theme_path('class/watchlist/watchlist.php');
                    exit;
                }
            });

            // remove javascript
            if(osc_version()<311) {
                osc_remove_hook('footer', 'watchlist_footer');
            } else {
                osc_remove_hook('scripts_loaded', 'watchlist_scripts_loaded');
                osc_remove_script('watchlist');
                osc_add_hook('scripts_loaded', function() {
                    echo '<!-- Watchlist js -->';
                    echo '<script type="text/javascript">';
                    echo 'var watchlist_url = "' . osc_ajax_hook_url('watchlist') . '";';
                    echo '</script>';
                    echo '<!-- Watchlist js end -->';
                });
            }
        }
    }

    function watchlist() {
        if (Params::getParam('id') != '') {
            $id = Params::getParam('id');
            if (osc_is_web_user_logged_in()) {
                //check if the item is not already in the watchlist
                $conn = getConnection();
                $detail = $conn->osc_dbFetchResult("SELECT * FROM %st_item_watchlist WHERE fk_i_item_id = %d and fk_i_user_id = %d", DB_TABLE_PREFIX, $id, osc_logged_user_id());

                //If nothing returned then we can process
                if (!isset($detail['fk_i_item_id'])) {
                    $conn = getConnection();
                    $conn->osc_dbExec("INSERT INTO %st_item_watchlist (fk_i_item_id, fk_i_user_id) VALUES (%d, '%d')", DB_TABLE_PREFIX, $id, osc_logged_user_id());
                    echo json_encode(array(
                        'href' => osc_base_url(true)."?page=custom&file=watchlist/watchlist.php",
                        'text' => __('View your watchlist', 'gum')
                    ));
                } else {
                    //Already in watchlist !
                    echo json_encode(array(
                        'href' => osc_base_url(true)."?page=custom&file=watchlist/watchlist.php",
                        'text' => __('View your watchlist', 'gum')
                    ));
                }
            } else {
                //error user is not login in
                echo json_encode(array(
                    'href' => '',
                    'text' => ''
                ));
//                echo '<a href="' . osc_user_login_url() . '">' . __('Please login', 'watchlist') . '</a>';
            }
        }
    }
}