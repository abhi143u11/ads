jQuery(document).ready(function($) {
    $(".watchlist").click(function() {
        var id = $(this).attr("id");
        var dataString = 'id='+ id ;
        var that = this;
        $.ajax({
            type: "POST",
            url: watchlist_url,
            data: dataString,
            cache: false,
            dataType: 'json',
            success: function(data) { 
                $(that).html(data.text);
                $(that).prop('href', data.href);
            }
        });
    });
});