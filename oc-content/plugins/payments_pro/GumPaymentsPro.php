<?php


class GumPaymentsPro {

    public function __construct()
    {
        // plugin watchlist
        if(function_exists('payment_pro_form')) {

            //user menu
            osc_remove_hook('user_menu', 'payment_pro_user_menu');
            osc_add_hook('user_menu', array(&$this,'payment_pro_user_menu'));

            osc_remove_hook('item_form', 'payment_pro_form');
            osc_remove_hook('item_edit', 'payment_pro_form');
            // add hooks
            osc_add_hook('item_form', array(&$this,'payment_pro_form'));
            osc_add_hook('item_edit', array(&$this,'payment_pro_form'));

            // done page
            osc_add_hook('init', function() {
                if(Params::getParam('route')=='payment-pro-done') {
                    osc_current_web_theme_path('class/payments_pro/user/done.php');
                    exit();
                }
            });
            // payment-pro-user-menu
            osc_add_hook('init', function() {
                if(Params::getParam('route')=='payment-pro-user-menu') {
                    osc_current_web_theme_path('class/payments_pro/user/menu.php');
                    exit();
                }
            });
            // payment-pro-user-packs
            osc_add_hook('init', function() {
                if(Params::getParam('route')=='payment-pro-user-packs') {
                    osc_current_web_theme_path('class/payments_pro/user/packs.php');
                    exit();
                }
            });
        }
    }

    // item add / item edit form
    function payment_pro_form($category_id = null, $item_id = null) {
        $payment_pro_premium_fee = ModelPaymentPro::newInstance()->getPremiumPrice($category_id);
        $payment_pro_publish_fee = ModelPaymentPro::newInstance()->getPublishPrice($category_id);
        if($item_id==null) { // POST
            if((isset($payment_pro_publish_fee['price']) && $payment_pro_publish_fee['price']>0) || (isset($payment_pro_premium_fee['price']) && $payment_pro_premium_fee['price']>0)) {
                require 'user/item_edit.php';
            }
        } else {
            $item = Item::newInstance()->findByPrimaryKey($item_id);
            if($item['b_premium']!=1) {
                require 'user/item_edit.php';
            }
        }
    }

    function payment_pro_user_menu() {
        $menu_active = '';
        if(Params::getParam('route')=='payment-pro-user-menu') {
            $menu_active = 'active';
        }
        $packs_active = '';
        if(Params::getParam('route')=='payment-pro-user-packs') {
            $packs_active = 'active';
        }
        echo '<li class="opt_payment '.$menu_active.'" ><a href="'.osc_route_url('payment-pro-user-menu').'" >'.__("Listings payment status", 'payment_pro').'</a></li>' ;
        if(osc_get_preference('allow_wallet', 'payment_pro')==1) {
            echo '<li class="opt_payment_pro_pack '.$packs_active.'" ><a href="'.osc_route_url('payment-pro-user-packs').'" >'.__("Buy credit for payments", 'payment_pro').'</a></li>' ;
        }
    }
}