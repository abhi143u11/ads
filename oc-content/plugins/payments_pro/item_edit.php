<div class="row">
    <div class="box-border">
        <h2 class="h2-custom"><?php _e('Publish options', 'payment_pro') ; ?></h2>
        <?php if(isset($payment_pro_premium_fee['price']) && $payment_pro_premium_fee['price']>0) { ?>
            <div class="controls checkbox">
                <input type="checkbox" name="payment_pro_make_premium" id="payment_pro_make_premium" value="1" checked="yes" /> <label><?php printf(__('Make this ad premium (+%s)', 'payment_pro'), osc_format_price($payment_pro_premium_fee['price']*1000000, osc_get_preference('currency', 'payment_pro'))); ?></label>
          </div>
        <?php };
        if(isset($payment_pro_publish_fee['price']) && $payment_pro_publish_fee['price']>0) { ?>
        <div class="controls checkbox">
            <label><?php printf(__('Publishing this ad costs %s', 'payment_pro'), osc_format_price($payment_pro_publish_fee['price']*1000000, osc_get_preference('currency', 'payment_pro'))); ?></label>
        </div>
        <?php }; ?>
    </div>
</div>