<?php if( osc_comments_enabled() ) { ?>
    <div id="comments">
            <h2><i class="fa fa-comments"></i> 
            <?php if ( osc_count_item_comments () <= 1 ) { ?>
                <?php echo osc_count_item_comments(); ?> <?php _e('Comment', 'ads'); ?>
            <?php } else { ?>
                <?php echo osc_count_item_comments(); ?> <?php _e('Comments', 'ads'); ?>
            <?php } ?>
            </h2>
        <?php if( osc_count_item_comments() >= 1 ) { ?>
            <div class="comments_list">
                <?php while ( osc_has_item_comments() ) { ?>
                    <div class="comment clearfix">
                        <div class="pull-left avatar">
                        	<?php dd_commentpic();?>
                        </div>
                        <div class="pull-left message">
                            <?php /*?><h5><a href="<?php echo osc_user_public_profile_url(osc_comment_user_id()); ?>"><?php echo osc_comment_author_name(); ?></a> <small><?php echo osc_format_date( osc_comment_pub_date() ); ?></small></h5><?php */?>
                            <?php if( osc_comment_user_id() != null ) { ?>
                            <h5><a href="<?php echo osc_user_public_profile_url(osc_comment_user_id()); ?>"><?php echo osc_comment_author_name(); ?></a> <small><?php echo osc_format_date( osc_comment_pub_date() ); ?></small></h5>
                            <?php } else { ?>
                            <h5><?php echo osc_comment_author_name(); ?> <small><?php echo osc_format_date( osc_comment_pub_date() ); ?></small></h5>
                            <?php } ?>
                            <p><?php echo nl2br( osc_comment_body() ); ?></p>
                            <?php if ( osc_comment_user_id() && (osc_comment_user_id() == osc_logged_user_id()) ) { ?>
                            <p><a rel="nofollow" class="btn-danger" href="<?php echo osc_delete_comment_url(); ?>" title="<?php _e('Delete your comment', 'ads'); ?>"><i class="fa fa-times"></i> <?php _e('Delete', 'ads'); ?></a></p>
                        <?php } ?>
                        </div>
                    </div><!-- Comment -->
                <?php } ?>
                <div class="paginate" style="text-align: right;">
                    <?php echo osc_comments_pagination(); ?>
                </div>
            </div><!-- Comments List -->
        <?php } else { ?>
        <div class="no-comments">
            <?php _e('No comments', 'ads'); ?>
        </div>
        <?php } ?>
        <?php if( osc_reg_user_post_comments () && osc_is_web_user_logged_in() || !osc_reg_user_post_comments() ) { ?>
        <!--<ul id="comment_error_list"></ul>-->
        <?php CommentForm::js_validation(); ?>
        <div class="post-comments">
            <div class="comment clearfix">
                    <div class="pull-left avatar">
                        <?php if (function_exists("profile_picture_show")) { ?>
							<?php current_user_picture(); ?>
                        <?php } else { ?>
                            <img class="img-responsive" src="http://www.gravatar.com/avatar/<?php echo md5( strtolower( trim( osc_logged_user_email() ) ) ); ?>?s=60&d=<?php echo osc_current_web_theme_url('images/user-default.jpg') ; ?>" />
                        <?php } ?>
                    </div>
                    <div class="pull-left message">
                    	<ul id="comment_error_list"></ul>
                        <form action="<?php echo osc_base_url(true); ?>" method="post" name="comment_form" id="comment_form">
                            <input type="hidden" name="action" value="add_comment" />
                            <input type="hidden" name="page" value="item" />
                            <input type="hidden" name="id" value="<?php echo osc_item_id(); ?>" />
                            <?php if(osc_is_web_user_logged_in()) { ?>
                                <input type="hidden" name="authorName" value="<?php echo osc_esc_html( osc_logged_user_name() ); ?>" />
                                <input type="hidden" name="authorEmail" value="<?php echo osc_logged_user_email();?>" />
                            <?php } else { ?>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="<?php _e('Your name', 'ads'); ?>" value="" name="authorName" id="authorName">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="<?php _e('Your e-mail', 'ads'); ?>" value="" name="authorEmail" id="authorEmail">
                                </div>
                            <?php } ?>
                            <h5><?php echo osc_logged_user_name(); ?></h5>
                            <div class="form-group">
                               <textarea class="form-control" placeholder="<?php _e('Leave your comment (spam and offensive messages will be removed)', 'ads'); ?>" rows="5" name="body" id="body"></textarea>
                            </div>
                            <div class="actions">
                                <button class="btn btn-success" type="submit"><?php _e('Send', 'ads'); ?></button>
                            </div>
                        </form>
                    </div>
                </div><!-- Comment -->
        </div><!-- Comments Container -->
        <?php } else { ?>
        <div class="panel-footer"><?php _e("You must be logged in to comment", 'ads'); ?>. <a href="<?php echo osc_user_login_url(); ?>"><?php _e("Login", 'ads'); ?></a> <?php _e("or", 'ads'); ?> <a href="<?php echo osc_register_account_url(); ?>"><?php _e("Register", 'ads'); ?></a></div>
        <?php } ?>    
    </div>
<?php } ?>
<!-- Comments End -->