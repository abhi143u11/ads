<!DOCTYPE html>
<html dir="ltr" lang="<?php echo str_replace('_', '-', osc_current_user_locale()); ?>">
<head>
<meta charset="utf-8">
<title><?php echo meta_title(); ?></title>
<?php if( meta_description() != '' ) { ?>
<meta name="description" content="<?php echo osc_esc_html(meta_description()); ?>" />
<?php } ?>
<?php if( meta_keywords() != '' ) { ?>
<meta name="keywords" content="<?php echo osc_esc_html(meta_keywords()); ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php if(osc_is_ad_page()) { ?>
<!-- Open Graph Tags -->
<meta property="og:title" content="<?php echo osc_item_title(); ?>" />
<meta property="og:image" content="<?php if( osc_count_item_resources() ) { ?><?php echo osc_resource_url(); ?><?php } ?>" />
<meta property="og:description" content="<?php echo osc_highlight( strip_tags( osc_item_description() ), 120 ) ; ?>" />
<!-- /Open Graph Tags -->
<?php } ?>
<?php if( osc_get_preference('google_webmaster', 'ads_theme') !== '0') { ?>
<meta name="google-site-verification" content="<?php echo osc_get_preference("g_webmaster", "ads_theme"); ?>" />
<?php } ?>
<?php if( osc_get_canonical() != '' ) { ?><link rel="canonical" href="<?php echo osc_get_canonical(); ?>"/><?php } ?>
<link rel="shortcut icon" href="<?php echo osc_current_web_theme_url('favicon.ico'); ?>">
<link href="<?php echo osc_current_web_theme_url('css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo osc_current_web_theme_url('css/simple-line-icons.css'); ?>?ver=<?php echo $info['version']; ?>" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/simple-line-icons.css" media="all">

<link rel="stylesheet" type="text/css" href="<?php echo osc_current_web_theme_url('css/owl.theme.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo osc_current_web_theme_url('css/jquery.bxslider.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo osc_current_web_theme_url('css/jquery.mobile-menu.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo osc_current_web_theme_url('css/style1.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo osc_current_web_theme_url('css/revslider.css'); ?>">


<?php $getColorScheme = ads_def_color(); ?>
<?php osc_enqueue_style(''.$getColorScheme.'green', osc_current_web_theme_url('css/'.$getColorScheme.'.css')); ?>
<?php if(osc_get_preference('anim', 'ads_theme') !='0') { ?>
<link href="<?php echo osc_current_web_theme_url('css/animate.min.css'); ?>" rel="stylesheet" type="text/css" />
<?php } ?>
<link rel="stylesheet" href="<?php echo osc_current_web_theme_url('css/owl.carousel.css'); ?>" type="text/css" media="screen" />
<link href="<?php echo osc_current_web_theme_url('css/responsivefix.css'); ?>" rel="stylesheet" type="text/css" />
<?php 
	osc_enqueue_script('jquery');
	osc_enqueue_style('jquery-ui', osc_assets_url('css/jquery-ui/jquery-ui.css'));
	osc_enqueue_script('jquery-ui');
?>
<!-- Header Hook -->
<?php osc_run_hook('header'); ?>
<!-- /Header Hook -->
<?php if(!osc_is_publish_page() || !osc_is_edit_page()) { ?>
<script type="text/javascript" src="<?php echo osc_current_web_theme_url('js/owl.carousel.min.js'); ?>"></script>
<?php } ?>
<?php if(osc_get_preference('custom_css', 'ads_theme', "UTF-8") !='') { ?>
<style type="text/css">
<?php echo osc_get_preference('custom_css', 'ads_theme', "UTF-8"); ?>
</style>
<?php } ?>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="<?php ads_body_class(); ?>">

<body class="cms-index-index cms-home-page">
<div id="page"> 
  <!-- Header -->
  <header>
    <div class="header-container">
      <div class="header-top">
        <div class="container">
          <div class="row"> 
            <!-- Header Language -->
            <div class="col-xs-12 col-sm-6">
              <div class="dropdown block-language-wrapper"> <a role="button" data-toggle="dropdown" data-target="#" class="block-language dropdown-toggle" href="#"> <img src="images/english.png" alt="language"> English <span class="caret"></span> </a>
                <ul class="dropdown-menu" role="menu">
                  <li role="presentation"> <a href="#"><img src="images/english.png" alt="language"> English </a> </li>
                  <li role="presentation"> <a href="#"><img src="images/francais.png" alt="language"> French </a> </li>
                  <li role="presentation"> <a href="#"><img src="images/german.png" alt="language"> German </a> </li>
                </ul>
              </div>
              <!-- End Header Language --> 
              
              <!-- Header Currency -->
              <div class="dropdown block-currency-wrapper"> <a role="button" data-toggle="dropdown" data-target="#" class="block-currency dropdown-toggle" href="#"> USD <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li role="presentation"><a href="#"> $ - Dollar </a> </li>
                  <li role="presentation"><a href="#"> £ - Pound </a> </li>
                  <li role="presentation"><a href="#"> € - Euro </a> </li>
                </ul>
              </div>
              <!-- End Header Currency -->
              <div class="welcome-msg"> Default welcome msg! </div>
            </div>
            <div class="col-xs-6 hidden-xs"> 
              <!-- Header Top Links -->
              <div class="toplinks">
                <div class="links">
                 
                 <div class="myaccount">                
                      <a title="Home" href="<?php echo osc_is_home_page(); ?>"><span class="hidden-xs"><?php _e('Home', 'ads') ; ?></span></a>
                 </div>
     
                  <div class="check">
                  <a title="Login" href="<?php echo osc_user_login_url(); ?>"><span class="hidden-xs"><?php _e('Login', 'ads') ; ?></span></a> 
                  </div>
                  
                  
                  <div class="demo">
                    
<a title="Register" href="<?php echo osc_register_account_url() ; ?>"><span class="hidden-xs"><?php _e('Register', 'ads'); ?></span></a>                  
                   </div>
  
             <div class="demo">

                    
<a title="Contact Us" href="<?php echo osc_contact_url(); ?>"><span class="hidden-xs"><?php _e('Contact us', 'ads'); ?></span></a>                  
                   </div>
  
                  
                
                </div>
              </div>
              <!-- End Header Top Links --> 
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 hidden-xs">
            <div class="search-box">
              <form action="cat" method="POST" id="search_mini_form" name="Categories">
                <input type="text" placeholder="Search entire store here..." value="Search" maxlength="70" name="search" id="search">
                <button type="button" class="search-btn-bg"><span class="glyphicon glyphicon-search"></span>&nbsp;</button>
              </form>
            </div>
          </div>
          <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 logo-block"> 
            <!-- Header Logo -->
            <div class="logo"> <a title="Magento Commerce" href="index.html"><img alt="Magento Commerce" src="images/logo.png"> </a> </div>
            <!-- End Header Logo --> 
          </div>
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <div class="top-cart-contain pull-right"> 
              <!-- Top Cart -->
              <div class="mini-cart">
                <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="shopping_cart.html"> <span class="cart_count">2</span><span class="price">My Cart / $259.00</span> </a> </div>
                <div>
                  <div class="top-cart-content"> 
                    
                    <!--block-subtitle-->
                    <ul class="mini-products-list" id="cart-sidebar">
                      <li class="item first">
                        <div class="item-inner"> <a class="product-image" title="Retis lapen casen" href="#l"><img alt="Retis lapen casen" src="products-images/product4.jpg"> </a>
                          <div class="product-details">
                            <div class="access"><a class="btn-remove1" title="Remove This Item" href="#">Remove</a> <a class="btn-edit" title="Edit item" href="#"><i class="icon-pencil"></i><span class="hidden">Edit item</span></a> </div>
                            <!--access--><strong>1</strong> x <span class="price">$179.99</span>
                            <p class="product-name"><a href="#">Retis lapen casen...</a> </p>
                          </div>
                        </div>
                      </li>
                      <li class="item last">
                        <div class="item-inner"> <a class="product-image" title="Retis lapen casen" href="product_detail.html"><img alt="Retis lapen casen" src="products-images/product3.jpg"> </a>
                          <div class="product-details">
                            <div class="access"><a class="btn-remove1" title="Remove This Item" href="#">Remove</a> <a class="btn-edit" title="Edit item" href="#"><i class="icon-pencil"></i><span class="hidden">Edit item</span></a> </div>
                            <!--access--><strong>1</strong> x <span class="price">$80.00</span>
                            <p class="product-name"><a href="#">Retis lapen casen...</a> </p>
                          </div>
                        </div>
                      </li>
                    </ul>
                    
                    <!--actions-->
                    <div class="actions">
                      <button class="btn-checkout" title="Checkout" type="button"><span>Checkout</span> </button>
                      <a href="shopping_cart.html" class="view-cart"><span>View Cart</span></a> </div>
                  </div>
                </div>
              </div>
              <!-- Top Cart -->
              <div id="ajaxconfig_info" style="display:none"> <a href="#/"></a>
                <input value="" type="hidden">
                <input id="enable_module" value="1" type="hidden">
                <input class="effect_to_cart" value="1" type="hidden">
                <input class="title_shopping_cart" value="Go to shopping cart" type="hidden">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- end header --> 
  
  <!-- Navigation -->
  
<!--  <nav>
    <div class="container">
      <div class="mm-toggle-wrap">
        <div class="mm-toggle"><i class="fa fa-align-justify"></i><span class="mm-label">Menu</span> </div>
      </div>
      <div class="nav-inner"> 

           <ul id="nav" class="hidden-xs">
               
                </div>
        
        <ul id="nav" class="hidden-xs level0">
          <li class="level0 parent drop-menu" id="nav-home"><a href="index.html" class="level-top">
          <i class="fa fa-home"></i><span class="hidden">Home</span></a>
           
            <ul style="display: none;" class="level1">
              <li class="level1"><a href="index.html"><span>Fashion Store</span></a> </li>
              <li class="level1"><a href="../digital/index.html"><span>Digital Store</span></a> </li>
              <li class="level1"><a href="../furniture/index.html"><span>Furniture Store</span></a> </li>
              <li class="level1"><a href="../jewellery/index.html"><span>Jewellery Store</span></a> </li>
            </ul>
          </li>
          
           <?php // RESET CATEGORIES IF WE USED THEN IN THE HEADER ?>
                <?php osc_goto_first_category(); ?>
                <?php if ( osc_count_categories() >= 0 ) { ?>
                <?php while ( osc_has_categories() ) { ?>
                 
                 
              
                 
                 
                          <li class="level0 nav-6 level-top drop-menu">     <a class="level-top <?php echo osc_category_slug(); ?>" href="<?php echo osc_search_category_url() ; ?>">
                                 <span><?php echo osc_category_name() ; ?></span></a>
                                 
                                  <?php if ( osc_count_subcategories() > 0 ) { ?>
                                
                                 <ul class="level1">
                                <?php 
                                $i=0;
                                while ( osc_has_subcategories() ) { ?>
                                
                                    <?php if( osc_category_total_items() >= 0 ) {
                                    
                                        if($i%4==0)
                                        {
                                           echo "<li class='level2 nav-6-1-1'>"; 
                                        }
                                        else
                                        {
                                            echo "<li class='level2 first'>";
                                        }
                                    
                                     ?>
                                 
                                 
                                        <a class="<?php echo osc_category_slug(); ?>" href="<?php echo osc_search_category_url() ; ?>"><span><?php echo osc_category_name() ; ?> (<?php echo osc_category_total_items() ; ?>)</span></a></li>
                                 
                                 
                                 
                                    <?php 
                                    
                                    $i++;
                                    } ?>
                                <?php 
                                
                                
                                } ?>
                                
                                
                                </ul>
                                <?php } ?>
                                </li>
                                
                <?php } ?>
                
                <?php }
                
                
               
                 ?>
     
        </ul>
       
      </div>
    </div>
  </nav>-->

	
    <?php if( osc_users_enabled() || ( !osc_users_enabled() && !osc_reg_user_post() )) { ?>
    	<div class="publish visible-xs"><a href="<?php echo osc_item_post_url() ; ?>"><?php _e("Publish your ad for free", 'ads');?></a></div>
    <?php } ?>
    <!-- Navbar -->
    <?php if (osc_show_flash_message()) { ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 notification">
                <?php osc_show_flash_message(); ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if (osc_search_page()) { ?>
	<?php //osc_breadcrumb(); ?><!-- Don't Remove - Require for Refine Category-->
    <?php } ?>