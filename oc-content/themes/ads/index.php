<?php
/*
Theme Name: ads
Theme URI: http://www.drizzledesigns.in/
Description: Osclass Premium Responsive Theme by DrizzleDesgins
Version: 1.3.8
Author: DrizzleDesigns
Author URI: http://www.drizzledesigns.in/
Widgets: sidebar, header, footer
Theme update URI: ads
*/

    function ads_theme_info() {
        return array(
             'name'        => 'ads'
            ,'version'     => '1.3.8'
            ,'description' => 'Osclass Premium Responsive Theme by DrizzleDesigns'
            ,'author_name' => 'DrizzleDesigns'
            ,'author_url'  => 'http://www.drizzledesigns.in/'
            ,'locations'   => array()
        );
    }

?>