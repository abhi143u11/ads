<?php
	// meta tag robots
    osc_add_hook('header','ads_nofollow_construct');

    ads_add_body_class('login');
	osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php');
?>
<div class="loginbox">
	<div class="container">
    	<div class="row">
        	<div class="col-sm-6">
        	<div class="login-register">
            <h2><?php _e('Login For User', 'ads'); ?></h2>
            <ul id="error_list"></ul>
            <form action="<?php echo osc_base_url(true); ?>" name="login" id="login" method="post" >
                    <input type="hidden" name="page" value="login" />
                    <input type="hidden" name="action" value="login_post" />
                    <div class="form-group">
                    	<input type="text" name="email" class="form-control" id="email" placeholder="<?php _e('E-mail', 'ads'); ?>">
                  	</div>
                    <div class="form-group">
                    	<input type="password" name="password" class="form-control" id="password" placeholder="<?php _e('Password', 'ads'); ?>">
                  	</div>
                    <div class="clearfix">
                    	<button type="submit" class="btn btn-success"><?php _e("Log in", 'ads');?></button>
                        <div class="checkbox pull-right">
                            <?php UserForm::rememberme_login_checkbox();?> <label for="remember"><?php _e('Remember me', 'ads'); ?></label>
                        </div>
                    </div>
                    <?php if (function_exists("fbc_button")) { ?>
						<div class="divider">
                            <hr />
                            <span><?php _e("or", 'ads'); ?></span>
                        </div>
                        <?php fbc_button(); ?>
                    <?php } ?>
                    <div class="panel-footer"><a href="<?php echo osc_register_account_url(); ?>"><?php _e("Register for a free account", 'ads'); ?></a>
                    <a class="pull-right" href="<?php echo osc_recover_user_password_url(); ?>" data-toggle="tooltip" data-placement="left" data-original-title="<?php _e("Forgot password?", 'ads'); ?>"><i class="fa fa-question-circle fa-lg"></i></a></div>
                </form>
    		</div>
            </div>
            <div class="col-sm-6">
            <div class="login-register">
            <h2><?php _e('Login For Enterprise', 'ads'); ?></h2>
            <ul id="error_list"></ul>
            <form action="<?php echo osc_base_url(true); ?>" name="login" id="login" method="post" >
                    <input type="hidden" name="page" value="login" />
                    <input type="hidden" name="action" value="login_post" />
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" id="email" placeholder="<?php _e('E-mail', 'ads'); ?>">
                      </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" id="password" placeholder="<?php _e('Password', 'ads'); ?>">
                      </div>
                    <div class="clearfix">
                        <button type="submit" class="btn btn-success"><?php _e("Log in", 'ads');?></button>
                        <div class="checkbox pull-right">
                            <?php UserForm::rememberme_login_checkbox();?> <label for="remember"><?php _e('Remember me', 'ads'); ?></label>
                        </div>
                    </div>
                   
                    <div class="panel-footer"><a href="#">&nbsp;</a>
                    <a class="pull-right" href="<?php echo osc_recover_user_password_url(); ?>" data-toggle="tooltip" data-placement="left" data-original-title="<?php _e("Forgot password?", 'ads'); ?>"><i class="fa fa-question-circle fa-lg"></i></a></div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
	$("form[name=login]").validate({
		rules: {
			email: {
				required: true,
				email: true
			}
		},
		messages: {
                email: {
                    required: "Email: this field is required.",
                    email: "Invalid email address."
                }
            },
		errorLabelContainer: "#error_list",
		wrapper: "li",
		invalidHandler: function(form, validator) {
			$('html,body').animate({ scrollTop: $('h1').offset().top }, { duration: 250, easing: 'swing'});
		},
		submitHandler: function(form){
			$('button[type=submit], input[type=submit]').attr('disabled', 'disabled');
			form.submit();
		}
	});
}); 
</script>
<?php osc_current_web_theme_path('footer.php') ; ?>