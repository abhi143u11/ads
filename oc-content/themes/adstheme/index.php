<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

/*
Theme Name: Ads
Theme URI: http://www.digitalsense.in
Description: Ads theme
Version: 1.0.0
Author: Digital Sense
Author URI: http://www.digitalsense.in
Widgets:  header, footer
Theme update URI: bender
*/

    function bender_theme_info() {
        return array(
             'name'        => 'Ads'
            ,'version'     => '1.0.0'
            ,'description' => 'Ads theme'
            ,'author_name' => 'Digital Sense'
            ,'author_url'  => 'http://www.digitalsense.in'
            ,'locations'   => array()
        );
    }

?>