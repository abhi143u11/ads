<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="<?php echo str_replace('_', '-', osc_current_user_locale()); ?>">
    <head>
        <?php osc_current_web_theme_path('common/head.php') ; ?>
        <!-- JavaScript --> 
<script type="text/javascript" src="<?php echo osc_current_web_theme_url(); ?>dist/js/jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo osc_current_web_theme_url(); ?>dist/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo osc_current_web_theme_url(); ?>dist/js/revslider.js"></script> 
<script type="text/javascript" src="<?php echo osc_current_web_theme_url(); ?>dist/js/common.js"></script> 
<script type="text/javascript" src="http://htmldemo.magikcommerce.com/ecommerce/creta-html-template/fashion/js/jquery.bxslider.min.js"></script> 
<script type="text/javascript" src="<?php echo osc_current_web_theme_url(); ?>dist/js/owl.carousel.min.js"></script> 
<script type="text/javascript" src="<?php echo osc_current_web_theme_url(); ?>dist/js/jquery.mobile-menu.min.js"></script> 


    </head>

<body class="cms-index-index cms-home-page">
<div id="page"> 
  <!-- Header -->
  <header>
    <div class="header-container">
      <div class="header-top">
        <div class="container">
          <div class="row"> 
            <!-- Header Language -->
            <div class="col-xs-12 col-sm-6">
              <div class="dropdown block-language-wrapper"> <a role="button" data-toggle="dropdown" data-target="#" class="block-language dropdown-toggle" href="index.html#"> <?php $lcode = osc_get_current_user_locale(); echo $lcode['s_short_name']; ?> <span class="caret"></span> </a>
                <ul class="dropdown-menu" role="menu">
                  <?php while ( osc_has_web_enabled_locales() ) { 
                    if (osc_locale_code()!=osc_current_user_locale()) { ?>
                    <li><a id="<?php echo osc_locale_code(); ?>" rel="nofollow" href="<?php echo osc_change_language_url ( osc_locale_code() ); ?>"><?php echo osc_locale_name(); ?></a></li><?php if( $i == 0 ) { echo ""; } ?>
                        <?php $i++; ?>
                    <?php } } ?>
                </ul>
              </div>
              <!-- End Header Language --> 
             
              
            </div>
            <div class="col-xs-12 col-sm-6"> 
              <!-- Header Top Links -->
              <div class="toplinks">
                <div class="links">
                 
            <?php if( osc_is_static_page() || osc_is_contact_page() ){ ?>
                  <div class="demo"><a class="ico-search icons" data-bclass-toggle="display-search"></a></div>
                <div class="demo"><a class="ico-menu icons" data-bclass-toggle="display-cat"></a></div>
            <?php } ?>
            <?php if( osc_users_enabled() ) { ?>
            <?php if( osc_is_web_user_logged_in() ) { ?>
               <div class="demo">
                    <span><?php echo sprintf(__('Hi %s', 'bender'), osc_logged_user_name() . '!'); ?>  &middot;</span>
                    <strong><a href="<?php echo osc_user_dashboard_url(); ?>"><?php _e('My account', 'bender'); ?></a></strong> &middot;
                    <a href="<?php echo osc_user_logout_url(); ?>"><?php _e('Logout', 'bender'); ?></a>
                </div>
            <?php } else { ?>
                <div class="demo"><a id="login_open" href="<?php echo osc_user_login_url(); ?>"><?php _e('Login', 'bender') ; ?></a></div>
                <?php if(osc_user_registration_enabled()) { ?>
                   <div class="demo"><a href="<?php echo osc_register_account_url() ; ?>"><?php _e('Register', 'bender'); ?></a> </div>
                <?php }; ?>
            <?php } ?>
            <?php } ?>
            <?php if( osc_users_enabled() || ( !osc_users_enabled() && !osc_reg_user_post() )) { ?>
                   <div class="demo"><a href="<?php echo osc_contact_url() ; ?>"><?php _e("Contact Us", 'bender');?></a> </div>
            <?php } ?>
            </div>
                  <!-- End Header Company -->
                  
                  
                </div>
              </div>
              <!-- End Header Top Links --> 
           
          </div>
        </div>
      </div>
         </header>
</div>
      <div class="container bgsrc">
    	<div class="row">
        	<div class="col-md-12">
        	<div class="bigsearch">
            	<form action="<?php echo osc_base_url(true); ?>" method="get" class="search nocsrf" <?php /* onsubmit="javascript:return doSearch();"*/ ?>>
                    <input type="hidden" name="page" value="search"/>
                    <input type="text" name="sPattern" id="query" class="input-text" value="" placeholder="<?php echo osc_esc_html(__(osc_get_preference('keyword_placeholder', 'flatter_theme'), 'flatter')); ?>" />
                    <?php osc_goto_first_category(); ?>
                    <?php  if ( osc_count_categories() ) { ?>
                    <select id="sCategory" name="sCategory">
                        <option value=""><?php _e('Select a category', 'flatter'); ?></option>
                        <?php while ( osc_has_categories() ) { ?>
                        <option class="maincat" value="<?php echo osc_category_id() ; ?>"><?php echo osc_category_name(); ?></option>
                            <?php if ( osc_count_subcategories() ) { ?>
								<?php while ( osc_has_subcategories() ) { ?>
                                <option class="subcat" value="<?php echo osc_category_id() ; ?>"><?php echo osc_category_name(); ?></option>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </select>
                    <?php //osc_categories_select('sCategory', null, __('Select a category', 'flatter')) ; ?>
                    <?php  } ?>
                   <?php if( osc_get_preference('location_input', 'flatter_theme') == '1') { ?> 
                   <?php $aRegions = Region::newInstance()->listAll(); ?>
					<?php if(count($aRegions) > 0 ) { ?>
                    <select name="sRegion" id="sRegion">
                    	<option value=""><?php _e('Select a region', 'flatter'); ?></option>
                        <?php foreach($aRegions as $region) { ?>
                        <option value="<?php echo $region['s_name'] ; ?>"><?php echo $region['s_name'] ; ?></option>
                        <?php } ?>
                    </select>
                    <?php } ?>
                    <?php } else { ?>
                    <input name="sCity" id="sCity" placeholder="<?php _e('Type a city', 'flatter'); ?>" type="text" />
                    <input name="sRegion" id="sRegion" type="hidden" />
                    <script type="text/javascript">
						$(function() {
							function log( message ) {
								$( "<div/>" ).text( message ).prependTo( "#log" );
								$( "#log" ).attr( "scrollTop", 0 );
							}
					
							$( "#sCity" ).autocomplete({
								source: "<?php echo osc_base_url(true); ?>?page=ajax&action=location",
								minLength: 2,
								select: function( event, ui ) {
									$("#sRegion").attr("value", ui.item.region);
									log( ui.item ?
										"<?php _e('Selected', 'flatter'); ?>: " + ui.item.value + " aka " + ui.item.id :
										"<?php _e('Nothing selected, input was', 'flatter'); ?> " + this.value );
								}
							});
						});
					</script>
                    <?php } ?>
                    <button class="btn btn-sclr">Find</button>
                </form>
                
            </div>
            </div>
        </div>
    </div>
 
    <?php
        $breadcrumb = osc_breadcrumb('&raquo;', false, get_breadcrumb_lang());
        if( $breadcrumb !== '') { ?>
    <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
            <?php echo $breadcrumb; ?>
        </div></div></div>
    <?php
        }
    ?>
    <?php osc_show_flash_message(); ?>
</div>
<?php osc_run_hook('before-content'); ?>
    <div class="main-container col2-left-layout bounceInUp animated">
    <?php osc_run_hook('before-main'); ?>
    <div id="main">
       
