<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // meta tag robots
    if( osc_item_is_spam() || osc_premium_is_spam() ) {
        osc_add_hook('header','bender_nofollow_construct');
    } else {
        osc_add_hook('header','bender_follow_construct');
    }

    osc_enqueue_script('fancybox');
    osc_enqueue_style('fancybox', osc_current_web_theme_url('js/fancybox/jquery.fancybox.css'));
    osc_enqueue_script('jquery-validate');

    bender_add_body_class('item');
  //  osc_add_hook('after-main','sidebar');
    function sidebar(){
        osc_current_web_theme_path('item-sidebar.php');
    }

    $location = array();
    if( osc_item_city_area() !== '' ) {
        $location[] = osc_item_city_area();
    }
    if( osc_item_city() !== '' ) {
        $location[] = osc_item_city();
    }
    if( osc_item_region() !== '' ) {
        $location[] = osc_item_region();
    }
    if( osc_item_country() !== '' ) {
        $location[] = osc_item_country();
    }

    osc_current_web_theme_path('header.php');
?>
<section class="main-container col1-layout">
    <div class="main">
      <div class="container">
        <div class="row">
          <div class="col-main">
            <div class="product-view">
              <div class="product-essential">
              
              
                  <div class="product-img-box col-lg-4 col-sm-5 col-xs-12">
                    
                    <div class="product-image"> <div class="box-timer">
             <div class="countbox_<?php echo osc_item_id(); ?> timer-grid"></div>
                    </div>
                      <div class="product-full">
                          <?php if( osc_images_enabled_at_items() ) { ?>
        <?php
        if( osc_count_item_resources() > 0 ) {
            $i = 0;
        ?>
        <img  id="product-zoom" src=" <?php echo osc_resource_url(); ?>" alt="<?php echo osc_item_title(); ?>" title="<?php echo osc_item_title(); ?>" />
           <div class="more-views">
                        <div class="slider-items-products">
                          <div id="gallery_01" class="product-flexslider hidden-buttons product-img-thumb">
                            <div class="slider-items slider-width-col4 block-content owl-carousel owl-theme" style="opacity: 1; display: block;">
                              <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1240px; left: 0px; display: block;"><div class="owl-item" style="width: 124px;"><div class="more-views-items">
                <?php for ( $i = 0; osc_has_item_resources(); $i++ ) { ?>
                <a href="<?php echo osc_resource_url(); ?>" class="fancybox" data-fancybox-group="group" title="<?php _e('Image', 'bender'); ?> <?php echo $i+1;?> / <?php echo osc_count_item_resources();?>">
                    <img src="<?php echo osc_resource_thumbnail_url(); ?>" width="75" alt="<?php echo osc_item_title(); ?>" title="<?php echo osc_item_title(); ?>" />
                </a>
                <?php } ?>
              <div class="owl-controls clickable"><div class="owl-buttons"><div class="owl-prev"><a class="flex-prev"></a></div><div class="owl-next"><a class="flex-next"></a></div></div></div></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- end: more-images --> 
                  </div>  </div>  </div>
        <?php } ?>
    <?php } ?> 
              </div> </div> </div>       
                          
                <script type="text/javascript">
var dthen<?php echo osc_item_id(); ?> = new Date("<?php echo date('m/d/Y H:m:s',strtotime(osc_item_dt_expiration())); ?>");
	start = new Date("<?php echo date('m/d/Y H:m:s'); ?>");
	start_date = Date.parse(start);
	var dnow<?php echo osc_item_id(); ?> = new Date(start_date);
	if (CountStepper > 0)
	ddiff = new Date((dnow<?php echo osc_item_id(); ?>) - (dthen<?php echo osc_item_id(); ?>));
	else
	ddiff = new Date((dthen<?php echo osc_item_id(); ?>) - (dnow<?php echo osc_item_id(); ?>));
	gsecs<?php echo osc_item_id(); ?> = Math.floor(ddiff.valueOf() / 1000);
	
	
	CountBack_slider(gsecs<?php echo osc_item_id(); ?>, "countbox_<?php echo osc_item_id(); ?>", 1);
        
        </script>          
                        
                              
                          
                  <div class="product-shop col-lg-8 col-sm-7 col-xs-12">
                   
                    <div class="product-name">
                      <h1><?php echo osc_item_title(); ?></h1>
                    </div>
                    <div class="ratings">
                      <div class="rating-box">
                        <div style="width:60%" class="rating"></div>
                      </div>
                     
                    </div>
                 
                    <div class="short-description">
                      <h2>Quick Overview</h2>
                      <p><?php echo osc_item_description(); ?></p>
                    </div>
               </div>
             
              </div>
            </div>
          </div>
          <div class="product-collateral col-lg-12 col-sm-12 col-xs-12">
     
              
             
                     <?php osc_run_hook('location'); ?> 
                </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php osc_current_web_theme_path('footer.php') ; ?>
