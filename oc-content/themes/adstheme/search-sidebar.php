<?php
/*
 *      Osclass – software for creating and publishing online classified
 *                           advertising platforms
 *
 *                        Copyright (C) 2014 OSCLASS
 *
 *       This program is free software: you can redistribute it and/or
 *     modify it under the terms of the GNU Affero General Public License
 *     as published by the Free Software Foundation, either version 3 of
 *            the License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful, but
 *         WITHOUT ANY WARRANTY; without even the implied warranty of
 *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU Affero General Public License for more details.
 *
 *      You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
$category = __get("category");
if (!isset($category['pk_i_id'])) {
    $category['pk_i_id'] = null;
}
?>

        <div class="col-left sidebar col-sm-3 col-xs-12 col-sm-pull-9">
            <aside class="col-left sidebar">
                <div class="side-nav-categories">
                    <div class="block-title"> Categories </div>
                    <div class="box-content box-category">

                        <?php bender_sidebar_category_search($category['pk_i_id']); ?>
                    </div>
                </div>
            </aside>
        </div>