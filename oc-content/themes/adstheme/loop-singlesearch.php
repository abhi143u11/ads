<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */
?>

<?php $size = explode('x', osc_thumbnail_dimensions()); ?>

 <li class="item col-lg-4 col-md-4 col-sm-4 col-xs-6 <?php echo $class; if(osc_item_is_premium()){ echo ' premium'; } ?>">
     <div class="item-inner">
                          <div class="item-img">
                            <div class="item-img-info">
    <?php if( osc_images_enabled_at_items() ) { ?>
        <?php if(osc_count_item_resources()) { ?>
     <a class="product-image" href="<?php echo osc_item_url() ; ?>" title="<?php echo osc_esc_html(osc_item_title()) ; ?>"><img src="<?php echo osc_resource_thumbnail_url(); ?>" title="" alt="<?php echo osc_esc_html(osc_item_title()) ; ?>" ></a>
        <?php } else { ?>
            <a class="product-image" href="<?php echo osc_item_url() ; ?>" title="<?php echo osc_esc_html(osc_item_title()) ; ?>"><img src="<?php echo osc_current_web_theme_url('images/no_photo.gif'); ?>" title="" alt="<?php echo osc_esc_html(osc_item_title()) ; ?>" ></a>
        <?php } ?>
    <?php } ?>
                            </div>
                          </div>
         <div class="box-timer">
             <div class="countbox_<?php echo osc_item_id(); ?> timer-grid"></div>
                    </div>
         <div class="item-info">
						  
                            <div class="info-inner">
							
                              <div class="item-title">    <a href="<?php echo osc_item_url() ; ?>" class="title" title="<?php echo osc_esc_html(osc_item_title()) ; ?>"><?php echo osc_item_title() ; ?></a> </div>
                              <div class="item-content">
							
                          
                                <div class="action">
                                  
                                </div>
                              </div>
                            </div>
                          </div>
    <div class="listing-detail">
        <div class="listing-cell">
            <div class="listing-data">
                <div class="listing-basicinfo">
                    <a href="<?php echo osc_item_url() ; ?>" class="title" title="<?php echo osc_esc_html(osc_item_title()) ; ?>"><?php echo osc_item_title() ; ?></a>
                    <div class="listing-attributes">
                        <span class="category"><?php echo osc_item_category() ; ?></span> -
                        <span class="location"><?php echo osc_item_city(); ?> <?php if( osc_item_region()!='' ) { ?> (<?php echo osc_item_region(); ?>)<?php } ?></span> <span class="g-hide">-</span> <?php echo osc_format_date(osc_item_pub_date()); ?>
                        <?php if( osc_price_enabled_at_items() ) { ?><span class="currency-value"><?php echo osc_format_price(osc_item_price()); ?></span><?php } ?>
                    </div>
                    <p><?php echo osc_highlight( osc_item_description() ,10) ; ?></p>
                </div>
               
            </div>
        </div>
    </div>
</li>
<script type="text/javascript">
var dthen<?php echo osc_item_id(); ?> = new Date("<?php echo date('m/d/Y H:m:s',strtotime(osc_item_dt_expiration())); ?>");
	start = new Date("<?php echo date('m/d/Y H:m:s'); ?>");
	start_date = Date.parse(start);
	var dnow<?php echo osc_item_id(); ?> = new Date(start_date);
	if (CountStepper > 0)
	ddiff = new Date((dnow<?php echo osc_item_id(); ?>) - (dthen<?php echo osc_item_id(); ?>));
	else
	ddiff = new Date((dthen<?php echo osc_item_id(); ?>) - (dnow<?php echo osc_item_id(); ?>));
	gsecs<?php echo osc_item_id(); ?> = Math.floor(ddiff.valueOf() / 1000);
	
	
	CountBack_slider(gsecs<?php echo osc_item_id(); ?>, "countbox_<?php echo osc_item_id(); ?>", 1);
        
        </script>