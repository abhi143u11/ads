<?php
    // meta tag robots
    osc_add_hook('header','gum_nofollow_construct');

    osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php') ;

    $osc_user = User::newInstance()->findByPrimaryKey(osc_logged_user_id());
    View::newInstance()->_exportVariableToView('user', $osc_user);
    ?>
<?php GumUserForm::location_javascript(); ?>
<div class="container-fluid main">
    <div class="container-fluid main">
        <!-- Example row of columns -->
        <div class="container">
            <div class="row">
                <div class="col-xs-12 custom-tabs">
                    <?php osc_current_web_theme_path('user-sidebar.php'); ?>
                    <div class="col-xs-12 col-md-8 col-lg-9">
                        <h1 class="h1-custom col-xs-12"><?php _e('My account', 'gum'); ?></h1>
                        <form name="register" action="<?php echo osc_base_url(true); ?>" method="post" >
                                <input type="hidden" name="page" value="user" />
                                <input type="hidden" name="action" value="profile_post" />
                                <div class="clearfix"></div>
                                <ul id="error_list"></ul>
                                <span class="col-xs-12 col-md-6 text-right text-normal">*&nbsp;<?php _e('Required', 'gum'); ?></span>
                                <div class="clearfix"></div>
                                <div class="col-xs-12 col-md-6 form-group">
                                    <label for="s_name"><?php _e('Name', 'gum'); ?> *</label>
                                    <?php GumUserForm::name_text(osc_user()); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12 col-md-6  form-group">
                                    <label for="phoneMobile"><?php _e('Cell phone', 'gum'); ?></label>
                                    <?php GumUserForm::mobile_text(osc_user()); ?>
                                </div>
                                <div class="clearfix"></div>
                                <?php $aCountries = osc_get_countries();
                                if(count($aCountries)>0) { ?>
                                <div class="col-xs-12 col-md-6 form-group">
                                    <?php if(count($aCountries)>1) { ?>
                                    <label for="country"><?php _e('Country', 'gum'); ?></label>
                                        <?php GumUserForm::country_select($aCountries, osc_user());
                                    } else { ?>
                                    <?php GumForm::generic_input_hidden('countryId', $aCountries[0]['pk_c_code']);?>
                                    <label for="country"><?php echo __('Country', 'gum'). " " .$aCountries[0]['s_name'];?></label>
                                    <?php } ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12 col-md-6 form-group">
                                    <label for="region"><?php _e('Region', 'gum'); ?></label>
                                    <?php GumUserForm::region_select(osc_get_regions(), osc_user()); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12 col-md-6 form-group">
                                    <label for="city"><?php _e('City', 'gum'); ?></label>
                                    <?php GumUserForm::city_select(osc_get_cities(), osc_user()); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12 col-md-6 form-group">
                                    <label for="city_area"><?php _e('City area', 'gum'); ?></label>
                                    <?php GumUserForm::city_area_text(osc_user()); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12 col-md-6 form-group">
                                    <label for="city_area"><?php _e('Address', 'gum'); ?></label>
                                    <?php GumUserForm::address_text(osc_user()); ?>
                                </div>
                                <div class="clearfix"></div>
                                <?php } ?>

                                <div class="col-xs-12 col-md-6 form-group">
                                    <label for="s_website"><?php _e('Website', 'gum'); ?></label>
                                    <?php GumUserForm::website_text(osc_user()); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12 col-md-6 form-group">
                                    <label for=""><?php _e('Description', 'gum'); ?></label>
                                    <?php GumUserForm::info_textarea('s_info', osc_locale_code(), @$osc_user['locale'][osc_locale_code()]['s_info']); ?>
                                </div>
                                <div class="clearfix"></div>

                                <?php osc_run_hook('user_profile_form', osc_user()); ?>
                                <div class="form-group">
                                    <div class="col-xs-6">
                                        <button type="submit" class="btn btn-primary-custom btn-block"><?php _e("Create", 'gum'); ?></button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr class="hr-custom">
                                <div class="col-xs-12">
                                    <p class="text-info"><?php _e('By creating an account or using a social login, you are agreeing to our website Terms & Conditions and Privacy Policy.', 'gum'); ?></p>
                                </div>
                                <div class="col-xs-12 form-group">
                                    <?php osc_run_hook('user_form', osc_user()); ?>
                                </div>
                            </form>
                    </div>
                </div>
                </div>
            </div>
        </div>
</div>
<?php UserForm::js_validation(); ?>
<?php osc_current_web_theme_path('footer.php') ; ?>