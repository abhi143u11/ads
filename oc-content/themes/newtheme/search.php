<?php
    // meta tag robots
if (osc_count_items() == 0 || stripos($_SERVER['REQUEST_URI'], 'search')) {
    osc_add_hook('header', 'gum_nofollow_construct');
} else {
    osc_add_hook('header', 'gum_follow_construct');
}

osc_add_hook('footer', function() { ?>
        <script>
            $(document).ready(function () {
                $('.plugin-hooks').find('input,select').not(':checkbox').addClass('form-control');
            });
        </script>
<?php });

$category['pk_i_id'] = null;
if(count(osc_search_category_id())>0 ) {
    $aux = osc_search_category_id();
    $category['pk_i_id'] = $aux[0];
}


osc_current_web_theme_path('header.php');


?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="search-overlay"></div>
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="title-section">
                <?php
                $demo = '<img src="'.osc_current_web_theme_url('images/banner-728x90.jpg').'"/>';
                $ad = osc_get_preference('search-results-top-728x90', 'gum_theme');
                echo ($ad=="#demo#") ? $demo : $ad;
                ?>
                    </div>
            </div>
            <div class="col-xs-6">
                <span class="counter-search line"><?php
                $search_number = gum_search_number();
                printf(__('%1$d - %2$d of %3$d listings', 'gum'), $search_number['from'], $search_number['to'], $search_number['of']);
                ?></span>
            </div>
            <div class="col-xs-6">
                <div class='pull-right line'>
                    <div class="visible-lg-block visible-md-block pull-left">
                        <?php _e('Sort by', 'gum'); ?>
                        <span class='select-custom-wrapper'>
                        <?php gum_search_orderby_select(); ?>
                        </span>
                    </div>
                    <a id="toogle-filters" class="visible-to-sm btn btn-secondary-custom pull-right" href=""><?php _e('Refine', 'gum'); ?></a>
                    <?php if(osc_is_web_user_logged_in()) { ?>
                    &nbsp;
                    <script type="text/javascript">
                    $(document).ready(function(){
                        $(".sub_button").click(function(){
                            $.post('<?php echo osc_base_url(true); ?>', {email:$("#alert_email").val(), userid:$("#alert_userId").val(), alert:$("#alert").val(), page:"ajax", action:"alerts"},
                                function(data){
                                    if(data==1) { alert('<?php echo osc_esc_js(__('You have sucessfully subscribed to the alert', 'gum')); ?>'); }
                                    else if(data==-1) { alert('<?php echo osc_esc_js(__('Invalid email address', 'gum')); ?>'); }
                                    else { alert('<?php echo osc_esc_js(__('There was a problem with the alert', 'gum')); ?>');
                                    };
                            });
                            return false;
                        });
                    });
                    </script>
                    <form action="<?php echo osc_base_url(true); ?>" method="post" name="sub_alert" id="sub_alert" class="pull-right nocsrf">
                        <?php AlertForm::page_hidden(); ?>
                        <?php AlertForm::alert_hidden(); ?>
                        <?php AlertForm::user_id_hidden(); ?>
                        <?php AlertForm::email_hidden(); ?>
                        <a class="sub_button  btn btn-secondary-custom" href=""><?php _e('Save search', 'gum'); ?></a>
                    </form>
                    <?php } ?>
                </div>
            </div>
        </div>
        <hr class="hr-custom">
    </div>
    <div class="container">
        <div class="row search-body">
            <div class="col-md-12">
                <div id="search-sidebar" class="search-sidebar">
                    <form action="<?php echo osc_base_url(true); ?>" id="form-search" method="get" class="nocsrf">
                        <input type="hidden" name="page" value="search"/>
                        <input type="hidden" name="sOrder" value="<?php echo osc_search_order(); ?>" />
                        <input type="hidden" name="iOrderType" value="<?php $allowedTypesForSorting = Search::getAllowedTypesForSorting(); echo $allowedTypesForSorting[osc_search_order_type()]; ?>" />
                        <div class="sidebar-block-title visible-to-sm ">
                            <label class="pull-right close-search-sidebar btn btn-secondary-custom "><?php _e('Close', 'gum'); ?></label>
                        </div>
                        <div class="sidebar-block-title">
                            <label><?php _e('Keyword', 'gum'); ?></label>
                        </div>
                        <div class="sidebar-block-content">
                            <input class="form-control" type="text" name="sPattern"  id="query" placeholder="<?php echo osc_esc_html(__(osc_get_preference('keyword_placeholder', 'gum_theme'), 'gum')); ?>" value="<?php echo osc_esc_html(osc_search_pattern()); ?>" />
                        </div>
                        <?php /* category */ ?>
                        <div class="sidebar-block-title">
                            <label><?php _e('Category', 'gum'); ?></label>
                        </div>
                        <div class="sidebar-block-content scroll">
                            <?php gum_sidebar_category_search($category['pk_i_id']); ?>
                        </div>
                        <?php /* location */ ?>
                        <div class="sidebar-block-title">
                            <label><?php _e('Location', 'gum'); ?></label>
                        </div>
                        <div class="sidebar-block-content scroll">
                            <?php gum_sidebar_location_search(); ?>
                        </div>
                        <?php /* price */ ?>
                        <?php if( osc_price_enabled_at_items() ) { ?>
                        <div class="sidebar-block-title">
                            <label><?php _e('Price', 'gum'); ?></label>
                        </div>
                        <div class="sidebar-block-content">
                            <div class="row form-inline">
                                <div class="col-xs-6">
                                    <label><?php _e('Min', 'gum') ; ?>.</label>
                                    <input class="form-control" type="text" id="priceMin" name="sPriceMin" value="<?php echo osc_esc_html(osc_search_price_min()); ?>" size="6" maxlength="6" />
                                </div>
                                <div class="col-xs-6">
                                    <label><?php _e('Max', 'gum') ; ?>.</label>
                                    <input class="form-control" type="text" id="priceMax" name="sPriceMax" value="<?php echo osc_esc_html(osc_search_price_max()); ?>" size="6" maxlength="6" />
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="sidebar-block-actions sidebar-block-plugins">
                            <div class="plugin-hooks">
                            <?php
                            if(osc_search_category_id()) {
                                osc_run_hook('search_form', osc_search_category_id()) ;
                            } else {
                                osc_run_hook('search_form') ;
                            }
                            ?>
                        </div>
                        </div>
                        <div class="sidebar-block-actions">
                            <a class="btn btn-primary pull-left reset-search-form"><?php _e('Clear', 'gum'); ?></a>
                            <button type="submit" class="btn btn-primary-custom pull-right"><?php _e('Update', 'gum'); ?></button>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                    <?php
                    $demo = '<p class="visible-lg-block visible-md-block" style="text-align: center;"><img style="padding:0.5em 0;" src="'.osc_current_web_theme_url('images/banner-160x600.jpg').'"/></p>';
                    $ad = osc_get_preference('search-sidebar-160x600', 'gum_theme');
                    echo ($ad=="#demo#") ? $demo : $ad;
                    ?>
                </div>
                <div class="search-content">
                    <div class="">
                        <?php
                        if(osc_count_items()>0) {
                            // premium ads
                            osc_get_premiums();
                            if(osc_count_premiums() > 0) {

                                echo '<span class="line">'.__('Featured Ads', 'gum').'</span>';
                                while ( osc_has_premiums() ) {
                                    gum_draw_itemPremium('list');
                                    $i++;
                                    if($i == 3){
                                        break;
                                    }
                                }
                            }
                            echo '<br><br><span class="line pull-left">'.__('Search results', 'gum').'</span>';
                            // results
                            $done = false;
                            $i = 0;
                            while ( osc_has_items() ) {
                                gum_draw_item('list');
                                $i++;
                                if($i%8==0 && !$done) {
                                    $done = true;
                                    $demo = '<p class="listing-list col-xs-12"><img style="width:100%; overflow:hidden; padding:0.5em 0;" src="'.osc_current_web_theme_url('images/banner-728x90.jpg').'"/></p>';
                                    $ad = osc_get_preference('search-results-middle-728x90', 'gum_theme');
                                    echo ($ad=="#demo#") ? $demo : $ad;
                                }
                            }
                            if($i%osc_default_results_per_page_at_search()==0 ) {
                                $demo = '<p class="listing-list col-xs-12"><img style="width:100%; overflow:hidden; padding:0.5em 0;" src="'.osc_current_web_theme_url('images/banner-728x90.jpg').'"/></p>';
                                $ad = osc_get_preference('search-results-bottom-728x90', 'gum_theme');
                                echo ($ad=="#demo#") ? $demo : $ad;
                            }  ?>
                        <div class="clearfix"></div>
                        <div class="col-sm-12 text-center" >
                            <?php echo gum_search_pagination(); ?>
                        </div>
                        <?php } else { ?>
                        <div class="text-left notice">
                          <?php printf(__("Sorry we didn't find any results for '%s'", "gum"), osc_search_pattern() ); ?>
                        </div>
                        <br>
                        <div class="text-left notice">
                            <h3 class="h2-custom"><?php _e('Tips for your search', 'gum'); ?></h3>
                            <ul class="checklist">
                                <li><i class="fa fa-check text-success"></i><?php _e('Check the spelling of your keywords for mistakes', 'gum'); ?></li>
                                <li><i class="fa fa-check text-success"></i><?php _e('Try using less or more general keywords', 'gum'); ?></li>
                                <li><i class="fa fa-check text-success"></i><?php _e('Try browsing the categories', 'gum'); ?></li>
                            </ul>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#sOrder').change(function(){
        var url = $(this).val();
        if (url) {
            window.location = url; // redirect
        }
        return false;
    });
</script>
<?php osc_current_web_theme_path('footer.php'); ?>