<?php
    // meta tag robots
    osc_add_hook('header','gum_follow_construct');


    osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php') ;
?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 col-sm-12">
                <div class="box-border">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="h1-custom"><?php _e('Recover your password', 'gum'); ?></h1>
                        </div>
                        <form name="register" class="col-xs-12" action="<?php echo osc_base_url(true); ?>" method="post" >
                            <input type="hidden" name="page" value="login"/>
                            <input type="hidden" name="action" value="recover_post"/>
                            <ul id="error_list"></ul>
                            <div class="form-group">
                                <p class="alert alert-info"><?php _e('Please enter your registered email address.', 'gum'); ?></p>
                            </div>
                            <div class="form-group">
                                <label for="name"><?php _e('Email address', 'gum'); ?> *</label>
                                <?php GumUserForm::email_text(); ?>
                                <span class="help-block">*&nbsp;<?php _e('Required', 'gum'); ?></span>
                            </div>
                            <div class="form-group">
                                <?php if( osc_recaptcha_items_enabled() ) { ?>
                                <div class="col-xs-6 text-center">
                                    <?php osc_show_recaptcha(); ?>
                                </div>
                                <?php }?>
                                <div class="clearfix"></div>
                                <div class="col-xs-12 col-ms-6 col-sm-6">
                                    <button type="submit" class="btn btn-primary-custom btn-block"><?php _e("Send me a new password", 'gum'); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php osc_current_web_theme_path('footer.php') ; ?>