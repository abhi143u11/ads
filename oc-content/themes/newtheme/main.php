<?php
// meta tag robots
osc_add_hook('header','gum_follow_construct');
osc_current_web_theme_path('header.php');

?>
<div class="container-fluid main">
<?php
$alerts_url = '#saved-searches';
$alerts_class = '';
if(osc_is_web_user_logged_in()){
    $alerts_url = osc_user_alerts_url();
    $alerts_class = 'openlink';
}
$fav_url = '#favourites';
$fav_class = '';
if(function_exists('watchlist') && osc_is_web_user_logged_in()){
    $fav_url = osc_render_file_url( 'watchlist/watchlist.php');
    $fav_class = 'openlink';
}
?>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul id="myTabs" class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="#latest"><?php _e('Latest listings', 'gum'); ?></a></li>
                    <?php if(function_exists('watchlist')) { ?>
                    <li role="presentation"><a class="<?php echo $fav_class; ?>" href="<?php echo $fav_url; ?>"><?php _e('Favourites', 'gum'); ?></a></li>
                    <?php } ?>
                    <li role="presentation"><a class="hidden-xs <?php echo $alerts_class; ?>" href="<?php echo $alerts_url; ?>"><?php _e('Saved Sarches', 'gum'); ?></a><a class="visible-xs-block <?php echo $alerts_class; ?>" href="<?php echo $alerts_url; ?>"><?php _e('Saved', 'gum'); ?></a></li>
                  </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="latest">
                        <?php
                        $total_latest_items = osc_max_latest_items();
                        View::newInstance()->_exportVariableToView('latestItems', Search::newInstance()->getLatestItems($total_latest_items, array(), true));
                        if(osc_count_latest_items()>0) :  ?>
                         <div class="row">
                        <?php $i = 0; while ( osc_has_latest_items() ) {
                            gum_draw_item();
                            $i++;
                        } ?>
                        </div>
                        <?php
                        if( osc_count_latest_items() == osc_max_latest_items() ) { ?>
                        <p class="text-center"><a class="btn btn-primary-custom" href="<?php echo osc_search_show_all_url() ; ?>">
                            <strong><?php _e('See more', 'gum') ; ?> &raquo;</strong></a>
                        </p>
                        <?php } ?>
                        <?php else: ?>
                        <div class="text-center theme-box">
                          <p>
                              <i class="fa fa-battery-empty fa-5x"></i>
                          </p>
                          <p class="lead">
                              <?php _e('There aren\'t listings available at this moment', 'gum'); ?>
                          </p>
                          <a class="btn btn-primary-custom" href="<?php echo osc_register_account_url(); ?>"><?php _e('Login now', 'gum'); ?></a>
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php if(function_exists('watchlist')) { ?>
                    <div role="tabpanel" class="tab-pane" id="favourites">
                        <div class="text-center theme-box">
                            <p>
                                <i class="fa fa-star-o fa-5x"></i>
                            </p>
                            <p class="lead">
                                <?php _e('You must login to see your favourites', 'gum'); ?>
                            </p>
                            <a class="btn btn-primary-custom" href="<?php echo osc_register_account_url(); ?>"><?php _e('Login now', 'gum'); ?></a>
                        </div>
                    </div>
                    <?php } ?>
                  <div role="tabpanel" class="tab-pane" id="saved-searches">
                      <div class="text-center theme-box">
                          <p>
                              <i class="fa fa-floppy-o fa-5x"></i>
                          </p>
                          <p class="lead">
                              <?php _e('You must login to see your saved searches', 'gum'); ?>
                          </p>
                          <a class="btn btn-primary-custom" href="<?php echo osc_register_account_url(); ?>"><?php _e('Login now', 'gum'); ?></a>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-md-4 visible-md-block visible-lg-block">
                <?php
                $demo = '<img src="'.osc_current_web_theme_url('images/banner-300x250.gif').'"/>';
                $ad = osc_get_preference('ad-homepage-sidebar-300x250', 'gum_theme');
                echo ($ad=="#demo#") ? $demo : $ad;
                ?>
            </div>
        </div>
    </div>
</div>

<?php osc_get_premiums(8);
if(osc_count_premiums()>0): ?>
<div class="container-fluid main-premium">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="pull-left lead"><?php _e('Ads in the spotlight', 'gum'); ?></span>
                <ul class="list-inline pull-right">
                        <li>
                            <a onclick="bxSlider_premium.goToPrevSlide();">
                                 <i class="fa fa-chevron-left fa-2x"></i>
                            </a>
                        </li>
                        <li >
                            <a onclick="bxSlider_premium.goToNextSlide();">
                                <i class="fa fa-chevron-right fa-2x"></i>
                            </a>
                        </li>
                        </li>
                </ul>
            </div>
        </div>
        <div class="row" id="premium-slider-wrapper">
            <ul class="bxslider_premium">
                <?php $i = 0; while (osc_has_premiums() ) {
                    echo '<li>';
                    gum_draw_item_premium();
                    echo '</li>';
                    $i++;
                } ?>
            </ul>
        </div>
    </div>
</div>
<?php endif; ?>

<div class="container-fluid main-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <?php
                $demo = '<img src="'.osc_current_web_theme_url('images/banner-728x90.jpg').'"/>';
                $ad = osc_get_preference('ad-homepage-middle-728x90', 'gum_theme');
                echo ($ad=="#demo#") ? $demo : $ad;
                ?>
            </div>
        </div>
    </div>
</div>

<script>
    $('#myTabs a').click(function (e) {
        e.preventDefault();
        <?php if(osc_is_web_user_logged_in()) {  ?>
        if($(this).hasClass('openlink')) {
            window.location = $(this).prop('href');
        } else {
        <?php } ?>
        $(this).tab('show');
        <?php if(osc_is_web_user_logged_in()) { echo '}'; } ?>
    });

    var bxSlider_premium;
    $(document).ready(function(){
        bxSlider_premium = $('.bxslider_premium').bxSlider({
            pager: false,
            controls: false,
            minSlides: 2,
            maxSlides: 5,
            slideWidth: 200,
            infiniteLoop: false,
            slideMargin: 0,
            moveSlides: 1
        });
    });
</script>
<?php osc_current_web_theme_path('footer.php'); ?>