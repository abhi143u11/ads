<?php
    // meta tag robots
    osc_add_hook('header','gum_follow_construct');

    osc_current_web_theme_path('header.php') ;
?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <?php osc_render_file(); ?>
        </div>
    </div>
</div>
<?php osc_current_web_theme_path('footer.php') ; ?>