<?php
    // meta tag robots
    osc_add_hook('header','gum_nofollow_construct');

    osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php') ;
    View::newInstance()->_exportVariableToView('user', User::newInstance()->findByPrimaryKey(osc_logged_user_id()));
?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12 custom-tabs">
                <?php osc_current_web_theme_path('user-sidebar.php'); ?>
                <div class="col-xs-12 col-md-8 col-lg-9">
                    <h1 class="h1-custom"><?php _e('My listings', 'gum'); ?></h1>
                    <?php if(osc_count_items() == 0) { ?>
                        <p class="empty" ><?php printf(__('Hi %s, you currently have 0 adverts', 'gum'), osc_user_name()); ?></p>
                        <div class="text-center notice">
                          <a class="btn btn-primary-custom" href="<?php echo osc_item_post_url(); ?>"><?php _e('Post an ad', 'gum'); ?></a>
                        </div>
                    <?php } else {
                        $i = 0; while ( osc_has_items() ) {
                        gum_draw_item('list');
                        $i++;
                    } ?>
                    <div class="col-sm-12 text-center" >
                        <?php echo gum_pagination_items(); ?>
                    </div>

                    <?php
                    if(osc_rewrite_enabled()){
                        $footerLinks = osc_search_footer_links();
                    ?>
                    <?php } ?>
                <?php } ?>
                </div>
            </div>
            <div class="col-xs-12">
            </div>
        </div>
    </div>
</div>


<?php UserForm::js_validation(); ?>
<?php osc_current_web_theme_path('footer.php') ; ?>