<?php
// meta tag robots
osc_add_hook('header','gum_follow_construct');

osc_enqueue_script('jquery-validate');
osc_current_web_theme_path('header.php');
?>
<div class="container-fluid main main-separator">
    <div class="container">
        <div class="row">
            <div class="col-xs-12  col-md-8">
                <p class="pull-left"><a href="<?php echo osc_item_url(); ?>"><i class="fa fa-angle-left"></i>&nbsp;<?php _e("Return to ad", "gum"); ?></a></p>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <hr class="hr-custom">
</div>
<div class="container-fluid main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12  col-md-8">
                <div>
                    <h1 class="item-title"><?php echo osc_item_title(); ?></h1>
                    <span class="h4 location pull-left"><?php echo gum_get_item_location(); ?></span>
                    <?php if (osc_price_enabled_at_items()) : ?>
                        <span class="h4 price pull-right"><?php echo osc_item_formated_price(); ?></span>
                        <div class="clearfix"></div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-xs-12  col-md-8">
                <div class="row box-border ">
                        <?php if( osc_item_is_expired () ) { ?>
                            <p>
                                <?php _e("The listing is expired. You can't contact the publisher.", 'gum'); ?>
                            </p>
                        <?php } else if( ( osc_logged_user_id() == osc_item_user_id() ) && osc_logged_user_id() != 0 ) { ?>
                            <p>
                                <?php _e("It's your own listing, you can't contact the publisher.", 'gum'); ?>
                            </p>
                        <?php } else if( osc_reg_user_can_contact() && !osc_is_web_user_logged_in() ) { ?>
                            <p>
                                <?php _e("You must log in or register a new account in order to contact the advertiser", 'gum'); ?>
                            </p>
                            <p class="contact_button">
                                <strong><a href="<?php echo osc_user_login_url(); ?>"><?php _e('Login', 'gum'); ?></a></strong>
                                <strong><a href="<?php echo osc_register_account_url(); ?>"><?php _e('Register for a free account', 'gum'); ?></a></strong>
                            </p>
                        <?php } else { ?>
                            <ul id="error_list"></ul>
                            <form action="<?php echo osc_base_url(true); ?>" method="post" name="contact_form" id="contact_form" <?php if(osc_item_attachment()) { echo 'enctype="multipart/form-data"'; };?> >
                                <?php osc_prepare_user_info(); ?>
                                <input type="hidden" name="action" value="contact_post" />
                                <input type="hidden" name="page" value="item" />
                                <input type="hidden" name="id" value="<?php echo osc_item_id(); ?>" />
                                <h2 class="h2-custom underline"><?php _e('Contact publisher', 'gum'); ?><span class="pull-right text-normal">*&nbsp;<?php _e('Required', 'gum'); ?></span></h2>
                                <div class="col-xs-12 form-group">
                                    <label class="control-label" for="message"><?php _e('Message', 'gum'); ?>: *</label>
                                    <?php GumContactForm::your_message(); ?>
                                </div>
                                <div class="col-xs-12 col-md-6 col-lg-6 form-group">
                                    <label class="control-label" for="yourName"><?php _e('Your name', 'gum'); ?>: *</label>
                                    <?php GumContactForm::your_name(); ?>
                                </div>
                                <div class="col-xs-12 col-md-6 col-lg-6 form-group">
                                    <label class="control-label" for="yourEmail"><?php _e('Your e-mail address', 'gum'); ?>: *</label>
                                    <?php GumContactForm::your_email(); ?>
                                </div>

                                <div class="col-xs-12 col-md-6 col-lg-6 form-group">
                                    <label class="control-label" for="phoneNumber"><?php _e('Phone number', 'gum'); ?> (<?php _e('optional', 'gum'); ?>):</label>
                                    <?php GumContactForm::your_phone_number(); ?>
                                </div>
                                <div class="clearfix"></div>

                                <?php if(osc_item_attachment()) { ?>
                                <div class="col-xs-12 form-group">
                                    <label class="control-label" for="attachment"><?php _e('Attachment', 'gum'); ?>:</label>
                                    <?php GumContactForm::your_attachment(); ?>
                                </div>
                                <?php } ?>

                                <div class="col-xs-12">
                                        <?php osc_run_hook('item_contact_form', osc_item_id()); ?>
                                </div>
                                <?php if( osc_recaptcha_public_key() ) { ?>
                                <div class="col-xs-12">
                                    <?php osc_show_recaptcha(); ?>
                                    <br>
                                </div>
                                <?php } ?>
                                <div class="col-xs-12 col-md-6 col-lg-6 form-group">
                                    <button type="submit" class="btn btn-primary-custom btn-block"><?php _e("Send", 'gum');?></button>
                                </div>

                            </form>
                        <?php } ?>
                    </div>
                </div>
            <!--</div>-->

            <div class="col-xs-12  col-md-4">
                <?php /* Useful tips */ ?>
                <div class="col-sm-12 visible-sm-block visible-md-block visible-lg-block">
                    <div class="row box-border ">
                        <?php GumViewHelpers::show_useful_tips('1'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    var bxSlider_tips_1;
    var bxSlider_tips_2;
    $(document).ready(function(){
        bxSlider_tips_1 = $('.bxslider_tips_1').bxSlider({
            pager: false,
            controls: false,
            responsive : true,
            adaptiveHeight: true,
            moveSlides: 1
        });
    });
</script>
<?php GumContactForm::js_validation(); ?>
<?php osc_current_web_theme_path('footer.php'); ?>