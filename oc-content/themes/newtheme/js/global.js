$(document).ready(function () {
    $('.header-keyword select#sCategory').change(function () {
        $("#width_tmp_option").html($('.header-keyword select#sCategory option:selected').text());
        $(this).width($("#width_tmp_select").width());
        $(this).parent().width($("#width_tmp_select").width());
    });
    $('.header-keyword select#sCategory').change();


    $('.flashmessage .ico-close').click(function () {
        $(this).parents('.flashmessage').remove();
    });


    $('.viewport-mask').click(function( event ) {
        $('.search-sidebar-mobile').remove();
        $('.filters-mask').toggleClass('open');
        // reset <.filters-mask.open> height
        $('.filters-mask.open').css('height', 'auto' );
    });


    $('.reset-search-form').click(function( event ) {
        event.preventDefault();
        var form = $(this).parents('form:first');
        console.log(form);
        $(form)[0].reset();
        $(form).find("input[type=text], textarea").val("");

        $(form)
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
    });


    $('#toogle-filters').click(function( event ) {
        event.preventDefault();

        $('.search-overlay').toggleClass('open');
        $('#search-sidebar').toggleClass('open');
        $('.search-overlay.open').css('height', $(document).height() );
        $('body').scrollTo('.search-overlay.open',{duration:'slow', offsetTop : '50'});

        $('html, body').animate({
            scrollTop: $('.search-overlay.open').offset().top
        }, 2000);
    });
    $('.search-overlay, .close-search-sidebar').click(function( event ) {
        $('#search-sidebar').toggleClass('open');
        $('.search-overlay').toggleClass('open');
    });

    $('#toogle-filters_').click(function( event ) {
        event.preventDefault();

        $('.filters-mask').toggleClass('open');
        var _copy = $('#search-sidebar').get( 0 );
        var copy = $(_copy).clone();
        $(copy).toggleClass('search-sidebar-mobile');
        $(copy).toggleClass('visible-to-sm');
        $('.filters-mask').before( copy );
        $('.filters-mask.open').css('height', $(copy).height() );
    });




});
