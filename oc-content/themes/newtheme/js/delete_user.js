$(document).ready(function(){
    $(".opt_delete_account a").click(function(){
        swal({
            title: gum_theme.delete_user_title,
            text: gum_theme.delete_user_text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: gum_theme.langs.delete,
            closeOnConfirm: true
        },
        function (isConfirm) {
            window.location = gum_theme.base_url + '?page=user&action=delete&id=' + gum_theme.user.id  + '&secret=' + gum_theme.user.secret;
            swal("Deleted!", gum_theme.delete_user_title, "success");
        });
    });

//
//    $("#dialog-delete-account").click(function(){
//        swal({
//            title: "Are you sure?",
//            text: "You will not be able to recover this imaginary file!",
//            type: "warning",
//            showCancelButton: true,
//            confirmButtonColor: "#DD6B55",
//            confirmButtonText: gum_theme.langs.delete,
//            closeOnConfirm: true
//        },
//        function () {
//            window.location = gum_theme.base_url + '?page=user&action=delete&id=' + gum_theme.user.id  + '&secret=' + gum_theme.user.secret;
//            swal("Deleted!", "Your imaginary file has been deleted.", "success");
//        });
//    });


//    $("#dialog-delete-account").dialog({
//        autoOpen: false,
//        modal: true,
//        buttons: [
//            {
//                text: gum_theme.langs.delete,
//                click: function() {
//                    window.location = gum_theme.base_url + '?page=user&action=delete&id=' + gum_theme.user.id  + '&secret=' + gum_theme.user.secret;
//                }
//            },
//            {
//                text: gum_theme.langs.cancel,
//                click: function() {
//                    $(this).dialog("close");
//                }
//            }
//        ]
//    });
});