<?php
    // meta tag robots
    osc_add_hook('header','gum_nofollow_construct');

    osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php') ;
    View::newInstance()->_exportVariableToView('user', User::newInstance()->findByPrimaryKey(osc_logged_user_id()));

?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12 custom-tabs">
                <?php osc_current_web_theme_path('user-sidebar.php'); ?>
                <div class="col-xs-12 col-md-8 col-lg-9">
                    <h1 class="h1-custom col-xs-12"><?php _e('Change e-mail', 'gum'); ?></h1>
                    <form id="change-email" action="<?php echo osc_base_url(true); ?>" method="post">
                            <input type="hidden" name="page" value="user" />
                            <input type="hidden" name="action" value="change_email_post" />
                            <div class="clearfix"></div>
                            <ul id="error_list"></ul>
                            <div class="clearfix"></div>
                            <div class="col-xs-12 col-md-6 form-group">
                                <label for=""><?php _e('Current e-mail', 'gum'); ?> </label> <span><?php echo osc_logged_user_email(); ?></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12 col-md-6 form-group">
                                <label for="new_email"><?php _e('New e-mail', 'gum'); ?></label>
                                <input type="text" class="form-control" name="new_email" id="new_email" value="" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary-custom btn-block"><?php _e("Update", 'gum'); ?></button>
                                </div>
                            </div>
                    </form>
                </div>
            </div> 
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('form#change-email').validate({
            rules: {
                new_email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                new_email: {
                    required: '<?php echo osc_esc_js(__("Email: this field is required", "gum")); ?>.',
                    email: '<?php echo osc_esc_js(__("Invalid email address", "gum")); ?>.'
                }
            },
            errorLabelContainer: "#error_list",
            wrapper: "li",
            invalidHandler: function(form, validator) {
                $('html,body').animate({ scrollTop: $('h1').offset().top }, { duration: 250, easing: 'swing'});
            },
            submitHandler: function(form){
                $('button[type=submit], input[type=submit]').attr('disabled', 'disabled');
                form.submit();
            }
        });
    });
</script>
<?php osc_current_web_theme_path('footer.php') ; ?>