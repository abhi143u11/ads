<?php
/* DEFINES */
define('GUM_THEME_VERSION', '100');
define('GUM_THEME_PATH', dirname(__FILE__) . '/' );

// models
require GUM_THEME_PATH . 'model/GumModelUsefulTips.php';
require GUM_THEME_PATH . 'model/GumModelItemPhoneReveal.php';

// frm classes
require GUM_THEME_PATH . 'class/frm/GumForm.php';
require GUM_THEME_PATH . 'class/frm/GumItemForm.php';
require GUM_THEME_PATH . 'class/frm/GumFieldForm.php';
require GUM_THEME_PATH . 'class/frm/GumUserForm.php';
require GUM_THEME_PATH . 'class/frm/GumContactForm.php';
require GUM_THEME_PATH . 'class/GumItemPost.php';

// all
require GUM_THEME_PATH . 'class/GumHelpers.php';
require GUM_THEME_PATH . 'class/GumSocialLinks.php';
require GUM_THEME_PATH . 'class/GumViewHelpers.php';
require GUM_THEME_PATH . 'class/GumTheme.php'; // <-
require GUM_THEME_PATH . 'class/GumAdmin.php';
require GUM_THEME_PATH . 'class/GumAssets.php';
require GUM_THEME_PATH . 'class/GumAjax.php';

require GUM_THEME_PATH . 'class/GumGeo.php';

require GUM_THEME_PATH . 'class/watchlist/GumWatchlist.php';

require GUM_THEME_PATH . 'class/GumListingHelper.php';

$gumTheme = new GumTheme();
$gumAssets = new GumAssets();
$gumAdmin = new GumAdmin();
$gumAjax = new GumAjax();

require GUM_THEME_PATH . 'class/GumRegister.php';

$_GumWatchlist = new GumWatchlist();

require GUM_THEME_PATH . 'class/payments_pro/GumPaymentsPro.php';
$_GumPaymentsPro = new GumPaymentsPro();