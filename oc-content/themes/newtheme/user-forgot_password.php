<?php
    // meta tag robots
    osc_add_hook('header','gum_nofollow_construct');


    osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php') ;
?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 col-sm-12">
                <div class="box-border">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="h1-custom"><?php _e('Recover your password', 'gum'); ?></h1>
                        </div>
                        <form name="register" class="col-xs-12" action="<?php echo osc_base_url(true); ?>" method="post" >
                            <input type="hidden" name="page" value="login" />
                            <input type="hidden" name="action" value="forgot_post" />
                            <input type="hidden" name="userId" value="<?php echo osc_esc_html(Params::getParam('userId')); ?>" />
                            <input type="hidden" name="code" value="<?php echo osc_esc_html(Params::getParam('code')); ?>" />
                            <ul id="error_list"></ul>
                            <div class="form-group">
                                <label for="new_password"><?php _e('New password', 'gum'); ?> *</label>
                                <input id="new_password" type="password" name="new_password" value="" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="new_password2"><?php _e('Repeat new password', 'gum'); ?> *</label>
                                <input id="new_password2" type="password" name="new_password2" value="" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <div class="clearfix"></div>
                                <div class="col-xs-12 col-ms-6 col-sm-6">
                                    <button type="submit" class="btn btn-primary-custom btn-block"><?php _e("Change password", 'gum'); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php osc_current_web_theme_path('footer.php') ; ?>