<?php
$js_lang = array(
    'delete' => __('Delete', 'gum'),
    'cancel' => __('Cancel', 'gum')
);

osc_enqueue_script('jquery');
?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php echo meta_title(); ?></title>
<meta name="title" content="<?php echo osc_esc_html(meta_title()); ?>" />
<?php if (meta_description() != '') { ?>
    <meta name="description" content="<?php echo osc_esc_html(meta_description()); ?>" />
<?php } ?>
<?php if (meta_keywords() != '') { ?>
    <meta name="keywords" content="<?php echo osc_esc_html(meta_keywords()); ?>" />
<?php } ?>
<?php if (osc_get_canonical() != '') { ?>
    <!-- canonical -->
    <link rel="canonical" href="<?php echo osc_get_canonical(); ?>"/>
    <!-- /canonical -->
<?php } ?>

<!-- favicon -->
<?php $favicon_url = gum_favicon_url();?>
<link rel="shortcut icon" href="<?php echo $favicon_url; ?>" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $favicon_url; ?>" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $favicon_url; ?>" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $favicon_url; ?>" />
<link rel="apple-touch-icon-precomposed" href="<?php echo $favicon_url; ?>" />
<!-- /favicon -->


<?php /* openGrapth meta tags */ ?>
<meta property="og:type" content="website">
<meta property="og:image" content="<?php echo ''; ?>">
<meta property="og:title" content="<?php echo osc_esc_html(meta_title()); ?>">
<meta property="og:description" content="<?php echo osc_esc_html(meta_description()); ?>">

<script type="text/javascript">
    var gum_theme = window.gum_theme || {};
    gum_theme.base_url = '<?php echo osc_base_url(true); ?>';
    gum_theme.langs = <?php echo json_encode($js_lang); ?>;
    gum_theme.fancybox_prev = '<?php echo osc_esc_js(__('Previous image', 'gum')) ?>';
    gum_theme.fancybox_next = '<?php echo osc_esc_js(__('Next image', 'gum')) ?>';
    gum_theme.fancybox_closeBtn = '<?php echo osc_esc_js(__('Close', 'gum')) ?>';
    gum_theme.delete_user_title = '<?php echo osc_esc_js("Delete account", "gum"); ?>';
    gum_theme.delete_user_text = '<?php echo osc_esc_js("Are you sure you want to delete your account?", "gum"); ?>';
</script>
<?php osc_run_hook('header'); ?>

<style>
    .banner-header {
        background: url(<?php echo gum_homebanner_url(); ?>) no-repeat center;
        background-size: cover;
    }
</style>