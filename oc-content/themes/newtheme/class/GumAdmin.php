<?php

class GumAdmin {
    public function __construct()
    {
        if(strpos(Params::getParam('file'),'oc-content/themes/gum/admin')!==false) {

            osc_enqueue_style('gum-admin-css', osc_current_web_theme_url('admin/css/style.css'));
            osc_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
            
            osc_add_hook('admin_header',        array(&$this, '_init_pageHeader') );
            osc_add_filter('admin_body_class',   array(&$this, '_body_class') );
        }
    }

    function _init_pageHeader() {
        osc_remove_hook('admin_page_header', 'customPageHeader');
        osc_add_hook('admin_page_header', array(&$this,'_customPageHeader'));
    }

    function _customPageHeader() {
    ?>
        <h1><?php _e('Gum theme', 'gum'); ?></h1>
    <?php
        @include(THEMES_PATH . 'gum/admin/header.php');
    }

    function _body_class($array){
        $array[] = 'market';
        return $array;
    }
}
