<?php

class GumItemPost {
    public function __construct()
    {
        osc_remove_hook('item_form', 'osc_meta_publish');
        osc_remove_hook('item_edit', 'osc_meta_edit'); 
        osc_add_hook('item_form', array(&$this,'meta_publish'));
        osc_add_hook('item_edit', array(&$this,'meta_edit'));
    }

    function meta_publish($catId = null) {
        osc_enqueue_script('php-date');
        echo '<div class="row">';
            GumFieldForm::meta_fields_input($catId);
        echo '</div>';
    }

    function meta_edit($catId = null, $item_id = null) {
        osc_enqueue_script('php-date');
        echo '<div class="row">';
            GumFieldForm::meta_fields_input($catId, $item_id);
        echo '</div>';
    }
}

$_GumItemPost = new GumItemPost();