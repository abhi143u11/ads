<?php

class GumViewHelpers {

    static function show_social_buttons()
    {
       if(osc_is_ad_page()) {
       ?>
<div class="social-links">
    <h2><?php _e('Share:', 'gum');?></h2>
    <ul class="list-inline">
        <li><a href="<?php echo GumSocialLinks::facebook_url(); ?>">
            <img src="<?php echo osc_current_web_theme_url('images/social-icons/64/64-facebook.png')?>"/></a></li>
        <li><a href="<?php echo GumSocialLinks::twitter_url(); ?>">
            <img src="<?php echo osc_current_web_theme_url('images/social-icons/64/64-twitter.png')?>"/></a></li>
        <li><a href="<?php echo GumSocialLinks::email_url() ?>" class="mail">
            <i class="fa fa-envelope"></i></a></li>
    </ul>
    <div class="clearfix"></div>
</div>
    <?php
       }
    }

    static function show_useful_tips($class = '') {
?>
<div class="">
    <h5>
        <div class="row">
            <div class="col-md-12">
                <span class="pull-left lead"><?php _e('Usefull tips', 'gum'); ?></span>
                <ul class="list-inline pull-right">
                    <li>
                        <a onclick="bxSlider_tips_<?php echo $class;?>.goToPrevSlide();">
                            <i class="fa fa-chevron-left fa-2x"></i>
                        </a>
                    </li>
                    <li >
                        <a onclick="bxSlider_tips_<?php echo $class;?>.goToNextSlide();">
                            <i class="fa fa-chevron-right fa-2x"></i>
                        </a>
                    </li>
                    </li>
                </ul>
            </div>
        </div>
    </h5>
    <hr class="hr-custom">
    <?php $aUsefulTips = GumModelUsefulTips::newInstance()->all();
    if(count($aUsefulTips>0)) : ?>
    <ul class="bxslider_tips_<?php echo $class;?>">
    <?php foreach($aUsefulTips as $value) : ?>
        <li><?php echo $value['s_description'];?></li>
    <?php endforeach; ?>
    </ul>
    <?php endif;?>
</div>
<?php
    }
}