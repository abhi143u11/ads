<?php

class GumTheme {
    var $version;

    public function __construct()
    {
        $this->version = GUM_THEME_VERSION;

        // update theme if necessary
        $this->update();
        osc_add_hook('theme_delete_gum', array(&$this,'theme_delete') );

        if(osc_is_public_profile()) {
            osc_add_hook('before_html', function(){
                osc_redirect_to(osc_search_url(array('user' => osc_user_id())));
            });
        }
        osc_add_admin_submenu_divider('appearance', __('Gum Theme', 'gum'), 'gum_theme_div');
        osc_admin_menu_appearance(__('Theme settings', 'gum'), osc_admin_render_theme_url('oc-content/themes/gum/admin/settings.php'), 'settings_gum');
    }

    function theme_install() {
        osc_set_preference('version', $this->version, 'gum_theme');
        osc_set_preference('donation', '0', 'gum_theme');
        // header
        osc_set_preference('logo', '', 'gum_theme');
        osc_set_preference('favicon', '', 'gum_theme');
        osc_set_preference('homebanner', '', 'gum_theme');
        osc_set_preference('keyword_placeholder', __('Search ...', 'gum'), 'gum_theme');
        osc_set_preference('location_region_placeholder', __('Region', 'gum'), 'gum_theme');
        osc_set_preference('location_city_placeholder', __('City', 'gum'), 'gum_theme');
        osc_set_preference('search_location_type', 'region', 'gum_theme');
        // footer
        osc_set_preference('footer_link', '1', 'gum_theme');
        osc_set_preference('footer_social_networks', '1', 'gum_theme');
        osc_set_preference('footer_social_links_target', '1', 'gum_theme');
        osc_set_preference('footer_copyright', '&copy; 2015 Company, Inc.', 'gum_theme');
        osc_set_preference('footer_blog_url', '', 'gum_theme');
        osc_set_preference('footer_facebook_url', '', 'gum_theme');
        osc_set_preference('footer_twitter_url', '', 'gum_theme');
        osc_set_preference('footer_gplus_url', '', 'gum_theme');
        osc_set_preference('footer_pinterest_url', '', 'gum_theme');

        osc_set_preference('footer_seo_links', '1', 'gum_theme');
        osc_set_preference('footer_seo_links_topsearches', '1', 'gum_theme');
        osc_set_preference('footer_seo_links_toplocations', '1', 'gum_theme');
        osc_set_preference('footer_seo_links_toplocations_type', 'region', 'gum_theme');
        // search sidebar
        osc_set_preference('defaultLocationShowAs', 'dropdown', 'gum_theme');
        // item detail
        osc_set_preference('ad_id', '1', 'gum_theme');

        // init usefull tips
        GumModelUsefulTips::newInstance()->import('/gum/sql_usefultips.sql');
        GumModelUsefulTips::newInstance()->insertBasicData();

        // init GumModelItemPhoneReveal
        GumModelItemPhoneReveal::newInstance()->import('/gum/phone_reveal.sql');
        GumModelItemPhoneReveal::newInstance()->_init();

        osc_reset_preferences();
    }

    function theme_delete() {
        GumModelUsefulTips::newInstance()->uninstall();
        GumModelItemPhoneReveal::newInstance()->uninstall();
        Preference::newInstance()->delete(array('s_section' => 'gum_theme'));
    }

    function update() {
        $current_version = osc_get_preference('version', 'gum_theme');
        //check if current version is installed or need an update<
        if (!$current_version) {
            $this->theme_install();
            osc_set_preference('version', '100', 'gum_theme');
            $current_version = 100;
        }
//        if ($current_version < '{new_version}') {
//            // update here
//            osc_set_preference('version', '{new_version}', 'gum_theme');
//        }
    }
}