<?php
    // meta tag robots
    osc_add_hook('header','gum_follow_construct');

    osc_current_web_theme_path('header.php') ;
?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">

            <?php /* render file */ ?>
            <div style='text-align: center;width: 100%; '>
            <div style="margin:2em;font-size: 2em;line-height: 1.2em;">
                <p><?php _e('Want to publish more listings?', 'payment_pro'); ?></p>
            </div>

            <a class="btn btn-primary-custom" href="<?php echo osc_item_post_url_in_category(); ?>"><?php _e("Publish another listing", 'payment_pro'); ?></a>
            <a class="btn btn-secondary-custom" href="<?php echo osc_base_url(); ?>"><?php _e('Continue browsing', 'payment_pro'); ?></a>
            </div>
            <?php osc_run_hook('payment_pro_done_page', Params::getParam('tx')); ?>
            <?php /* --- render file */ ?>
        </div>
    </div>
</div>
<?php osc_current_web_theme_path('footer.php') ; ?>
