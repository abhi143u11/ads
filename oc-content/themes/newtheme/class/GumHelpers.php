<?php

function gum_body_class() {
    if (Params::getParam('page') == '' && Params::getParam('action') == '') {
        return 'homepage';
    }
    if(osc_is_404()) {
        return 'page-404';
    }
    if (osc_is_ad_page()) {
        return 'item';
    }
    if (Params::getParam('page')=='search') {
        return 'search';
    }
    if(osc_is_list_alerts()) {
        return 'user-alerts';
    }
    if(osc_is_list_items() || Params::getParam('file')=='watchlist/watchlist.php') {
        return 'user-items';
    }
    if(osc_is_publish_page()) {
        return 'item item-post';
    }
    if(osc_is_recover_page()) {
        return 'recover';
    }
    if(osc_is_item_contact_page()) {
        return 'item item-contact';
    }

}

if(!function_exists('osc_item_contact_url')) {
    function osc_item_contact_url() {
        if ( osc_rewrite_enabled() ) {
            $path = osc_base_url() . osc_get_preference('rewrite_item_contact')."/".osc_item_id();
        } else {
            $path = osc_base_url(true) . '?page=item&action=contact&id='.osc_item_id();
        }
        return $path;
    }
}

if (!function_exists('gum_nofollow_construct')) {

    /**
     * Hook for header, meta tags robots nofollows
     */
    function gum_nofollow_construct() {
        echo '<meta name="robots" content="noindex, nofollow, noarchive" />' . PHP_EOL;
        echo '<meta name="googlebot" content="noindex, nofollow, noarchive" />' . PHP_EOL;
    }

}
if (!function_exists('gum_follow_construct')) {

    /**
     * Hook for header, meta tags robots follow
     */
    function gum_follow_construct() {
        echo '<meta name="robots" content="index, follow" />' . PHP_EOL;
        echo '<meta name="googlebot" content="index, follow" />' . PHP_EOL;
    }

}

if(!function_exists('addhttp')) {
    function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }
}
if (!function_exists('get_breadcrumb_lang')) {

    function get_breadcrumb_lang() {
        $lang = array();
        $lang['item_add'] = __('Publish a listing', 'gum');
        $lang['item_edit'] = __('Edit your listing', 'gum');
        $lang['item_send_friend'] = __('Send to a friend', 'gum');
        $lang['item_contact'] = __('Contact publisher', 'gum');
        $lang['search'] = __('Search results', 'gum');
        $lang['search_pattern'] = __('Search results: %s', 'gum');
        $lang['user_dashboard'] = __('Dashboard', 'gum');
        $lang['user_dashboard_profile'] = __("%s's profile", 'gum');
        $lang['user_account'] = __('Account', 'gum');
        $lang['user_items'] = __('Listings', 'gum');
        $lang['user_alerts'] = __('Alerts', 'gum');
        $lang['user_profile'] = __('Update account', 'gum');
        $lang['user_change_email'] = __('Change email', 'gum');
        $lang['user_change_username'] = __('Change username', 'gum');
        $lang['user_change_password'] = __('Change password', 'gum');
        $lang['login'] = __('Login', 'gum');
        $lang['login_recover'] = __('Recover password', 'gum');
        $lang['login_forgot'] = __('Change password', 'gum');
        $lang['register'] = __('Create a new account', 'gum');
        $lang['contact'] = __('Contact', 'gum');
        return $lang;
    }
}

if (!function_exists('get_user_menu')) {

    function get_user_menu() {
        $options = array();

        $options[] = array(
            'name' => __('Public Profile', 'gum'),
            'url' => osc_user_public_profile_url(osc_logged_user_id()),
            'class' => 'opt_publicprofile'
        );

        $class = ''; if(osc_is_list_items() || osc_is_user_dashboard()) {$class = "active";}
        $options[] = array(
            'name' => __('Listings', 'gum'),
            'url' => osc_user_list_items_url(),
            'class' => 'opt_items '.$class
        );
        $class = ''; if(osc_is_list_alerts()) {$class = "active";}
        $options[] = array(
            'name' => __('Alerts', 'gum'),
            'url' => osc_user_alerts_url(),
            'class' => 'opt_alerts '.$class
        );
        $class = ''; if(osc_is_user_profile()) {$class = "active";}
        $options[] = array(
            'name' => __('Account', 'gum'),
            'url' => osc_user_profile_url(),
            'class' => 'opt_account '.$class
        );
        $class = ''; if(osc_is_change_email_page()) {$class = "active";}
        $options[] = array(
            'name' => __('Change email', 'gum'),
            'url' => osc_change_user_email_url(),
            'class' => 'opt_change_email '.$class
        );
        $class = ''; if(osc_is_change_username_page()) {$class = "active";}
        $options[] = array(
            'name' => __('Change username', 'gum'),
            'url' => osc_change_user_username_url(),
            'class' => 'opt_change_username '.$class
        );
        $class = ''; if(osc_is_change_password_page()) {$class = "active";}
        $options[] = array(
            'name' => __('Change password', 'gum'),
            'url' => osc_change_user_password_url(),
            'class' => 'opt_change_password '.$class
        );
        $options[] = array(
            'name' => __('Delete account', 'gum'),
            'url' => '#',
            'class' => 'opt_delete_account'
        );

        return $options;
    }
}


function gum_private_user_menu($options = null, $mobile = false)
{
    if($options == null) {
        $options = array();
        $options[] = array('name' => __('Public Profile', 'gum'), 'url' => osc_user_public_profile_url(), 'class' => 'opt_publicprofile');
        $options[] = array('name' => __('Dashboard', 'gum'), 'url' => osc_user_dashboard_url(), 'class' => 'opt_dashboard');
        $options[] = array('name' => __('Manage your listings', 'gum'), 'url' => osc_user_list_items_url(), 'class' => 'opt_items');
        $options[] = array('name' => __('Manage your alerts', 'gum'), 'url' => osc_user_alerts_url(), 'class' => 'opt_alerts');
        $options[] = array('name' => __('My profile', 'gum'), 'url' => osc_user_profile_url(), 'class' => 'opt_account');
        $options[] = array('name' => __('Logout', 'gum'), 'url' => osc_user_logout_url(), 'class' => 'opt_logout');
    }

    $options = osc_apply_filter('user_menu_filter', $options);

    if($mobile) { ?>
        <ul class="user_menu nav nav-pills pull-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle btn-hover" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php _e('Show menu', 'gum'); ?> <i class="fa fa-caret-down btn-ico-left"></i></a>
                <ul class="dropdown-menu dropdown-menu-right">
                <?php
                 $var_l = count($options);
                 for($var_o = 0; $var_o < ($var_l-1); $var_o++) {
                     echo '<li class="' . $options[$var_o]['class'] . '" ><a href="' . $options[$var_o]['url'] . '" >' . $options[$var_o]['name'] . '</a></li>';
                 }
                 osc_run_hook('user_menu'); ?>
                </ul>
            </li>
        </ul>
    <?php
    } else {
        echo '<ul class="user_menu nav nav-pills nav-stacked">';
        $var_l = count($options);
        for($var_o = 0; $var_o < ($var_l-1); $var_o++) {
            echo '<li class="' . $options[$var_o]['class'] . '" ><a href="' . $options[$var_o]['url'] . '" >' . $options[$var_o]['name'] . '</a></li>';
        }

        osc_run_hook('user_menu');

        echo '<li class="' . $options[$var_l-1]['class'] . '" ><a href="' . $options[$var_l-1]['url'] . '" >' . $options[$var_l-1]['name'] . '</a></li>';

        echo '</ul>';
    }

}

function gum_get_item_location() {
    $location_array = array();
    if (trim(osc_item_address()) != '') {
        if (osc_item_city_area() != '') {
            $location_array[] = osc_item_address() . ", " . osc_item_city_area();
        } else {
            $location_array[] = osc_item_address();
        }
    } else if (osc_item_city_area() != '') {
        $location_array[] = osc_item_city_area();
    }
    if (trim(osc_item_city() . " " . osc_item_zip()) != '') {
        $location_array[] = trim(osc_item_city() . " " . osc_item_zip());
    }
    if (osc_item_region() != '') {
        $location_array[] = osc_item_region();
    }
    return implode(", ", $location_array);
}

function gum_get_premium_location() {
    $location_array = array();
    if (trim(osc_premium_address()) != '') {
        if (osc_premium_city_area() != '') {
            $location_array[] = osc_premium_address() . ", " . osc_premium_city_area();
        } else {
            $location_array[] = osc_premium_address();
        }
    } else if (osc_premium_city_area() != '') {
        $location_array[] = osc_premium_city_area();
    }
    if (trim(osc_premium_city() . " " . osc_premium_zip()) != '') {
        $location_array[] = trim(osc_premium_city() . " " . osc_premium_zip());
    }
    if (osc_premium_region() != '') {
        $location_array[] = osc_premium_region();
    }
    return implode(", ", $location_array);
}

function gum_get_premium_item_location() {
    $location_array = array();
    if (trim(osc_premium_address()) != '') {
        if (osc_premium_city_area() != '') {
            $location_array[] = osc_premium_address() . ", " . osc_premium_city_area();
        } else {
            $location_array[] = osc_premium_address();
        }
    } else if (osc_premium_city_area() != '') {
        $location_array[] = osc_premium_city_area();
    }
    if (trim(osc_premium_city() . " " . osc_premium_zip()) != '') {
        $location_array[] = trim(osc_premium_city() . " " . osc_premium_zip());
    }
    if (osc_premium_region() != '') {
        $location_array[] = osc_premium_region();
    }
    return implode(", ", $location_array);
}


function gum_elapsed_time($current_date, $aprox = false) {
    $date = new DateTime();
    $date->setTimestamp(strtotime($current_date));
    $interval = $date->diff(new DateTime('now'));

    $years = $interval->format('%y');
    $months = $interval->format('%m');
    $days = $interval->format('%d');
    $hours = $interval->format('%h');
    $min = $interval->format('%i');

    $result = '';
    if ($years > 0) {
        if ($years == 1) {
            $result = sprintf(__('%d year', 'gum'), $years);
        } else {
            $result = sprintf(__('%d years', 'gum'), $years);
        }
        if($aprox) {
            return sprintf(__('%d+ years', 'gum'), $years);
        }
    }

    if ($months > 0) {
        if ($result != '') {
            $result .= ', ';
        }
        if ($months == 1) {
            $result = sprintf(__('%d month', 'gum'), $months);
        } else {
            $result = sprintf(__('%d months', 'gum'), $months);
        }
        if($aprox) {
            return sprintf(__('%d+ months', 'gum'), $months);
        }
    }



    if ($days > 0) {
        if ($result != '') {
            $result .= ', ';
        }
        if ($days == 1) {
            $result = sprintf(__('%d day', 'gum'), $days);
        } else {
            $result = sprintf(__('%d days', 'gum'), $days);
        }
        if($aprox) {
            return $result = sprintf(__('%d+ days', 'gum'), $days);
        }
    }

    if($days==0 && $months==0 && $years==0) {

        if($hours==0) {
            if($min==0) {
                $result = sprintf(__('right now', 'gum'));
            } else if($min==1) {
                $result = sprintf(__('%d minute', 'gum'), $min);
            } else {
                $result = sprintf(__('%d minutes', 'gum'), $min);
            }
        } else {
            if($hours==1) {
                $result = sprintf(__('%d hour', 'gum'), $hours);
            } else {
                $result = sprintf(__('%d hours', 'gum'), $hours);
            }
        }
        if($aprox) {
            return sprintf(__('hours', 'gum'), $days);
        }
    }


    return $result;
}

function gum_parent_categories_select($name = 'sCategory', $category = null, $default_str = null) {
    if($default_str == null) $default_str = __('Select a category', 'gum');
    CategoryForm::category_select(Category::newInstance()->findRootCategories(), $category, $default_str, $name);
}

function gum_search_orderby_select() {
    $orders = osc_list_orders();
    $aOptions = array();
    foreach($orders as $label => $params) {
        $orderType = ($params['iOrderType'] == 'asc') ? '0' : '1';
        $aux['value'] = osc_update_search_url($params);
        $aux['name'] = $label;
        if(osc_search_order() == $params['sOrder'] && osc_search_order_type() == $orderType) {
            $current = $aux['value'];
        }
        $aOptions[] = $aux;
    }
    gum_generic_select('sOrder', $aOptions, 'value', 'name', null, $current);

}

function gum_search_number() {
    $search_from = ((osc_search_page() * osc_default_results_per_page_at_search()) + 1);
    $search_to = ((osc_search_page() + 1) * osc_default_results_per_page_at_search());
    if ($search_to > osc_search_total_items()) {
        $search_to = osc_search_total_items();
    }

    return array(
        'from' => $search_from,
        'to' => $search_to,
        'of' => osc_search_total_items()
    );
}


function gum_sidebar_category_search($catId = null)
{
    $aCategories = array();
    if($catId==null) {
        $aCategories[] = Category::newInstance()->findRootCategoriesEnabled();
    } else {
        // if parent category, only show parent categories
        $aCategories = Category::newInstance()->toRootTree($catId);
        end($aCategories);
        $cat = current($aCategories);
        // if is parent of some category
        $childCategories = Category::newInstance()->findSubcategoriesEnabled($cat['pk_i_id']);
        if(count($childCategories) > 0) {
            $aCategories[] = $childCategories;
        }
    }

    if(count($aCategories) == 0) {
        return "";
    }

    gum_print_sidebar_category_search($aCategories, $catId);
}

function gum_print_sidebar_category_search($aCategories, $current_category = null, $i = 0)
{
    $class = '';
    if(!isset($aCategories[$i])) {
        return null;
    }

    if($i===0) {
    }
        $class = 'class="category"';

    $c   = $aCategories[$i];
    $i++;
    if(!isset($c['pk_i_id'])) {
        echo '<ul '.$class.'>';
        if($i==1) {
            echo '<li><a href="'.osc_esc_html(osc_update_search_url(array('sCategory'=>null, 'iPage'=>null))).'">'.__('All categories', 'gum')."<i class=\"fa fa-check\"></i></a></li>";
        }
        foreach($c as $key => $value) {
    ?>
            <li>
                <a id="cat_<?php echo osc_esc_html($value['pk_i_id']);?>" href="<?php echo osc_esc_html(osc_update_search_url(array('sCategory'=> $value['pk_i_id'], 'iPage'=>null))); ?>">
                <?php if(isset($current_category) && $current_category == $value['pk_i_id']){ echo $value['s_name'].'<i class="fa fa-check"></i>'; }
                else{ echo $value['s_name']; } ?>
                </a>
            </li>
    <?php
        }
        if($i==1) {
        echo "</ul>";
        } else {
        echo "</ul>";
        }
    } else {
    ?>
    <ul <?php echo $class;?>>
        <?php if($i==1) { ?>
        <li><a href="<?php echo osc_esc_html(osc_update_search_url(array('sCategory'=>null, 'iPage'=>null))); ?>"><?php _e('All categories', 'gum'); ?></a></li>
        <?php } ?>
            <li>
                <a id="cat_<?php echo osc_esc_html($c['pk_i_id']);?>" href="<?php echo osc_esc_html(osc_update_search_url(array('sCategory'=> $c['pk_i_id'], 'iPage'=>null))); ?>">
                <?php if(isset($current_category) && $current_category == $c['pk_i_id']){ echo $c['s_name'].'<i class="fa fa-check"></i>'; }
                      else{ echo $c['s_name']; } ?>
                </a>
                <?php gum_print_sidebar_category_search($aCategories, $current_category, $i); ?>
            </li>
        <?php if($i==1) { ?>
        <?php } ?>
    </ul>
<?php
    }
}

function gum_sidebar_location_search() {
    $aLocation = array();
    $aCountries = osc_get_countries();
    if( osc_search_country()=='' && osc_search_region()=='' && osc_search_city()=='') {
        if(count($aCountries)>1 ) {
            $aCountries = array();
            $_aCountries = osc_get_countries();
             foreach($_aCountries as $_aux) {
                $_aux['type'] = 'country';
                $aCountries[] = $_aux;
            }
            gum_print_sidebar_location_search($aCountries);
            return '';
        }
    }
    if(osc_search_city()!='') {
        $city = City::newInstance()->findByName(osc_search_city());
        $city['type'] = 'city';
        $aCities[] = $city;
        $region = Region::newInstance()->findByPrimaryKey($city['fk_i_region_id']);
        $region['type'] = 'region';
        $region['aCities'] = $aCities;
        $aRegions[] = $region;
        $country = Country::newInstance()->findByPrimaryKey($region['fk_c_country_code']);
        if(count($aCountries)>1) {
            $aLocation[] = array(
                'type' => 'location_reset',
                's_name'=> __('All countries', 'gum'),
                );
        }
        $aLocation[] = array(
            'type' => 'country',
            's_name'=> $country['s_name'],
            'aRegions' =>$aRegions
            );

    } else {
        $aRegions = array();
        if(osc_search_region()!='') {
            $region = Region::newInstance()->findByName(osc_search_region());
            $region['type'] = 'region';
            $region['aCities'] = array();

            $country = Country::newInstance()->findByPrimaryKey($region['fk_c_country_code']);

            // get cities
            $aCities = array();
            $_aCities= osc_get_cities( $region['pk_i_id'] );
            // --------
            View::newInstance()->_exportVariableToView('list_cities', CityStats::newInstance()->listCities($region['pk_i_id']) );
            while(osc_has_list_cities()) {
                $_c = array(
                    'type' => 'city',
                    's_name' => osc_list_city_name()
                    );
                $aCities[] = $_c;
            }
            $region['aCities'] = $aCities;
            $aRegions[] = $region;
            if(count($aCountries)>1) {
                $aLocation[] = array(
                    'type' => 'location_reset',
                    's_name'=> __('All countries', 'gum'),
                );
            }
            $aLocation[] = array(
                'type' => 'country',
                's_name'=> $country['s_name'],
                'aRegions' => $aRegions
                );
        }
        if((osc_search_country()!='' || count($aCountries)<=1)  && osc_search_region()=='' && osc_search_city()=='') {
            if(count($aCountries)==1) {
                $country = Country::newInstance()->findByPrimaryKey($aCountries[0]['pk_c_code']);
            } else {
                $country = Country::newInstance()->findByName(osc_search_country());
            }
            View::newInstance()->_exportVariableToView('list_regions', RegionStats::newInstance()->listRegions($country['pk_c_code']) );
            while(osc_has_list_regions()) {
                $_r = array(
                    'type' => 'region',
                    's_name' => osc_list_region_name()
                    );
                $aRegions[] = $_r;
            }
            if(count($aCountries)>1) {
                $aLocation[] = array(
                    'type' => 'location_reset',
                    's_name'=> __('All countries', 'gum'),
                );
                $aLocation[] = array(
                    'type' => 'country',
                    's_name'=> $country['s_name'],
                    'aRegions' => $aRegions
                );
            }
            if(count($aCountries)==1) {
                $aLocation =  $aRegions;
            }
        }
    }
    gum_print_sidebar_location_search($aLocation);
}

function gum_print_sidebar_location_search($aLocation) {
    if(count($aLocation)>0) {
        echo '<ul class="location">';
        foreach($aLocation as $key => $value) {
            $active = false;
            $url = '';
            if($value['type']=='location_reset') {
                $url = osc_update_search_url(array(
                    'sCountry' => null,
                    'sRegion' => null,
                    'sCity' => null));

            } elseif($value['type']=='country') {
                $url = osc_update_search_url(array(
                    'sCountry' => $value['s_name'],
                    'sRegion' => null,
                    'sCity' => null));
                if( ($value['s_name']==Params::getParam('sCountry') || $value['s_name']==osc_search_country())
                    && osc_search_region()=='' && osc_search_city()==''){
                    $active = true;
                }
            } elseif($value['type']=='region') {
                $url = osc_update_search_url(array(
                    'sCountry' => null,
                    'sRegion' => $value['s_name'],
                    'sCity' => null
                    ));
                if( ($value['s_name']==Params::getParam('sRegion') || $value['s_name']==osc_search_region())
                    && osc_search_city()==''){
                    $active = true;
                }
            } elseif($value['type']=='city') {
                $url = osc_update_search_url(array(
                    'sCountry' => null,
                    'sRegion' => null,
                    'sCity' => $value['s_name']
                    ));
                if( ($value['s_name']==Params::getParam('sCity') || $value['s_name']==osc_search_city()) ){
                    $active = true;
                }
            }
            ?>
            <li>
                <a href="<?php echo $url; ?>"><?php echo $value['s_name']; ?>
                    <?php if($active) { echo '<i class="fa fa-check"></i>'; } ?>
                </a>
                <?php if(isset($value['aRegions']) && count($value['aRegions'])>0) {
                    gum_print_sidebar_location_search($value['aRegions']);
                } ?>
                <?php if(isset($value['aCities']) && count($value['aCities'])>0) {
                    gum_print_sidebar_location_search($value['aCities']);
                } ?>
            </li>
            <?php
        }
        echo '</ul>';
    }
}

/* pagination */
function gum_search_pagination()
{
    $params = array();
    $params['list_class'] = 'pagination';
    if( View::newInstance()->_exists('search_uri') ) { // CANONICAL URL
        $params['url'] = osc_base_url().View::newInstance()->_get('search_uri') . '/{PAGE}';
        $params['first_url'] = osc_base_url().View::newInstance()->_get('search_uri');
    } else {
        $params['first_url'] = osc_update_search_url(array('iPage' => null));
    }
    $pagination = new Pagination($params);
    return $pagination->doPagination();
}

function gum_pagination_items($extraParams = array(), $field = false)
{
    if(osc_is_public_profile()) {
        $url = osc_user_list_items_pub_profile_url('{PAGE}', $field);
        $first_url = osc_user_public_profile_url();
    } elseif(osc_is_list_items()) {
        $url = osc_user_list_items_url('{PAGE}', $field);
        $first_url = osc_user_list_items_url();
    }

    $params = array('total'    => osc_search_total_pages(),
                    'selected' => osc_search_page(),
                    'url'      => $url,
                    'list_class' => 'pagination',
                    'first_url' => $first_url
              );
    if(is_array($extraParams) && !empty($extraParams)) {
        foreach($extraParams as $key => $value) {
            $params[$key] = $value;
        }
    }
    $pagination = new Pagination($params);
    return $pagination->doPagination();
}

function gum_generic_select($name, $items, $fld_key, $fld_name, $default_item, $id) {
    $name = osc_esc_html($name);
    echo '<select name="' . $name . '" id="' . preg_replace('|([^_a-zA-Z0-9-]+)|', '', $name) . '">';
    if (isset($default_item)) echo '<option value="">' . $default_item . '</option>';
    foreach($items as $i) {
        if(isset($fld_key) && isset($fld_name))
        echo '<option value="' . osc_esc_html($i[$fld_key]) . '"' . ( ($id == $i[$fld_key]) ? ' selected="selected"' : '' ) . '>' . $i[$fld_name] . '</option>';
    }
    echo '</select>';
}

function gum_show_flash_message($section = 'pubMessages', $class = "flashmessage", $id = "flashmessage") {
        $messages = Session::newInstance()->_getMessage($section);
        if (is_array($messages)) {

            foreach ($messages as $message) {

                echo '<div id="flash_js"></div>';
                $_class_fm = 'bg-';
                switch ($message['type']) {
                    case 'ok':
                        $_class_fm .= 'success';
                        break;
                    case 'info':
                        $_class_fm .= 'info';
                        break;

                    default:
                        $_class_fm .= 'danger';
                        break;
                }

                if (isset($message['msg']) && $message['msg'] != '') {
                    echo ' <div class="container-fluid  '.$_class_fm .'">';
                    echo ' <div class="container">';
                    echo '<div id="' . $id . '" class="' . strtolower($class) . '"><a class="btn ico btn-mini ico-close">x</a>';
                    echo osc_apply_filter('flash_message_text', $message['msg']);
                    echo ' </div>';
                    echo '</div>';
                    echo '</div>';
                } else if($message!='') {
                    echo '<div id="' . $id . '" class="' . $class . '">';
                    echo ' <div class="container '.$_class_fm .'">';
                    echo osc_apply_filter('flash_message_text', $message);
                    echo ' </div>';
                    echo '</div>';
                } else {
                    echo '<div id="' . $id . '" class="' . $class . '" style="display:none;">';
                    echo ' <div class="container '.$_class_fm .'">';
                    echo osc_apply_filter('flash_message_text', '');
                    echo '</div>';
                    echo '</div>';
                }
            }
        }
        Session::newInstance()->_dropMessage($section);
    }

    function gum_default_location_show_as(){
        return osc_get_preference('defaultLocationShowAs','gum_theme');
    }

    function gum_print_social_links() {
        if(osc_get_preference('footer_social_networks', 'gum_theme')) {
            $target = '';
            if(osc_get_preference('footer_social_links_target', 'gum_theme')) {
                $target = 'target="_blank"';
            }
            echo "<p>";
            if(trim(osc_get_preference('footer_blog_url', 'gum_theme'))!="") {
                echo '<a '.$target.' href="'.osc_esc_html(osc_get_preference('footer_blog_url', 'gum_theme')).'" title="" class="btn btn-social-icon btn-lg btn-openid"><i class="fa fa-rss"></i></a>';
            }
            if(trim(osc_get_preference('footer_facebook_url', 'gum_theme'))!="") {
                echo '<a '.$target.' href="'.osc_esc_html(osc_get_preference('footer_facebook_url', 'gum_theme')).'" title="" class="btn btn-social-icon btn-lg btn-facebook"><i class="fa fa-facebook"></i></a>';
            }
            if(trim(osc_get_preference('footer_twitter_url', 'gum_theme'))!="") {
                echo '<a '.$target.'  href="'.osc_esc_html(osc_get_preference('footer_twitter_url', 'gum_theme')).'" title="" class="btn btn-social-icon btn-lg btn-twitter"><i class="fa fa-twitter"></i></a>';
            }
            if(trim(osc_get_preference('footer_gplus_url', 'gum_theme'))!="") {
                echo '<a '.$target.' href="'.osc_esc_html(osc_get_preference('footer_gplus_url', 'gum_theme')).'" title="" class="btn btn-social-icon btn-lg btn-google"><i class="fa fa-google"></i></a>';
            }
            if(trim(osc_get_preference('footer_pinterest_url', 'gum_theme'))!="") {
                echo '<a '.$target.' href="'.osc_esc_html(osc_get_preference('footer_pinterest_url', 'gum_theme')).'" title="" class="btn btn-social-icon btn-lg btn-pinterest" ><i class="fa fa-pinterest"></i></a>';
            }
            echo "</p>";
        }
    }

    if( !function_exists('osc_uploads_url')) {
        function osc_uploads_url($item = '') {
            if ($item != '' && file_exists(osc_uploads_path() . $item)) {
                $path = str_replace(ABS_PATH, '', osc_uploads_path() . '/');
                return osc_base_url() . $path . $item;
            }
        }
    }

    /* logo / favicon / banner*/
    if( !function_exists('gum_logo_url') ) {
        function gum_logo_url() {
            $logo = osc_get_preference('logo','gum_theme');
            if( $logo ) {
                return osc_uploads_url($logo);
            }
            return osc_current_web_theme_url('gum_logo.png');
        }
    }
    if( !function_exists('gum_favicon_url') ) {
        function gum_favicon_url() {
            $fav = osc_get_preference('favicon','gum_theme');
            if( $fav ) {
                return osc_uploads_url($fav);
            }
            return osc_base_url().'favicon.ico';
        }
    }
    if( !function_exists('gum_homebanner_url') ) {
        function gum_homebanner_url() {
            $banner = osc_get_preference('homebanner','gum_theme');
            if( $banner ) {
                return osc_uploads_url($banner);
            }
            return osc_current_web_theme_url('gum_homebanner.png');
        }
    }

    /**
     * return true if listing is saved by osc_user()
     */
    function gum_is_saved() {
        if(function_exists('watchlist') && osc_logged_user_id()!='') {
            $conn   = getConnection();
            $detail = $conn->osc_dbFetchResult("SELECT * FROM %st_item_watchlist WHERE fk_i_item_id = %d and fk_i_user_id = %d", DB_TABLE_PREFIX, osc_item_id(), osc_logged_user_id());
            if (!isset($detail['fk_i_item_id'])) {
                return false;
            }
            return true;
        }
        return false;
    }