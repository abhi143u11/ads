<?php

function gum_draw_item($displayAs = 'grid') {
    $class = 'listing-grid col-md-4 col-sm-4 col-ms-4 col-xs-6';
    $class_wrapper = 'listing-grid-wrapper';
    if($displayAs!='grid') {
        $class = 'listing-list col-xs-12';
    $class_wrapper = 'listing-list-wrapper';
    }

    // item['active'] = 0
    if(!osc_item_is_active()) {
        $class .= " alert alert-danger";
    }
?>
<div class="<?php echo $class; ?> <?php if($displayAs!='grid') { osc_run_hook("highlight_class"); } ?>">
    <a class="listing-url" href="<?php echo osc_item_url() ; ?>" title="<?php echo osc_esc_html(osc_item_title()) ; ?>">
        <div class="<?php echo $class_wrapper; ?>">
            <?php if( osc_images_enabled_at_items() ) { ?>
                <?php if(osc_count_item_resources()) { ?>
            <div class="listing-thumb"><img class="img-responsive" src="<?php echo osc_resource_thumbnail_url(); ?>" alt="<?php echo osc_esc_html(osc_item_title()) ; ?>" />
                <?php } else { ?>
            <div class="listing-thumb"><img class="img-responsive" src="<?php echo osc_current_web_theme_url('images/no_photo.gif'); ?>" alt="<?php echo osc_esc_html(osc_item_title()) ; ?>" />
                <?php } ?>
            </div>
            <?php } ?>
            <?php if( osc_price_enabled_at_items() && $displayAs=='grid' ) { ?><span class="listing-simple-price"><?php echo osc_format_price(osc_item_price()); ?></span><?php } ?>
            <?php if(osc_count_item_resources()>0) { ?>
            <div class="listing-thumb-info text-center"><?php echo osc_count_item_resources(); ?> <i class="fa fa-camera"></i></div>
            <?php } ?>
        </div>
        <div class="listing-content">
            <h2 class="listing-title"><?php if(osc_item_is_premium()) { ?><span class="isPremium"><?php _e('featured', 'gum'); ?></span><?php } ?><?php echo osc_item_title(); ?></h2>
            <div class="listing-description"><?php echo osc_highlight( osc_item_description() ,250) ; ?></div>
            <div class="listing-location truncate-line">
                <span itemprop="name">
                    <?php echo gum_get_item_location();?>
                </span>
            </div>

            <?php if( osc_price_enabled_at_items() && $displayAs=='list' ) { ?>
            <strong class="listing-simple-price truncate-line"><?php echo osc_format_price(osc_item_price()); ?></strong>
            <?php } ?>
            <span class="listing-posted-date truncate-line text-right">
                <?php printf(__('%s ago', 'gum'),  gum_elapsed_time(osc_item_pub_date()) );?>
            </span>

            <?php if(osc_is_list_items() && osc_logged_user_id()==osc_item_user_id()) { ?>
            <div class="listing-actions">
                <p><a href="#" class="nolink"><i class="fa fa-phone-square"></i> <?php printf( __('Phone number shown %d times', 'gum'), GumModelItemPhoneReveal::newInstance()->getTotalViewPhoneByItemId(osc_item_id()) ); ?></a></p>
                <a class="btn btn-default btn-xs" href="<?php echo osc_item_edit_url();?>"><?php _e('Edit', 'gum'); ?></a>
                <a class="btn btn-default btn-xs" onclick="javascript:return confirm('<?php echo osc_esc_js(__('This action can not be undone. Are you sure you want to continue?', 'gum')); ?>')"  href="<?php echo osc_item_delete_url();?>"><?php _e('Delete', 'gum'); ?></a>
                <?php if (osc_item_is_inactive()) { ?>
                <a class="btn btn-success btn-xs" href="<?php echo osc_ajax_hook_url('activate_item').'&id='.osc_item_id(); ?>" ><?php _e('Activate', 'gum'); ?></a>
                <?php } else { ?>
                <a class="btn btn-warning btn-xs" href="<?php echo osc_ajax_hook_url('deactivate_item').'&id='.osc_item_id(); ?>" ><?php _e('Deactivate', 'gum'); ?></a>
                <?php } ?>
            </div>
            <?php } ?>
            <?php $options  = __get('gum_payments_pro_options');
                if($options != array() && is_array($options) && class_exists('ModelPaymentPro')) { ?>
                <div class="listing-actions">
                    <?php if(ModelPaymentPro::newInstance()->isEnabled(osc_item_id())) {
                        if(count($options)>0) {
                            echo '<p class="options">' . join("<span>&nbsp;&nbsp;</span>", $options) . '</p>';
                        }
                    } else { ?>
                        <p class="options">
                            <strong><?php _e('This listing is blocked', 'payment_pro'); ?></strong>
                        </p>
                    <?php } ?>
                </div>
                <?php } ?>
        </div>
        <div class="clearfix"></div>
    </a>
    <?php if(osc_is_search_page() && function_exists('watchlist')  ) {
        $fav_class = '';
        if(gum_is_saved()) { $fav_class = 'is-saved'; }
        ?>
    <span class="save-ad watchlist_ico <?php echo $fav_class; ?>" id="<?php echo osc_item_id(); ?>">
        <span class="text-hide"><?php _e('Save this ad', 'gum'); ?></span>
        <i class="fa fa-star"></i>
    </span>
    <?php } ?>
    <?php if(__get('is_watchlist')!='' && function_exists('watchlist')) { ?>
    <a class="save-ad is-saved" href="<?php echo osc_render_file_url('watchlist/watchlist.php') . '&delete=' . osc_item_id(); ?>">
        <span class="text-hide"><?php _e('Delete this ad', 'gum'); ?></span>
        <i class="fa fa-star"></i>
    </a>
    <?php } ?>
</div>
<?php }

/*
 *
 * Premium helpers used - same as gum_draw_item
 *
 */
function gum_draw_itemPremium($displayAs = 'grid') {
    $class = 'listing-grid col-md-4 col-sm-4 col-ms-4 col-xs-6';
    $class_wrapper = 'listing-grid-wrapper ';
    if($displayAs!='grid') {
        $class = 'listing-list col-xs-12';
        $class_wrapper = 'listing-list-wrapper ';
    }

    // item['active'] = 0
    if(!osc_premium_is_active()) {
        $class .= " alert alert-danger";
    }
?>
<div class="<?php echo $class; ?>  <?php osc_run_hook("highlight_class"); ?>">
    <a class="listing-url " href="<?php echo osc_premium_url() ; ?>" title="<?php echo osc_esc_html(osc_premium_title()) ; ?>">
        <div class="<?php echo $class_wrapper; ?>">
            <?php if( osc_images_enabled_at_items() ) { ?>
                <?php if(osc_count_premium_resources()) { ?>
            <div class="listing-thumb"><img class="img-responsive" src="<?php echo osc_resource_thumbnail_url(); ?>" alt="<?php echo osc_esc_html(osc_premium_title()) ; ?>" />
                <?php } else { ?>
            <div class="listing-thumb"><img class="img-responsive" src="<?php echo osc_current_web_theme_url('images/no_photo.gif'); ?>" alt="<?php echo osc_esc_html(osc_premium_title()) ; ?>" />
                <?php } ?>
            </div>
            <?php } ?>
            <?php if( osc_price_enabled_at_items() && $displayAs=='grid' ) { ?><span class="listing-simple-price"><?php echo osc_format_price(osc_premium_price()); ?></span><?php } ?>
            <?php if(osc_count_premium_resources()>0) { ?>
            <div class="listing-thumb-info text-center"><?php echo osc_count_premium_resources(); ?> <i class="fa fa-camera"></i></div>
            <?php } ?>
        </div>
        <div class="listing-content">
            <h2 class="listing-title"><?php if(osc_premium_is_premium()) { ?><span class="isPremium"><?php _e('featured', 'gum'); ?></span><?php } ?><?php echo osc_premium_title(); ?></h2>
            <div class="listing-description"><?php echo osc_highlight( osc_premium_description() ,250) ; ?></div>
            <div class="listing-location truncate-line">
                <span itemprop="name">
                    <?php echo gum_get_premium_location();?>
                </span>
            </div>

            <?php if( osc_price_enabled_at_items() && $displayAs=='list' ) { ?>
            <strong class="listing-simple-price truncate-line"><?php echo osc_format_price(osc_premium_price()); ?></strong>
            <?php } ?>
            <span class="listing-posted-date truncate-line text-right">
                <?php printf(__('%s ago', 'gum'),  gum_elapsed_time(osc_premium_pub_date()) );?>
            </span>

            <?php if(osc_is_list_items() && osc_logged_user_id()==osc_item_user_id()) { ?>
            <div class="listing-actions">
                <p><a href="#" class="nolink"><i class="fa fa-phone-square"></i> <?php printf( __('Phone number shown %d times', 'gum'), GumModelItemPhoneReveal::newInstance()->getTotalViewPhoneByItemId(osc_item_id()) ); ?></a></p>
                <a class="btn btn-default btn-xs" href="<?php echo osc_item_edit_url();?>"><?php _e('Edit', 'gum'); ?></a>
                <a class="btn btn-default btn-xs" onclick="javascript:return confirm('<?php echo osc_esc_js(__('This action can not be undone. Are you sure you want to continue?', 'gum')); ?>')"  href="<?php echo osc_item_delete_url();?>"><?php _e('Delete', 'gum'); ?></a>
                <?php if (osc_item_is_inactive()) { ?>
                <a class="btn btn-success btn-xs" href="<?php echo osc_ajax_hook_url('activate_item').'&id='.osc_item_id(); ?>" ><?php _e('Activate', 'gum'); ?></a>
                <?php } else { ?>
                <a class="btn btn-warning btn-xs" href="<?php echo osc_ajax_hook_url('deactivate_item').'&id='.osc_item_id(); ?>" ><?php _e('Deactivate', 'gum'); ?></a>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
    </a>
    <?php if(osc_is_search_page() && function_exists('watchlist')  ) {
        $fav_class = '';
        if(gum_is_saved()) { $fav_class = 'is-saved'; }
        ?>
    <span class="save-ad watchlist_ico <?php echo $fav_class; ?>" id="<?php echo osc_premium_id(); ?>">
        <span class="text-hide"><?php _e('Save this ad', 'gum'); ?></span>
        <i class="fa fa-star"></i>
    </span>
    <?php } ?>
    <?php if(__get('is_watchlist')!='' && function_exists('watchlist')) { ?>
    <a class="save-ad is-saved" href="<?php echo osc_render_file_url('watchlist/watchlist.php') . '&delete=' . osc_premiun_id(); ?>">
        <span class="text-hide"><?php _e('Delete this ad', 'gum'); ?></span>
        <i class="fa fa-star"></i>
    </a>
    <?php } ?>
</div>
<?php }


function gum_draw_item_premium() { ?>
<div class="listing-grid listing-grid-premium">
    <div class="listing-grid-wrapper">
        <a href="<?php echo osc_premium_url() ; ?>" title="<?php echo osc_esc_html(osc_premium_title()) ; ?>">
        <?php if( osc_images_enabled_at_items() ) {  ?>
            <div class="listing-thumb">
                <?php if(osc_count_premium_resources()) { osc_has_premium_resources(); ?>
                    <img  class="img-responsive" src="<?php echo osc_resource_thumbnail_url(); ?>" alt="<?php echo osc_esc_html(osc_premium_title()) ; ?>" />
                <?php } else { ?>
                    <img  class="img-responsive" src="<?php echo osc_current_web_theme_url('images/no_photo.gif'); ?>" alt="<?php echo osc_esc_html(osc_premium_title()) ; ?>" />
                <?php }
                if( osc_price_enabled_at_items() ) { ?>
                    <span class="listing-simple-price"><?php echo osc_format_price(osc_item_price()); ?></span>
                <?php } ?>
            <?php } ?>
            </div>
            <div class=" listing-content">
                <h2 class="listing-title">
                    <?php echo osc_premium_title(); ?>
                </h2>
                <div class="listing-location truncate-line">
                    <span itemprop="name">
                        <?php echo gum_get_premium_item_location();?>
                    </span>
                </div>
                <strong class="listing-posted-date truncate-line ">
                    <span class="pull-left_"><?php printf(__('%s ago', 'gum'),  gum_elapsed_time(osc_item_pub_date()) );?></span>
                </strong>
            </div>
        </a>
    </div>
</div>
<?php }

function gum_related_listings() {
    $mSearch = new Search();
    $mSearch->addCategory(osc_item_category_id());
    $mSearch->addRegion(osc_item_region());
    $mSearch->addItemConditions(sprintf("%st_item.pk_i_id < %s ", DB_TABLE_PREFIX, osc_item_id()));
    $mSearch->limit('0', '9');

    $aItems = $mSearch->doSearch();
    if( count($aItems) == 9 ) {
        View::newInstance()->_exportVariableToView('premiums', $aItems);
        return count($aItems);
    }
    unset($mSearch);

    $mSearch = new Search();
    $mSearch->addCategory(osc_item_category_id());
    $mSearch->addItemConditions(sprintf("%st_item.pk_i_id < %s ", DB_TABLE_PREFIX, osc_item_id()));
    $mSearch->limit('0', '9');

    $aItems = $mSearch->doSearch();
    if( count($aItems) == 9 ) {
        View::newInstance()->_exportVariableToView('premiums', $aItems);
        return count($aItems);
    }
    unset($mSearch);

    $mSearch = new Search();
    $mSearch->addCategory(osc_item_category_id());
    $mSearch->addItemConditions(sprintf("%st_item.pk_i_id != %s ", DB_TABLE_PREFIX, osc_item_id()));
    $mSearch->limit('0', '9');

    $aItems = $mSearch->doSearch();
    if( count($aItems) > 0 ) {
        View::newInstance()->_exportVariableToView('premiums', $aItems);
        return count($aItems);
    }
    unset($mSearch);

    return 0;
}

function gum_draw_alert($i = 1) {
//    $class = 'listing-grid col-md-4 col-sm-4 col-ms-4 col-xs-6';
//    $class_wrapper = 'listing-grid-wrapper';
//    if($displayAs!='grid') {
        $class = 'listing-list col-xs-12';
        $class_wrapper = 'listing-list-wrapper';
//    }
    $title = '';
    $alert = json_decode(osc_alert_search(), true);
    // title alert
    if($alert['sPattern']!='') {
        $title .= "'".$alert['sPattern']."' ".__('in', 'gum') . ' ';
    }

    if(count($alert['aCategories'])>0) {
        $cat_id = array_shift( $alert['aCategories'] );
        $_c = Category::newInstance()->findByPrimaryKey($cat_id);
        if(isset($_c['s_name'])) {
            $title .= $_c['s_name'];
        }
    } else {
        $title .= __('All categories', 'gum');
    }
    // location params ?
    $location = '';
    if(count($alert['cities'])>0) {
        $_city_id = array_shift( $alert['cities'] );
        $_city = City::newInstance()->findByPrimaryKey($_city_id);
        if(isset($_city['s_name'])) {
            $location = $_city['s_name'];
        }
    }
    if($location=='' && count($alert['regions'])>0) {
         $_city_id = array_shift( $alert['cities'] );
        $_city = City::newInstance()->findByPrimaryKey($_city_id);
        if(isset($_city['s_name'])) {
            $location = $_city['s_name'];
        }
    }
    if($location=='' && count($alert['countries'])>0) {
        $_country_code = array_shift( $alert['countries'] );
        $_country = Country::newInstance()->findByPrimaryKey($_country_code);
        if(isset($_country['s_name'])) {
            $location = $_country['s_name'];
        }
    } else {
        $aCountryNames = array();
        $allCountries = osc_get_countries();
        foreach($allCountries as $_country) {
            $aCountryNames[] = $_country['s_name'];
        }
        $location = implode(', ', $aCountryNames);
    }
?>
<div class="listing-list user-alerts <?php echo ($i==0) ? "first": ''; ?>">
    <div class="listing-url">
        <div class="listing-content">
            <h2 class="listing-title"><?php echo $title; ?></h2>
            <div class="listing-description">
                <?php echo $location; ?>
            </div>
            <div class="pull-left btn-group">
                <?php if(osc_alert_is_active()) { ?>
                <a  class="btn btn-default btn-sm" href="<?php echo osc_ajax_hook_url('deactivate_alert').'&id='.osc_alert_id(); ?>"><?php _e('Disable alert', 'gum'); ?></a>
                <?php } else { ?>
                <a  class="btn btn-default btn-sm" href="<?php echo osc_ajax_hook_url('activate_alert').'&id='.osc_alert_id(); ?>"><?php _e('Enable alert', 'gum'); ?></a>
                <?php } ?>
            </div>
            <a  class="pull-right btn btn-default btn-mini" onclick="javascript:return confirm('<?php echo osc_esc_js(__('This action can\'t be undone. Are you sure you want to continue?', 'gum')); ?>');" href="<?php echo osc_user_unsubscribe_alert_url(); ?>"><?php _e('Delete this alert', 'gum'); ?></a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<?php }


function gum_item_title() {
    $title = osc_item_title();
    foreach (osc_get_locales() as $locale) {
        if (Session::newInstance()->_getForm('title') != "") {
            $title_ = Session::newInstance()->_getForm('title');
            if (@$title_[$locale['pk_c_code']] != "") {
                $title = $title_[$locale['pk_c_code']];
            }
        }
    }
    return $title;
}

function gum_item_description() {
    $description = osc_item_description();
    foreach (osc_get_locales() as $locale) {
        if (Session::newInstance()->_getForm('description') != "") {
            $description_ = Session::newInstance()->_getForm('description');
            if (@$description_[$locale['pk_c_code']] != "") {
                $description = $description_[$locale['pk_c_code']];
            }
        }
    }
    return $description;
}
