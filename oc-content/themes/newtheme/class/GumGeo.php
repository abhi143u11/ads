<?php

class GumGeo {

    public function __construct()
    {
        osc_add_hook('show_item', array(&$this,'listing_update_lat_long') );
    }

    function listing_update_lat_long() {
        $aItem = Item::newInstance()->findByPrimaryKey(osc_item_id());
        $location_string = $this->_get_listing_location_string($aItem);
        if(osc_is_ad_page() && osc_item_latitude()==0 && osc_item_longitude()==0 && trim($location_string)!='') {
            $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($location_string) . "&sensor=true_or_false";
            $content = file_get_contents($url);
            if (json_encode($content)) {
                $json = json_decode($content, true);
                if (isset($json['results'][0]) &&
                    isset($json['results'][0]['geometry']['location']['lat']) &&
                    isset($json['results'][0]['geometry']['location']['lng'])) {

                    $lat = $json['results'][0]['geometry']['location']['lat'];
                    $long = $json['results'][0]['geometry']['location']['lng'];

                    ItemLocation::newInstance()->update(
                        array('d_coord_lat' => $lat, 'd_coord_long' => $long),
                        array('fk_i_item_id' => osc_item_id())
                    );
                    // force redirect
                    osc_redirect_to(osc_item_url());
                }
            }
        }
    }


    static function _get_listing_location_string($aItem) {
        $aux_item = View::newInstance()->_get('item');
        View::newInstance()->_exportVariableToView('item', $aItem);

        $location = array();
        if (osc_item_city_area() !== '') {
            $location[] = osc_item_city_area();
        }
        if (osc_item_city() !== '') {
            $location[] = osc_item_city();
        }
        if (osc_item_region() !== '') {
            $location[] = osc_item_region();
        }
        if (osc_item_country() !== '') {
            $location[] = osc_item_country();
        }

        View::newInstance()->_exportVariableToView('item', $aux_item);
        return implode(', ', $location);
    }
}
$_GumGeo = new GumGeo();