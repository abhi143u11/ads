jQuery(document).ready(function($) {
    $(".watchlist_ico").click(function() {
        var id = $(this).attr("id");
        var dataString = 'id='+ id ;
        var that = this;
        $.ajax({
            type: "POST",
            url: watchlist_url,
            data: dataString,
            cache: false,
            dataType: 'json',
            success: function(data) {
                if(data.text==='') {
                    // redirect login page
                } else {
                    $(that).addClass('is-saved');
                }
            }
        });
    });
});