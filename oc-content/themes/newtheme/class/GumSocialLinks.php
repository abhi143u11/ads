<?php

class GumSocialLinks {
    // @ todo pinterest

    // @todo google plus

    static function facebook_url() {
        if (osc_is_public_profile()) {
            $fb = "javascript:window.open('http://facebook.com/sharer/sharer.php?u='+encodeURIComponent('" . osc_user_public_profile_url()  . "'), '_blank', 'width=400,height=500');void(0);";
        }
        if (osc_is_ad_page()) {
            $fb = "javascript:window.open('http://facebook.com/sharer/sharer.php?u='+encodeURIComponent('" . osc_item_url() . "') , '_blank', 'width=400,height=500');void(0);";
        }
        return $fb;
    }

    static function twitter_url() {
        if (osc_is_public_profile()) {
            $text = sprintf(__("Check out this profile with interesting products at %1\$s. %2$", 'gum'), osc_page_title(), osc_user_public_profile_url());
        }
        if (osc_is_ad_page()) {
            $text = sprintf(__("Check out what I just found at %1\$s. %2$", 'gum'), osc_page_title(), osc_item_url());
        }
        $tw = "javascript:window.open('http://twitter.com/share?text='+encodeURIComponent('" . osc_esc_js(urldecode($text) ). "'), '_blank', 'width=400,height=500');void(0);";
        return $tw;
    }

    static function email_url() {
        if (osc_is_public_profile()) {
            $subject = (sprintf(__("Check out this profile with interesting products at %s.", 'gum'), osc_page_title()));
            $body = (sprintf(__('Take a look at %1$s\'s profile on %2$s', 'gum'), osc_user_public_profile_url(), osc_page_title()));
        }
        if (osc_is_ad_page()) {
            $subject = (sprintf(__("Check out what I just found < %s >", 'gum'), osc_item_title()));
            $body = (sprintf(__('Take a look at %s', 'gum'), osc_item_url()));
        }
        return 'mailto:?body=' . rawurlencode($body) . '&subject=' . rawurlencode($subject);
    }

}