<?php

class GumAssets {

    var $location;
    var $section;

    public function __construct()
    {
        $this->location = Rewrite::newInstance()->get_location();
        $this->section  = Rewrite::newInstance()->get_section();

        osc_add_hook('header', array(&$this, 'enqueue_css'), 1);
        osc_add_hook('header', array(&$this, 'enqueue_js'));

    }


    function enqueue_css()
    {
        $location = Rewrite::newInstance()->get_location();
        $section = Rewrite::newInstance()->get_section();

        if ($location === 'user' && in_array($section, array('dashboard', 'profile', 'alerts', 'change_email', 'change_username', 'change_password', 'items'))) {
            $user = User::newInstance()->findByPrimaryKey(Session::newInstance()->_get('userId'));
            View::newInstance()->_exportVariableToView('user', $user);
            ?>
            <script type="text/javascript">
                var gum_theme = window.gum_theme || {};
                gum_theme.user = {};
                gum_theme.user.id = '<?php echo osc_user_id(); ?>';
                gum_theme.user.secret = '<?php echo osc_user_field("s_secret"); ?>';
            </script>
            <?php
        }
        // osc_enqueue_style('bootstrap', osc_current_web_theme_url() . 'lib/bootstrap/css/bootstrap.min.css');
        // customized bootstrap with 480px @media
        // https://gist.github.com/wdollar/135ec3c80faaf5a821b0
        osc_enqueue_style('bootstrap', osc_current_web_theme_url() . 'css/bootstrap.min.css');

        osc_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
        osc_enqueue_style('bootstrap-social', osc_current_web_theme_url() . 'css/bootstrap-social.css');

        osc_enqueue_style('bxslider-css', osc_current_web_theme_url() . 'lib/bxslider/css/jquery.bxslider.min.css');
        osc_enqueue_style('sweetalert-css', osc_current_web_theme_url() . 'js/sweetalert-master/dist/sweetalert.css');
        osc_enqueue_style('style', osc_current_web_theme_url() . 'css/style.css');

        osc_enqueue_style('style-jquery-ui', osc_current_web_theme_url() . 'lib/jquery-ui/jquery-ui.css');
    }

    function enqueue_js()
    {
        osc_register_script('jquery', osc_current_web_theme_url() . 'lib/jquery/js/jquery-1.12.0.min.js');
        osc_enqueue_script('jquery');
        osc_register_script('jquery-ui', osc_current_web_theme_url() . 'lib/jquery-ui/jquery-ui.js', 'jquery');
        osc_enqueue_script('jquery-ui');
        osc_enqueue_script('php-date');

        osc_register_script('global-js', osc_current_web_theme_url() . 'js/global.js', 'jquery');
        osc_enqueue_script('global-js');

        osc_register_script('bootstrap-js', osc_current_web_theme_url() . 'lib/bootstrap/js/bootstrap.min.js', 'jquery');
        osc_enqueue_script('bootstrap-js');

        osc_register_script('bxslider-js', osc_current_web_theme_url() . 'lib/bxslider/js/jquery.bxslider.min.js', 'jquery');
        osc_enqueue_script('bxslider-js');

        osc_register_script('gmaps-js', 'https://maps.googleapis.com/maps/api/js');

        osc_register_script('sweetalert-js', osc_current_web_theme_url() . 'js/sweetalert-master/dist/sweetalert.min.js');
        osc_enqueue_script('sweetalert-js');

        if(osc_is_ad_page()) {
            osc_enqueue_script('gmaps-js');
        }

        osc_register_script('delete-user-js', osc_current_web_theme_js_url('delete_user.js'), 'jquery-ui');

        if( ($this->location === 'user' &&
            in_array($this->section,array( 'dashboard', 'profile', 'alerts', 'change_email', 'change_username',  'change_password', 'items')) )
            || (Params::getParam('page') ==='custom' && Params::getParam('in_user_menu')==true ) ) {
            osc_enqueue_script('delete-user-js');
        }
    }
}