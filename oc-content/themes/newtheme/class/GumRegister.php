<?php

class GumRegister {
    public function __construct()
    {
        // update s_username using s_name
        osc_add_hook('user_register_completed', array(&$this,'update_userName') );
        // check repeat email field
        osc_add_hook('before_user_register', array(&$this,'check_email') );
    }

    function update_userName($userId) {
        $user = User::newInstance()->findByPrimaryKey($userId);
        if(isset($user['pk_i_id'])) {
            $username = osc_sanitize_username($user['s_name'].'_'.$userId);
            // check if username already exist
            $username_taken = User::newInstance()->findByUsername($username);
            if($username_taken == false ) {
                User::newInstance()->update(
                                     array('s_username' => $username)
                                    ,array('pk_i_id'  => $userId)
                    );
            }
        }
    }

    function check_email() {
        if(Params::getParam('s_email')!=Params::getParam('s_email_2')) {
            osc_add_filter('user_add_flash_error', function($fm) {
                if($fm!='') {
                    $fm .= "\n";
                }
                $fm .= __("Emails don't match", 'gum');
                return $fm;
            });
        }
    }
}

$_GumRegister = new GumRegister();