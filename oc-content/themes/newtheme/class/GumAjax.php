<?php

class GumAjax {

    public function __construct()
    {
        osc_add_hook('ajax_activate_alert',     array(&$this,'activate_alert'));
        osc_add_hook('ajax_deactivate_alert', array(&$this,'deactivate_alert'));

        osc_add_hook('ajax_activate_item',      array(&$this, 'activate_item'));
        osc_add_hook('ajax_deactivate_item',  array(&$this, 'deactivate_item'));

        osc_add_hook('ajax_phone_reveal',   array(&$this, 'count_phone_reveal'));


        osc_add_hook('ajax_admin_gum_tip',   array(&$this, 'tip'));

    }

    function count_phone_reveal()
    {
        $error = 0;
        if (osc_csrfguard_validate_token(Params::getParam('CSRFName'), Params::getParam('CSRFToken'))) {
            $id = (int)Params::getParam('id');
            if($id=='' || !is_numeric($id)) {
                $error = 1;
            }
            if(!$error) {
                $success = GumModelItemPhoneReveal::newInstance()->increasePhoneReveal($id);
                if(!$success>0) {
                    $error = 1;
                }
            }
        }
        echo json_encode(array(
            'error' => $error,
            'msg'   => ''
        ));
        exit;
    }

    function activate_alert()
    {
        $error = 0;
        $alert_id = Params::getParam('id');
        if($alert_id=='' || !is_numeric($alert_id)) {
            $error = 1;
        }
        $alert = Alerts::newInstance()->findByPrimaryKey($alert_id);
        if(isset($alert['pk_i_id']) && $alert['fk_i_user_id']==osc_logged_user_id()) {
            if(!Alerts::newInstance()->activate($alert_id)) {
                $error = 1;
            }
        } else {
            $error = 1;
        }
        if($error) {
            osc_add_flash_ok_message(__('Some error occurred while we where enabling you alert.', 'gum'));
        }
        osc_add_flash_ok_message(__('You alert has been activated', 'gum'));
        osc_redirect_to(osc_user_alerts_url());
    }

    function deactivate_alert()
    {
        $error = 0;
        $alert_id = Params::getParam('id');
        if($alert_id=='' || !is_numeric($alert_id)) {
            $error = 1;
        }
        $alert = Alerts::newInstance()->findByPrimaryKey($alert_id);
        if(isset($alert['pk_i_id']) && $alert['fk_i_user_id']==osc_logged_user_id()) {
            if(!Alerts::newInstance()->deactivate($alert_id)) {
                $error = 1;
            }
        } else {
            $error = 1;
        }
        if($error) {
            osc_add_flash_ok_message(__('Some error occurred while we where enabling you alert.', 'gum'));
        }
        osc_add_flash_ok_message(__('You alert has been deactivated', 'gum'));
        osc_redirect_to(osc_user_alerts_url());
    }

    function activate_item()
    {
        $error = 0;
        $item_id = Params::getParam('id');
        if($item_id=='' || !is_numeric($item_id)) {
            $error = 1;
        } else {
            $item = Item::newInstance()->findByPrimaryKey($item_id);
            if(isset($item['pk_i_id']) && $item['fk_i_user_id']==osc_logged_user_id()) {
                $mItems = new ItemActions(false);
                $success = $mItems->activate( $item['pk_i_id'], $item['s_secret'] );
            }
        }
        if($error || !$success) {
            osc_add_flash_ok_message(__('Some error occurred while we where activating your listing.', 'gum'));
        }
        osc_add_flash_ok_message(__('You listing has been activated', 'gum'));
        osc_redirect_to(osc_user_list_items_url());
    }

    function deactivate_item()
    {
        $error = 0;
        $item_id = Params::getParam('id');
        if($item_id=='' || !is_numeric($item_id)) {
            $error = 1;
        } else {
            $item = Item::newInstance()->findByPrimaryKey($item_id);
            if(isset($item['pk_i_id']) && $item['fk_i_user_id']==osc_logged_user_id()) {
                $mItems = new ItemActions(false);
                $success = $mItems->deactivate( $item['pk_i_id'], $item['s_secret'] );
            }
        }
        if($error || !$success) {
            osc_add_flash_ok_message(__('Some error occurred while we where deactivating your listing.', 'gum'));
        }
        osc_add_flash_ok_message(__('You listing has been deactivated', 'gum'));
        osc_redirect_to(osc_user_list_items_url());
    }

    function tip() {
        echo json_encode(GumModelUsefulTips::newInstance()->findByPrimaryKey(Params::getParam("id")));
        die;
    }
}