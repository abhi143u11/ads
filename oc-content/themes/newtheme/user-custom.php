<?php
    // meta tag robots
    osc_add_hook('header','gum_nofollow_construct');

    osc_current_web_theme_path('header.php') ;
?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12 custom-tabs">
                <?php osc_current_web_theme_path('user-sidebar.php'); ?>
                <div class="col-xs-12 col-md-8 col-lg-9">
                    <?php osc_render_file(); ?>
                </div>
            </div>
        </div>
    </div>
</div>