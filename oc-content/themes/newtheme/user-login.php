<?php
    // meta tag robots
    osc_add_hook('header','gum_nofollow_construct');

    osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php') ;
?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 col-sm-12">
                <div class="box-border">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="h1-custom"><?php _e('Login please', 'gum'); ?></h1>
                        </div>
                        <?php if(function_exists('fbc_button') ) { ?>
                        <div class="col-xs-12 col-sm-push-8 col-sm-4">
                            <p class="visible-md-block visible-lg-block"><label><?php _e('Or', 'gum'); ?></label></p>
                            <a class="btn btn-primary-custom btn-block"><i class="fa fa-facebook text-left"></i> <?php _e('Login with facebook', 'gum'); ?></a>
                        </div>
                        <?php } ?>
                        <?php $class = '';
                        if(function_exists('fbc_button') ) {
                            $class= 'col-sm-pull-4 col-sm-8 register-social'; ?>
                        <div class="col-xs-12 visible-xs-block visible-ms-block visible-sm-block">
                        <span class="rule"><span class="rule-text"><?php _e('Or', 'gum'); ?></span></span>
                        </div>
                        <?php } ?>
                        <form name="register" class="col-xs-12 <?php echo $class; ?>" action="<?php echo osc_base_url(true); ?>" method="post" >
                            <input type="hidden" name="page" value="login" />
                            <input type="hidden" name="action" value="login_post" />
                            <ul id="error_list"></ul>
                            <div class="form-group">
                                <label for="name"><?php _e('Email address', 'gum'); ?></label>
                                <?php GumUserForm::email_login_text(); ?>
                            </div>

                            <?php osc_run_hook('user_register_form'); ?>

                            <div class="form-group">
                                <label for="name"><?php _e('Password', 'gum'); ?></label>
                                <?php GumUserForm::password_login_text(); ?>
                            </div>


                            <div class="form-group">
                                <div class="col-xs-6 col-xs-push-6 text-right">
                                    <p><a href="<?php echo osc_recover_user_password_url(); ?>"><?php _e("Forgot password?", 'gum'); ?></a></p>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-6">
                                    <span class="text-left"><?php GumUserForm::rememberme_login_checkbox(); ?> <label for="remember"><?php _e('Remember me', 'gum'); ?></label></span>
                                    <br>
                                    <button type="submit" class="btn btn-primary-custom btn-block"><?php _e("Log in", 'gum'); ?></button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr-custom">
                            <div class="col-xs-12 text-center">
                                <p class="text-info"><a href="<?php echo osc_register_account_url(); ?>"><?php _e('Don\'t you have a account yet?', 'gum'); ?></a></p>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php GumUserForm::js_validation(); ?>
<?php osc_current_web_theme_path('footer.php') ; ?>