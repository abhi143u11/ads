<?php

class GumModelUsefulTips extends DAO
{
    private static $instance ;
    public static function newInstance()
    {
        if( !self::$instance instanceof self ) {
            self::$instance = new self ;
        }
        return self::$instance ;
    }

    function __construct()
    {
        parent::__construct();
        $this->setTableName('t_gum_useful_tips');
        $this->setPrimaryKey('pk_i_id');
        $this->setFields( array('pk_i_id', 'i_order', 's_title', 's_description', 'b_active') );

    }

    /**
     * Import sql file
     * @param type $file
     */
    public function import($file)
    {
        $path = osc_themes_path().$file;
        $sql = file_get_contents($path);

        if(! $this->dao->importSQL($sql) ){
            throw new Exception( "Error importSQL::GumModelUsefulTips<br>".$file ) ;
        }
    }

    public function insertBasicData() {
        $this->insertTip(__('Tip 1', 'gum'), __('Don\'t use Paysafe, Ukash or Western Union for trades', 'gum'));
        $this->insertTip(__('Tip 2', 'gum'), __('Never pay with Western Union, Moneygram or other anonymous payment services', 'gum'));
        $this->insertTip(__('Tip 3', 'gum'), __('Don\'t buy or sell outside of your country . Don\'t accept cashier cheques from outside your country', 'gum'));
        $this->insertTip(__('Tip 4', 'gum'), __('This site is never involved in any transaction, and does not handle payments, shipping, guarantee transactions, provide escrow services, or offer "buyer protection" or "seller certification"', 'gum'));
    }

    public function all($active = true) {
        $this->dao->select('*');
        $this->dao->from($this->getTableName());
        if($active===true || $active===1) {
            $this->dao->where('b_active', 1);
        } else if($active===false || $active===0) {
            $this->dao->where('b_active', 0);
        }
        $this->dao->orderBy('i_order', 'ASC');
        $result = $this->dao->get();
        if($result == false) {
            return array();
        }
        return $result->result();
    }

    public function _lastOrder() {
        $this->dao->select('i_order');
        $this->dao->from($this->getTableName());
        $this->dao->orderBy('i_order', 'DESC');
        $this->dao->limit(1);
        $result = $this->dao->get();

        if( $result == false ) {
            return 0;
        }

        $row = $result->row();
        if(!isset($row['i_order'])) {
            return 0;
        }
        return $row['i_order'];
    }

    public function insertTip($title, $description, $active = 1) {
        $order = $this->_lastOrder()+1;
        return $this->dao->insert(
            $this->getTableName(),
            array(
                'i_order' => $order,
                's_title' => $title,
                's_description' => $description,
                'b_active' => $active
            )
        );
    }

    public function updateTip($id, $title, $description, $active = 1/*, $order*/) {
        return $this->dao->update(
            $this->getTableName(),
            array(
                //'i_order' => $order,
                's_title' => $title,
                's_description' => $description,
                'b_active' => $active
            ),
            array('pk_i_id' => $id)
        );
    }

    public function deleteTip($id) {
        $this->dao->delete( $this->getTableName(), array('pk_i_id' => (int)($id)) );
    }

    public function activate($id) {
        return $this->dao->update($this->getTableName(),array('b_active' => 1),array('pk_i_id' => $id));
    }

    public function deactivate($id) {
        return $this->dao->update($this->getTableName(),array('b_active' => 0),array('pk_i_id' => $id));
    }

    public function sort($id, $dir) {
        $otip = $this->findByPrimaryKey($id);
        if(isset($otip['i_order'])) {
            if($dir==1) {
                $last = $this->_lastOrder();
                if($otip['i_order']!=$last) {
                    $this->dao->select('pk_i_id, i_order') ;
                    $this->dao->from($this->getTableName());
                    $this->dao->where('i_order > ' . $otip['i_order']);
                    $this->dao->orderBy('i_order', 'ASC');
                    $this->dao->limit(1);
                    $result = $this->dao->get();
                    if($result) {
                        $tip = $result->row();
                        if(isset($tip['i_order'])) {
                            $this->dao->update($this->getTableName(), array('i_order' => $otip['i_order']), array('pk_i_id' => $tip['pk_i_id']));
                            $this->dao->update($this->getTableName(), array('i_order' => $tip['i_order']), array('pk_i_id' => $otip['pk_i_id']));
                        }
                    }
                }
            } else {
                if($otip['i_order']>1) {
                    $this->dao->select('pk_i_id, i_order') ;
                    $this->dao->from($this->getTableName());
                    $this->dao->where('i_order < ' . $otip['i_order']);
                    $this->dao->orderBy('i_order', 'DESC');
                    $this->dao->limit(1);
                    $result = $this->dao->get();
                    if($result) {
                        $tip = $result->row();
                        if(isset($tip['i_order'])) {
                            $this->dao->update($this->getTableName(), array('i_order' => $otip['i_order']), array('pk_i_id' => $tip['pk_i_id']));
                            $this->dao->update($this->getTableName(), array('i_order' => $tip['i_order']), array('pk_i_id' => $otip['pk_i_id']));
                        }
                    }
                }
            }
        }
    }
    /**
     * Remove data and tables related to the plugin.
     */
    public function uninstall()
    {
        $this->dao->query(sprintf('DROP TABLE %s', $this->getTableName()) ) ;
    }

}
