<?php

class GumModelItemPhoneReveal extends DAO
{
        private static $instance ;
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self ;
            }
            return self::$instance ;
        }

        function __construct()
        {
            parent::__construct();
            $this->setTableName('t_item_phone_reveal');
            $this->setPrimaryKey('pk_i_id');
            $this->setFields( array('pk_i_id', 'fk_i_item_id', 'dt_date') );

        }

        public function getTable_detail()
        {
            return DB_TABLE_PREFIX.'t_item_phone_reveal_detail';
        }

        /**
         * Import sql file
         * @param type $file
         */
        public function import($file)
        {
            $path = osc_themes_path().$file;
            $sql = file_get_contents($path);

            if(! $this->dao->importSQL($sql) ){
                throw new Exception( "Error importSQL::GumModelItemPhoneReveal<br>".$file ) ;
            }
        }

        /**
         * Remove data and tables related to the plugin.
         */
        public function uninstall()
        {
            $this->dao->query(sprintf('DROP TABLE %s', $this->getTableName()) ) ;
            $this->dao->query(sprintf('DROP TABLE %s', $this->getTable_detail()) ) ;
        }

        /**
         *
         * @param type $item_id
         */
        public function insertItemStat($item_id)
        {
            $aSet = array(
                'fk_i_item_id'    => $item_id,
                'i_views'           => 0
                );
            $this->dao->insert( $this->getTableName(), $aSet) ;
        }

        /**
         *
         * @param type $item_id
         * @return type
         */
        public function increasePhoneReveal($item_id)
        {
            $aSet = array(
                'fk_i_item_id'    => $item_id,
                'dt_date'           => date("Y-m-d H:i:s")
                );
            $this->dao->insert( $this->getTable_detail(), $aSet) ;

            $sql = "UPDATE ".$this->getTableName()." SET i_views = i_views + 1 WHERE fk_i_item_id = '" . $item_id . "'";
            return $this->dao->query($sql) ;
        }

        public function getTotalViewPhoneByItemId($item_id)
        {
            $sql = 'SELECT i_views as total FROM '.$this->getTableName(). ' WHERE fk_i_item_id = ' . $item_id ;
            $result = $this->dao->query($sql) ;

            if( $result === false ) {
                return 0;
            }

            $items = $result->result() ;
            if( is_array($items) && count($items)>0) {
                return $items[0]['total'];
            } else {
                return 0;
            }
        }

        public function _init()
        {
            $result = $this->dao->query('SELECT pk_i_id FROM '.DB_TABLE_PREFIX.'t_item') ;

            $items = $result->result() ;
            foreach ($items as $item) {
                $this->insertItemStat($item['pk_i_id']);
            }
        }
    }