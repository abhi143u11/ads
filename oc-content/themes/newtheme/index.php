<?php
/*
Theme Name: Gum
Theme URI: http://osclass.org/
Description: 100% responsive and mobile theme developed by Osclass team.
Version: 1.0.0
Author: Osclass
Author URI: http://osclass.org/
Widgets:
Theme update URI: gum
*/

    function gum_theme_info() {
        return array(
             'name'        => 'gum'
            ,'version'     => '1.0.0'
            ,'description' => '100% responsive and mobile theme developed by Osclass team.'
            ,'author_name' => 'Osclass'
            ,'author_url'  => 'http://osclass.org'
            ,'locations'   => array()
        );
    }

?>
