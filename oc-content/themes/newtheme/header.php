<!DOCTYPE html>
<html lang="en">
    <head>
        <?php osc_current_web_theme_path('head.php'); ?>
    </head>
    <body class="<?php echo gum_body_class(); ?>">
    <style>
        .header-navbar {
          margin-bottom: 0px;
        }
    </style>
    <div class="filters-mask">
        <div class="viewport-mask visible-to-sm"></div>
        <!-- Static navbar -->
        <nav class="header navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only"><?php _e('Toggle navigation', 'gum'); ?></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo osc_base_url(); ?>">
                        <img style="position: absolute;top: 6px; height: 40px;" border="0" alt="<?php echo osc_esc_html(osc_page_title()); ?> '" src="<?php echo gum_logo_url(); ?>">
                    </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <?php if(!osc_is_web_user_logged_in()) { ?>
                        <li><a href="<?php echo osc_user_login_url(); ?>"><?php _e('Login', 'gum'); ?></a></li>
                        <?php } else { ?>
                        <li class="visible-lg-inline-block visible-md-inline-block"><span class="navbar_text"><?php printf(__('Hi %s!', 'gum'), osc_logged_user_name()); ?></span></li>
                        <li><a class="navbar_logout" href="<?php echo osc_user_logout_url(); ?>"><?php _e('Logout', 'gum'); ?></a></li>
                        <?php } ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php _e('My account', 'gum'); ?> <i class="fa fa-caret-down btn-ico-left"></i></a>
                            <ul class="dropdown-menu">
                                <li><a class="gum-drop" href="<?php echo osc_user_list_items_url(); ?>"><i class="fa fa-folder-open btn-ico-right"></i> <?php _e('Manage my ads', 'gum'); ?></a></li>
                                <li><a class="gum-drop" href="<?php echo osc_user_alerts_url(); ?>"><i class="fa fa-floppy-o btn-ico-right"></i> <?php _e('Saved searches', 'gum'); ?></a></li>
                                <li><a class="gum-drop" href="<?php echo osc_user_profile_url(); ?>"><i class="fa fa-user btn-ico-right"></i><?php _e('My details', 'gum'); ?></a></li>
                                <?php if(!osc_is_web_user_logged_in()) { ?>
                                <li><a class="gum-drop" href="<?php echo osc_register_account_url(); ?>"><i class="fa fa-user-plus btn-ico-right"></i><?php _e('Create Account', 'gum'); ?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li>
                            <a id="header-nav-post-ad" href="<?php echo osc_item_post_url_in_category(); ?>">
                                <i class="fa fa-thumb-tack btn-ico"></i><span class="btn-txt"><?php _e('Post an ad', 'gum'); ?></span>
                            </a>
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>
        <div class="container-fluid container-header-search">
            <div class="container header-search row">
                <form action="<?php echo osc_base_url(true); ?>" method="get" class="search nocsrf form-inline" <?php /* onsubmit="javascript:return doSearch();"*/ ?>>
                    <input type="hidden" name="page" value="search"/>
                    <div class="col-md-8 col-sm-7">
                        <div class="input-group header-keyword ">
                            <div class="input-group-btn header-search-category-wrapper">
                                <?php gum_parent_categories_select(); ?>
                                <select id="width_tmp_select">
                                    <option id="width_tmp_option"></option>
                                 </select>
                            </div>
                            <input type="text" name="sPattern" class="form-control" placeholder="<?php echo osc_esc_html(osc_get_preference('keyword_placeholder', 'gum_theme')); ?>">
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-5 col-xs-12 header-location">
                        <div class="row">
                            <div class="col-md-9 col-sm-9 col-xs-8">
                                <div class="row">
                                    <label class="col-sm-2 header-location-label visible-lg-inline-block visible-md-inline-block visible-sm-inline-block"><?php _e('in', 'gum'); ?></label>
                                    <div class="col-sm-10 form-group">
                                        <?php if(osc_get_preference('search_location_type', 'gum_theme')=='region') { ?>
                                        <input type="text" name="sRegion" class="form-control" placeholder="<?php echo osc_esc_html(osc_get_preference('location_region_placeholder', 'gum_theme')); ?>">
                                        <?php } else { ?>
                                        <input type="text" name="sCity" class="form-control" placeholder="<?php echo osc_esc_html(osc_get_preference('location_city_placeholder', 'gum_theme')); ?>">
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-block" id="header-search-button"><?php _e('Go', 'gum'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /input-group -->

                </form>
            </div>
        </div>

        <?php if(osc_is_home_page()): ?>
        <div class="banner-header container-fluid">
            <div class="container">
            </div>
        </div>
        <?php elseif(osc_is_ad_page() ) : // Params::getParam('page')!='search'):
            $breadcrumb = osc_breadcrumb('&raquo;', false, get_breadcrumb_lang());
            if( $breadcrumb !== '') { ?>
        <div class="container-fluid container-breadcrumb">
            <div class="row">
                <div class="container breadcrumb">
                    <?php echo $breadcrumb; ?>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <?php
            }
            endif; ?>
        <div class="container-fluid main-background">
            <div class="container">
            <?php osc_show_flash_message(); ?>
            </div>
        </div>
