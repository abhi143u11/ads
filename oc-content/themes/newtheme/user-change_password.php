<?php
    // meta tag robots
    osc_add_hook('header','gum_nofollow_construct');

    osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php') ;
    View::newInstance()->_exportVariableToView('user', User::newInstance()->findByPrimaryKey(osc_logged_user_id()));
    ?>

<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12 custom-tabs">
                <?php osc_current_web_theme_path('user-sidebar.php'); ?>
                <div class="col-xs-12 col-md-8 col-lg-9">
                    <h1 class="h1-custom col-xs-12"><?php _e('Change password', 'gum'); ?></h1>
                    <form name="register" action="<?php echo osc_base_url(true); ?>" method="post" >
                            <input type="hidden" name="page" value="user" />
                            <input type="hidden" name="action" value="change_password_post" />
                            <div class="clearfix"></div>
                            <div class="col-sm-12 text-right">*&nbsp;<?php _e('Required', 'gum'); ?></div>
                            <div class="clearfix"></div>
                            <ul id="error_list"></ul>

                            <div class="col-xs-12 col-md-6 form-group">
                                <label for="password"><?php _e('Current password', 'gum'); ?> *</label>
                                <input class="form-control"  type="password" name="password" id="password" value="" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12 col-md-6 form-group">
                                <label for="new_password"><?php _e('New password', 'gum'); ?> *</label>
                                <input class="form-control" type="password" name="new_password" id="new_password" value="" />
                            </div>
                            <div class="col-xs-12 col-md-6 form-group">
                                <label for="new_password2"><?php _e('Confirm password', 'gum'); ?> *</label>
                                <input class="form-control" type="password" name="new_password2" id="new_password2" value="" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary-custom btn-block"><?php _e("Update", 'gum'); ?></button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php osc_current_web_theme_path('footer.php') ; ?>