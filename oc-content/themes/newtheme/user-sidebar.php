<div class="col-md-4 col-lg-3 visible-md-block visible-lg-block">
    <?php echo gum_private_user_menu( get_user_menu() ); ?>
</div>
<div class="col-xs-6 col-sm-4 visible-to-sm pull-right">
    <?php echo gum_private_user_menu( get_user_menu(), true ); ?>
</div>