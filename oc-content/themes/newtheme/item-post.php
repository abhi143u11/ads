<?php
    // meta tag robots
    osc_add_hook('header','gum_nofollow_construct');

    osc_enqueue_script('jquery-validate');
    osc_enqueue_script('jquery-fineuploader');
    osc_enqueue_style('jquery-ui', osc_current_web_theme_url() . 'lib/jquery-ui/jquery-ui.css');
    gum_body_class();

    $action = 'item_add_post';
    $edit = false;
    if(Params::getParam('action') == 'item_edit'){
        $action = 'item_edit_post';
        $edit = true;
    }

osc_current_web_theme_path('header.php');

if (gum_default_location_show_as() == 'dropdown') {
    GumItemForm::location_javascript();
} else {
    GumItemForm::location_javascript_new();
}

?>
<div class="container-fluid main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-9">
                <h1><?php _e('Publish a listing', 'gum'); ?></h1>
                <form name="item" action="<?php echo osc_base_url(true);?>" method="post" enctype="multipart/form-data" id="item-post">
                    <fieldset>
                    <input type="hidden" name="action" value="<?php echo $action; ?>" />
                        <input type="hidden" name="page" value="item" />
                    <?php if($edit){ ?>
                        <input type="hidden" name="id" value="<?php echo osc_item_id();?>" />
                        <input type="hidden" name="secret" value="<?php echo osc_item_secret();?>" />
                    <?php } ?>
                    <div class="clearfix"></div>
                    <ul id="error_list"></ul>
                    <p class="col-xs-12  text-right text-normal"><span ><?php _e('* Required'); ?></span></p>
                    <div class="clearfix"></div>


                    <div class="col-xs-12  form-group box-border">
                        <label for="select_1"><?php _e('Category', 'gum'); ?></label>
                        <?php GumItemForm::category_select(null, null, __('Select a category', 'gum')); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="box-border">
                        <div class="col-xs-12">
                            <label class="control-label" for="title[<?php echo osc_current_user_locale(); ?>]"><?php _e('Title', 'gum'); ?></label>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <?php GumItemForm::title_input('title',osc_current_user_locale(), osc_esc_html( gum_item_title() )); ?>
                         </div>
                        <div class="col-xs-12 col-md-6">
                            <span class="count-title"><?php echo osc_max_characters_per_title(); ?></span> <?php _e('characters remaining', 'gum'); ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12">
                            <br>
                            <label class="control-label" for="description[<?php echo osc_current_user_locale(); ?>]"><?php _e('Description', 'gum'); ?></label>
                        </div>
                        <div class="col-xs-12 col-md-6">
                           <?php GumItemForm::description_textarea('description',osc_current_user_locale(), osc_esc_html( gum_item_description() )); ?>
                         </div>
                        <div class="col-xs-12 col-md-6">
                            <span class="count-description"><?php echo osc_max_characters_per_description(); ?></span> <?php _e('characters remaining', 'gum'); ?>
                            <p><?php _e('Enter as much information possible; ads with detailed and longer descriptions get more views and replies!', 'gum'); ?></p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>

                     <?php if( osc_price_enabled_at_items() ) { ?>
                    <div class="col-xs-12 form-group box-border">
                        <label class="col-xs-12" for="price"><?php _e('Price', 'gum'); ?></label>
                        <div class="col-xs-12 col-sm-6">
                            <?php GumItemForm::price_input_text(); ?>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <?php GumItemForm::currency_select(); ?>
                        </div>
                    </div>
                    <?php } ?>

                    <div class="col-xs-12 form-group box-border">
                        <?php if( osc_images_enabled_at_items() ) {
                        GumItemForm::ajax_photos(); ?>
                        <?php if(!$edit) { ?>
                        <p class="text-info" style="font-weight: 600;"><i class="fa fa-info-circle"></i> <?php _e('Use drag and drop if you want to change your primary image. The first image will be your main image.', 'gum'); ?></p>
                         <?php  } ?>
                         <?php } ?>
                    </div>

                    <div class="col-xs-12 form-group box-border">
                        <?php if(count(osc_get_countries()) > 1) { ?>
                            <label class="control-label" for="country"><?php _e('Country', 'gum'); ?></label>
                            <?php GumItemForm::country_select(osc_get_countries(), osc_user()); ?>
                            <br>
                            <label for="regionId"><?php _e('Region', 'gum'); ?></label>
                            <?php if (gum_default_location_show_as() == 'dropdown') {
                                GumItemForm::region_select(osc_get_regions(osc_user_field('fk_c_country_code')), osc_user());
                            } else {
                                GumItemForm::region_text(osc_user());
                            }
                        } else {
                            $aCountries = osc_get_countries();
                            $aRegions = osc_get_regions($aCountries[0]['pk_c_code']);  ?>
                            <input type="hidden" id="countryId" name="countryId" value="<?php echo osc_esc_html($aCountries[0]['pk_c_code']); ?>"/>
                            <label for="region"><?php _e('Region', 'gum'); ?></label>
                          <?php
                            if (gum_default_location_show_as() == 'dropdown') {
                                GumItemForm::region_select($aRegions, osc_user());
                            } else {
                                GumItemForm::region_text(osc_user());
                            } ?>
                        <?php } ?>
                            <br>
                            <label for="city"><?php _e('City', 'gum'); ?></label>
                            <?php if (gum_default_location_show_as() == 'dropdown') {
                                if(Params::getParam('action') != 'item_edit') {
                                    GumItemForm::city_select(null, osc_item());
                                } else { // add new item
                                    GumItemForm::city_select(osc_get_cities(osc_user_region_id()), osc_user());
                                }
                            } else {
                                GumItemForm::city_text(osc_user());
                            } ?>
                            <br>
                            <label for="cityArea"><?php _e('City Area', 'gum'); ?></label>
                            <?php GumItemForm::city_area_text(osc_user()); ?>
                            <br>
                            <label for="address"><?php _e('Address', 'gum'); ?></label>
                            <?php GumItemForm::address_text(osc_user()); ?>
                    </div>

                    <!-- seller info -->
                    <?php if(!osc_is_web_user_logged_in() ) { ?>
                    <div class="col-xs-12 form-group box-border">
                            <h2 class="h2-custom"><?php _e("Seller's information", 'gum'); ?></h2>

                            <label for="contactName"><?php _e('Name', 'gum'); ?></label>
                            <?php GumItemForm::contact_name_text(); ?>
                            <br>
                            <label class="control-label" for="contactEmail"><?php _e('E-mail', 'gum'); ?></label>
                            <?php GumItemForm::contact_email_text(); ?>
                            <br>
                            <label for="showEmail"><?php GumItemForm::show_email_checkbox(); ?>&nbsp;<?php _e('Show e-mail on the listing page', 'gum'); ?></label>
                    </div>
                    <?php } ?>

                    <div class="col-xs-12 form-group">
                        <?php if($edit) {
                            ItemForm::plugin_edit_item();
                        } else {
                            ItemForm::plugin_post_item();
                        } ?>
                    </div>

                    <div class="form-group">
                        <?php if( osc_recaptcha_items_enabled() ) { ?>
                        <div class="col-xs-6 text-center">
                            <?php osc_show_recaptcha(); ?>
                        </div>
                        <?php }?>
                        <div class="clearfix"></div>
                        <div class="col-xs-6">
                            <button type="submit" class="btn btn-primary-custom btn-block"><?php _e("Post My Ad", 'gum'); ?></button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr-custom">
                    <div class="col-xs-12">
                        <p class="text-info"><?php _e('By creating an account or using a social login, you are agreeing to our website Terms & Conditions and Privacy Policy.', 'gum'); ?></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    <?php if(osc_locale_thousands_sep()!='' || osc_locale_dec_point() != '') { ?>
    $(document).ready(function(){
        $("#price").blur(function(event) {
            var price = $("#price").prop("value");
            <?php if(osc_locale_thousands_sep()!='') { ?>
            while(price.indexOf('<?php echo osc_esc_js(osc_locale_thousands_sep());  ?>')!=-1) {
                price = price.replace('<?php echo osc_esc_js(osc_locale_thousands_sep());  ?>', '');
            }
            <?php }; ?>
            <?php if(osc_locale_dec_point()!='') { ?>
            var tmp = price.split('<?php echo osc_esc_js(osc_locale_dec_point())?>');
            if(tmp.length>2) {
                price = tmp[0]+'<?php echo osc_esc_js(osc_locale_dec_point())?>'+tmp[1];
            }
            <?php }; ?>
            $("#price").prop("value", price);
        });
    });
    <?php }; ?>

    $('body').on('focus keypress', 'input[name*="title"]', function (e) {

        var $this = $(this);
        var msgSpan = $this.parents('body').find('.count-title');
        var ml = <?php echo osc_max_characters_per_title(); ?>;
        var length = this.value.length;
        var msg = ml - length;
        msgSpan.css('color', 'inherit');
        if(msg<0) {
            msgSpan.css('color', 'red');
        }
        msgSpan.html(msg);
    });

    $('body').on('focus keypress', 'textarea[name*="description"]', function (e) {
        var $this = $(this);
        var msgSpan = $this.parents('body').find('.count-description');
        var ml = <?php echo osc_max_characters_per_description(); ?>;
        var length = this.value.length;
        var msg = ml - length;
        msgSpan.css('color', 'inherit');
        if(msg<0) {
            msgSpan.css('color', 'red');
        }
        msgSpan.html(msg);
    });
</script>
<?php osc_current_web_theme_path('footer.php'); ?>