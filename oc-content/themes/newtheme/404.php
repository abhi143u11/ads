<?php
    // meta tag robots
    osc_add_hook('header','bender_nofollow_construct');
    osc_current_web_theme_path('header.php') ;
?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <div class="col-sm-7 col-ms-6 col-xs-12">
                <div class="row box-border">
                    <div class="col-xs-12">
                        <h1><i class="fa fa-exclamation-circle text-danger"></i>&nbsp;<?php _e("Oops Error 404", 'gum'); ?></h1>
                        <p class="h2-custom"><span class="h2-custom"><?php _e("Sorry but I can't find the page you're looking for", 'gum'); ?></span></p>
                    </div>
                    <form action="<?php echo osc_base_url(true) ; ?>" method="get" class="search form-inline">
                        <input type="hidden" name="page" value="search" />
                        <div class="col-xs-12 col-md-6 form-group">
                            <label><?php _e("<strong>Search</strong> for it:", 'gum') ; ?></label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 col-md-12 form-group">
                            <input class="form-control" type="text" name="sPattern"  id="query" value="" />
                            <br class="visible-to-ms">
                            <button type="submit" class="btn btn-primary-custom form-control"><?php _e('Search', 'gum') ; ?></button>
                        </div>
                    </form>
                </div>
             </div>
            <div class="col-sm-5 col-ms-6 text-center" style="overflow: hidden;">
                <?php
                $demo = '<img src="'.osc_current_web_theme_url('images/banner-300x250.gif').'"/>';
                $ad = osc_get_preference('ad-homepage-sidebar-300x250', 'gum_theme');
                echo ($ad=="#demo#") ? $demo : $ad;
                ?>
            </div>
     </div>
</div>

<?php osc_current_web_theme_path('footer.php') ; ?>