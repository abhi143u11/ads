<?php if ( (!defined('ABS_PATH')) ) exit('ABS_PATH is not loaded. Direct access is not allowed.'); ?>
<?php if ( !OC_ADMIN ) exit('User access is not allowed.'); ?>
<?php
if(Params::getParam('action_specific')=='upload_logo') {
    $package = Params::getFiles('logo');
    if( $package['error'] == UPLOAD_ERR_OK ) {
        $img = ImageResizer::fromFile($package['tmp_name']);
        $ext = $img->getExt();
        $logo_name     = 'gum_logo';
        $logo_name    .= '.'.$ext;
        $path = osc_uploads_path() . $logo_name ;

        move_uploaded_file($package['tmp_name'], $path);

        osc_set_preference('logo', $logo_name, 'gum_theme');

        osc_add_flash_ok_message(__('The logo image has been uploaded correctly', 'gum'), 'admin');
    } else {
        osc_add_flash_error_message(__("An error has occurred, please try again", 'gum'), 'admin');
    }
    ob_get_clean();
    osc_redirect_to(osc_admin_render_theme_url('oc-content/themes/gum/admin/media.php'));
}
if(Params::getParam('action_specific')=='upload_favicon') {
    $package = Params::getFiles('favicon');
    if( $package['error'] == UPLOAD_ERR_OK ) {
        if($package['type']=='image/x-icon' ||
            $package['type']=='image/vnd.microsoft.icon' ||
            $package['type']=='image/png')
            {
            $img = ImageResizer::fromFile($package['tmp_name']);
            $ext = $img->getExt();
            $fav_name     = 'gum_favicon';
            $fav_name    .= '.ico';
            $path = osc_uploads_path() . $fav_name ;
            $img->saveToFile($path);

            osc_set_preference('favicon', $fav_name, 'gum_theme');

            osc_add_flash_ok_message(__('The favicon image has been uploaded correctly', 'gum'), 'admin');
        } else {
            osc_add_flash_error_message(__('The favicon image must be an icon file. (*.ico)', 'gum'), 'admin');
        }
    } else {
        osc_add_flash_error_message(__("An error has occurred, please try again", 'gum'), 'admin');
    }
    ob_get_clean();
    osc_redirect_to(osc_admin_render_theme_url('oc-content/themes/gum/admin/media.php'));
}
if(Params::getParam('action_specific')=='upload_homebanner') {
    $package = Params::getFiles('homebanner');
    if( $package['error'] == UPLOAD_ERR_OK ) {
        $img = ImageResizer::fromFile($package['tmp_name']);
        $ext = $img->getExt();
        $banner_name     = 'gum_homebanner';
        $banner_name    .= '.' . $ext;
        $path = osc_uploads_path() . $banner_name ;
        move_uploaded_file($package['tmp_name'], $path);

        osc_set_preference('homebanner', $banner_name, 'gum_theme');

        osc_add_flash_ok_message(__('The banner image has been uploaded correctly', 'gum'), 'admin');
    } else {
        osc_add_flash_error_message(__("An error has occurred, please try again", 'gum'), 'admin');
    }
    ob_get_clean();
    osc_redirect_to(osc_admin_render_theme_url('oc-content/themes/gum/admin/media.php'));
}
if(Params::getParam('action_specific')=='remove') {
    $logo = osc_get_preference('logo','gum_theme');
    $path = osc_uploads_path() . $logo ;
    if(file_exists( $path ) ) {
        @unlink( $path );
        osc_delete_preference('logo','gum_theme');
        osc_reset_preferences();
        osc_add_flash_ok_message(__('The logo image has been removed', 'gum'), 'admin');
    } else {
        osc_add_flash_error_message(__("Image not found", 'gum'), 'admin');
    }
    ob_get_clean();
    osc_redirect_to(osc_admin_render_theme_url('oc-content/themes/gum/admin/media.php'));
}
if(Params::getParam('action_specific')=='remove_favicon') {
    $logo = osc_get_preference('favicon','gum_theme');
    $path = osc_uploads_path() . $logo ;
    if(file_exists( $path ) ) {
        @unlink( $path );
        osc_delete_preference('favicon','gum_theme');
        osc_reset_preferences();
        osc_add_flash_ok_message(__('The favicon image has been removed', 'gum'), 'admin');
    } else {
        osc_add_flash_error_message(__("Image not found", 'gum'), 'admin');
    }
    ob_get_clean();
    osc_redirect_to(osc_admin_render_theme_url('oc-content/themes/gum/admin/media.php'));
}
if(Params::getParam('action_specific')=='remove_banner') {
    $logo = osc_get_preference('homebanner','gum_theme');
    $path = osc_uploads_path() . $logo ;
    if(file_exists( $path ) ) {
        @unlink( $path );
        osc_delete_preference('homebanner','gum_theme');
        osc_reset_preferences();
        osc_add_flash_ok_message(__('The banner image has been removed', 'gum'), 'admin');
    } else {
        osc_add_flash_error_message(__("Image not found", 'gum'), 'admin');
    }
    ob_get_clean();
    osc_redirect_to(osc_admin_render_theme_url('oc-content/themes/gum/admin/media.php'));
}

?>

<style type="text/css" media="screen">
    .command { background-color: white; color: #2E2E2E; border: 1px solid black; padding: 8px; }
    .theme-files { min-width: 500px; }
    .gum_box {
        width: 450px;
        padding:15px;
        float:left;
    }
</style>
<h2 class="render-title"><?php _e('Logo, Favicon and Media', 'gum'); ?></h2>
<?php
    $logo_preference = osc_get_preference('logo', 'gum_theme');
    $favicon_preference = osc_get_preference('favicon', 'gum_theme');
    $homebanner_preference = osc_get_preference('homebanner', 'gum_theme');
?>


<?php if( is_writable( osc_uploads_path()) ) { ?>
<div class="gum_box">
    <?php if($logo_preference) { ?>
        <form action="<?php echo osc_admin_render_theme_url('oc-content/themes/gum/admin/media.php');?>" method="post" enctype="multipart/form-data" class="nocsrf" style="height: 60px;">
            <input type="hidden" name="action_specific" value="remove"/>
            <input id="button_remove" type="submit" value="<?php echo osc_esc_html(__('Remove logo','gum')); ?>" class="btn btn-red">
            <img border="0" height="40" alt="<?php echo osc_esc_html( osc_page_title() ); ?>" src="<?php echo gum_logo_url().'?'.filemtime(osc_uploads_path() . osc_get_preference('logo','gum_theme'));?>"/>
            <div class="clear"></div>
        </form>
    <?php } else { ?>
    <div class="flashmessage flashmessage-warning flashmessage-inline" style="display: block;">
        <p><?php _e('No <b>logo</b> has been uploaded yet', 'gum'); ?></p>
    </div>
<?php } ?>
    <h2 class="render-title separate-top"><?php _e('Upload logo', 'gum') ?></h2>
    <p><?php _e('The preferred size of the logo is 155x40.', 'gum'); ?></p>
    <?php if( $logo_preference ) { ?>
    <div class="flashmessage flashmessage-inline flashmessage-warning"><p><?php _e('<strong>Note:</strong> Uploading another logo will overwrite the current logo.', 'gum'); ?></p></div>
    <?php } ?>
    <br/><br/>
    <form action="<?php echo osc_admin_render_theme_url('oc-content/themes/gum/admin/media.php'); ?>" method="post" enctype="multipart/form-data" class="nocsrf">
        <input type="hidden" name="action_specific" value="upload_logo" />
        <fieldset>
            <div class="form-horizontal">
                <div class="form-row">
                    <div class="form-label"><?php _e('Logo image (png,gif,jpg)','gum'); ?></div>
                    <div class="form-controls">
                        <input type="file" name="logo" id="package" />
                    </div>
                </div>
                <div class="form-actions">
                    <input id="button_save" type="submit" value="<?php echo osc_esc_html(__('Upload logo','gum')); ?>" class="btn btn-submit">
                </div>
            </div>
        </fieldset>
    </form>
</div>
<div class="gum_box">
    <?php if($favicon_preference) { ?>
        <form action="<?php echo osc_admin_render_theme_url('oc-content/themes/gum/admin/media.php');?>" method="post" enctype="multipart/form-data" class="nocsrf" style="height: 60px;">
            <input type="hidden" name="action_specific" value="remove_favicon"/>
            <input id="button_remove" type="submit" value="<?php echo osc_esc_html(__('Remove favicon','gum')); ?>" class="btn btn-red">
            <img  height="32" border="0" alt="<?php echo osc_esc_html( osc_page_title() ); ?>" src="<?php echo gum_favicon_url().'?'.filemtime(osc_uploads_path() . osc_get_preference('favicon','gum_theme'));?>"/>
            <div class="clear"></div>
        </form>
    <?php } else { ?>
    <div class="flashmessage flashmessage-warning flashmessage-inline" style="display: block;">
        <p><?php _e('No <b>favicon</b> has been uploaded yet', 'gum'); ?></p>
    </div>
<?php } ?>
    <h2 class="render-title separate-top"><?php _e('Upload favicon', 'gum') ?></h2>
    <p><?php _e('The preferred size of the logo is 32x32.', 'gum'); ?></p>
    <?php if( $logo_preference ) { ?>
    <div class="flashmessage flashmessage-inline flashmessage-warning"><p><?php _e('<strong>Note:</strong> Uploading another favicon will overwrite the current favicon.', 'gum'); ?></p></div>
    <?php } ?>
    <br/><br/>
    <form action="<?php echo osc_admin_render_theme_url('oc-content/themes/gum/admin/media.php'); ?>" method="post" enctype="multipart/form-data" class="nocsrf">
        <input type="hidden" name="action_specific" value="upload_favicon" />
        <fieldset>
            <div class="form-horizontal">
                <div class="form-row">
                    <div class="form-label"><?php _e('Favicon image (png,ico)','gum'); ?></div>
                    <div class="form-controls">
                        <input type="file" name="favicon" id="favicon" />
                    </div>
                </div>
                <div class="form-actions">
                    <input id="button_save" type="submit" value="<?php echo osc_esc_html(__('Upload favicon','gum')); ?>" class="btn btn-submit">
                </div>
            </div>
        </fieldset>
    </form>
</div>
<div class="gum_box">
    <?php if($homebanner_preference) { ?>
        <form action="<?php echo osc_admin_render_theme_url('oc-content/themes/gum/admin/media.php');?>" method="post" enctype="multipart/form-data" class="nocsrf" style="height: 60px;">
            <input type="hidden" name="action_specific" value="remove_banner"/>
            <input id="button_remove" type="submit" value="<?php echo osc_esc_html(__('Remove banner','gum')); ?>" class="btn btn-red">
            <img  height="55"  border="0" alt="<?php echo osc_esc_html( osc_page_title() ); ?>" src="<?php echo gum_homebanner_url().'?'.filemtime(osc_uploads_path() . osc_get_preference('homebanner','gum_theme'));?>"/>
            <div class="clear"></div>
        </form>
    <?php } else { ?>
        <div class="flashmessage flashmessage-warning flashmessage-inline" style="display: block;">
            <p><?php _e('No <b>banner</b> has been uploaded yet', 'gum'); ?></p>
        </div>
    <?php } ?>
    <h2 class="render-title separate-top"><?php _e('Upload homepage banner', 'gum') ?></h2>
    <p><?php _e('The preferred size of the logo is 1440x520.', 'gum'); ?></p>
    <?php if( $homebanner_preference ) { ?>
    <div class="flashmessage flashmessage-inline flashmessage-warning"><p><?php _e('<strong>Note:</strong> Uploading another banner will overwrite the current banner.', 'gum'); ?></p></div>
    <?php } ?>
    <br/><br/>
    <form action="<?php echo osc_admin_render_theme_url('oc-content/themes/gum/admin/media.php'); ?>" method="post" enctype="multipart/form-data" class="nocsrf">
        <input type="hidden" name="action_specific" value="upload_homebanner" />
        <fieldset>
            <div class="form-horizontal">
                <div class="form-row">
                    <div class="form-label"><?php _e('Banner image (png,gif,jpg)','gum'); ?></div>
                    <div class="form-controls">
                        <input type="file" name="homebanner" id="homebanner" />
                    </div>
                </div>
                <div class="form-actions">
                    <input id="button_save" type="submit" value="<?php echo osc_esc_html(__('Upload banner','gum')); ?>" class="btn btn-submit">
                </div>
            </div>
        </fieldset>
    </form>
</div>
<?php } else { ?>
    <div class="flashmessage flashmessage-error" style="display: block;">
        <p>
            <?php
                $msg  = sprintf(__('The images folder <strong>%s</strong> is not writable on your server', 'gum'), WebThemes::newInstance()->getCurrentThemePath() ."images/" ) .", ";
                $msg .= __("Osclass can't upload the logo image from the administration panel.", 'gum') . ' ';
                $msg .= __('Please make the aforementioned image folder writable.', 'gum') . ' ';
                echo $msg;
            ?>
        </p>
        <p>
            <?php _e('To make a directory writable under UNIX execute this command from the shell:','gum'); ?>
        </p>
        <p class="command">
            chmod a+w <?php echo WebThemes::newInstance()->getCurrentThemePath() ."images/"; ?>
        </p>
    </div>
<?php } ?>
