<?php
if (Params::getParam('plugin_action') == 'done') {
    $footerLink  = Params::getParam('footer_link');
    osc_set_preference('footer_copyright', Params::getParam('footer_copyright'), 'gum_theme');
    osc_set_preference('footer_link', ($footerLink ? '1' : '0'), 'gum_theme');
    osc_set_preference('keyword_placeholder', Params::getParam('keyword_placeholder'), 'gum_theme');
    osc_set_preference('location_region_placeholder', Params::getParam('location_region_placeholder'), 'gum_theme');
    osc_set_preference('location_city_placeholder', Params::getParam('location_city_placeholder'), 'gum_theme');

    osc_set_preference('search_location_type', Params::getParam('search_location_type'), 'gum_theme');

    osc_set_preference('footer_social_networks', (Params::getParam('footer_social_networks') ? '1' : '0'), 'gum_theme');
    osc_set_preference('footer_social_links_target', (Params::getParam('footer_social_links_target') ? '1' : '0'), 'gum_theme');
    osc_set_preference('footer_blog_url', (trim(Params::getParam('footer_blog_url'))!="") ? addhttp(Params::getParam('footer_blog_url')) : '', 'gum_theme');
    osc_set_preference('footer_facebook_url', (trim(Params::getParam('footer_facebook_url'))!="") ? addhttp(Params::getParam('footer_facebook_url')) : '', 'gum_theme');
    osc_set_preference('footer_twitter_url', (trim(Params::getParam('footer_twitter_url'))!="") ? addhttp(Params::getParam('footer_twitter_url')) : '', 'gum_theme');
    osc_set_preference('footer_gplus_url', (trim(Params::getParam('footer_gplus_url'))!="") ? addhttp(Params::getParam('footer_gplus_url')) : '', 'gum_theme');
    osc_set_preference('footer_pinterest_url', (trim(Params::getParam('footer_pinterest_url'))!="") ? addhttp(Params::getParam('footer_pinterest_url')) : '', 'gum_theme');

    osc_set_preference('defaultLocationShowAs', Params::getParam('defaultLocationShowAs'), 'gum_theme');

    osc_set_preference('footer_seo_links', (Params::getParam('footer_seo_links') ? '1' : '0'), 'gum_theme');
    osc_set_preference('footer_seo_links_topsearches', (Params::getParam('footer_seo_links_topsearches') ? '1' : '0'), 'gum_theme');
    if(Params::getParam('footer_seo_links_topsearches')) {
        osc_set_preference('save_latest_searches', '1');
        osc_set_preference('purge_latest_searches', '1000');
    }
    osc_set_preference('footer_seo_links_toplocations', (Params::getParam('footer_seo_links_toplocations') ? '1' : '0'), 'gum_theme');
    osc_set_preference('footer_seo_links_toplocations_type', Params::getParam('footer_seo_links_toplocations_type'), 'gum_theme');

    osc_set_preference('ad_id', (Params::getParam('ad_id') ? '1' : '0'), 'gum_theme');

    // HACK : This will make possible use of the flash messages ;)
    ob_get_clean();
    osc_add_flash_ok_message(__('Theme settings updated correctly', 'gum'), 'admin');
    osc_redirect_to( osc_admin_render_theme_url('oc-content/themes/gum/admin/settings.php') );
}
?>
<form action="<?php echo osc_admin_render_theme_url('oc-content/themes/gum/admin/settings.php'); ?>" method="post" class="nocsrf">
    <input type="hidden" name="plugin_action" value="done" />
    <fieldset>
        <h2 class="render-title"><?php _e('Home page settings', 'gum'); ?></h2>
        <div class="form-horizontal">
            <div class="form-row">
                <div class="form-label"><?php _e('Seo links', 'gum'); ?></div>
                <div class="form-controls">
                    <div class="form-label-checkbox"><label><input type="checkbox" name="footer_seo_links" value="1" <?php echo (osc_get_preference('footer_seo_links', 'gum_theme') ? 'checked' : ''); ?> > <?php _e('Show seo links.', 'gum'); ?></label></div>
                    <span class="help-box"><?php _e('Show top searches and top locations at the bottom of the page.', 'gum'); ?></span>
                    <div class="gum-seo-links" style="margin-left: 1em;">
                        <div class="form-label-checkbox"><label><input type="checkbox" name="footer_seo_links_topsearches" value="1" <?php echo (osc_get_preference('footer_seo_links_topsearches', 'gum_theme') ? 'checked' : ''); ?> > <?php _e('Show <b>Top searches</b> tab.', 'gum'); ?></label></div>
                        <span class="help-box"><?php _e('Latest searches settings', 'gum'); ?>: <a href="<?php echo osc_admin_base_url(true).'?page=settings&action=latestsearches'?>"><?php osc_get_preference('save_latest_searches') ? _e('Enabled', 'gum') : _e('Disabled', 'gum');?></a></span>
                        <div class="form-label-checkbox"><label><input type="checkbox" name="footer_seo_links_toplocations" value="1" <?php echo (osc_get_preference('footer_seo_links_toplocations', 'gum_theme') ? 'checked' : ''); ?> > <?php _e('Show <b>Top locations</b> tab.', 'gum'); ?></label></div>
                        <ul>
                            <li><?php _e('Choose if you want region or city links:', 'gum'); ?></li>
                            <li><label><input type="radio" name="footer_seo_links_toplocations_type" value="region" <?php echo (osc_get_preference('footer_seo_links_toplocations_type', 'gum_theme')=='region' ? 'checked' : ''); ?>/><?php _e('Regions', 'gum'); ?></label></li>
                            <li><label><input type="radio" name="footer_seo_links_toplocations_type" value="city" <?php echo (osc_get_preference('footer_seo_links_toplocations_type', 'gum_theme')=='city' ? 'checked' : ''); ?>/><?php _e('Cities', 'gum'); ?></label></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <h2 class="render-title"><?php _e('Search settings', 'gum'); ?></h2>
        <div class="form-horizontal">
            <div class="form-row">
                <div class="form-label"><?php _e('Keyword  placeholder', 'gum'); ?></div>
                <div class="form-controls"><input type="text" class="xlarge" name="keyword_placeholder" value="<?php echo osc_esc_html( osc_get_preference('keyword_placeholder', 'gum_theme') ); ?>"></div>
            </div>
        </div>


        <div class="form-horizontal">
            <div class="form-row">
                <div class="form-label"><?php _e('Location, search by', 'gum'); ?></div>
                <div class="form-controls">
                    <div class="form-label-checkbox">
                        <label><input type="radio" name="search_location_type" value="region" <?php echo (osc_get_preference('search_location_type', 'gum_theme')=='region' ? 'checked' : ''); ?> > <?php _e('Search by region.', 'gum'); ?></label>
                        <p><label><input type="radio" name="search_location_type" value="city" <?php echo (osc_get_preference('search_location_type', 'gum_theme')=='city' ? 'checked' : ''); ?> > <?php _e('Search by city.', 'gum'); ?></label></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-horizontal location_region_placeholder">
            <div class="form-row">
                <div class="form-label"><?php _e('Location placeholder (search by region)', 'gum'); ?></div>
                <div class="form-controls"><input type="text" class="xlarge" name="location_region_placeholder" value="<?php echo osc_esc_html( osc_get_preference('location_region_placeholder', 'gum_theme') ); ?>"></div>
            </div>
        </div>
        <div class="form-horizontal location_city_placeholder">
            <div class="form-row">
                <div class="form-label"><?php _e('Location placeholder (search by city)', 'gum'); ?></div>
                <div class="form-controls"><input type="text" class="xlarge" name="location_city_placeholder" value="<?php echo osc_esc_html( osc_get_preference('location_city_placeholder', 'gum_theme') ); ?>"></div>
            </div>
        </div>

        <br>

        <h2 class="render-title"><?php _e('Location input', 'gum'); ?></h2>
        <div class="form-horizontal">
            <div class="form-row">
                <div class="form-label"><?php _e('Show location input as:', 'gum'); ?></div>
                <div class="form-controls">
                    <select name="defaultLocationShowAs">
                        <option value="dropdown" <?php if(gum_default_location_show_as() == 'dropdown'){ echo 'selected="selected"' ; } ?>><?php _e('Dropdown','gum'); ?></option>
                        <option value="autocomplete" <?php if(gum_default_location_show_as() == 'autocomplete'){ echo 'selected="selected"' ; } ?>><?php _e('Autocomplete','gum'); ?></option>
                    </select>
                </div>
            </div>
        </div>

        <h2 class="render-title"><?php _e('Footer settings', 'gum'); ?></h2>
        <div class="form-horizontal">
            <div class="form-row">
                <div class="form-label"><?php _e('Show social networks', 'gum'); ?></div>
                <div class="form-controls">
                    <div class="form-label-checkbox"><label><input type="checkbox" name="footer_social_networks" value="1" <?php echo (osc_get_preference('footer_social_networks', 'gum_theme') ? 'checked' : ''); ?> > <?php _e('Show social networks urls at footer page.', 'gum'); ?></label></div>
                </div>
            </div>
            <div class="gum-social-links">
                <div class="form-row">
                    <div class="form-controls">
                        <div class="form-label-checkbox"><label><input type="checkbox" name="footer_social_links_target" value="1" <?php echo (osc_get_preference('footer_social_links_target', 'gum_theme') ? 'checked' : ''); ?> > <?php _e('Open links in a new tab.', 'gum'); ?></label></div>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label"><i class="fa fa-rss"></i> <?php _e('Blog', 'gum'); ?></label>
                    <div class="form-controls">
                            <input type="text" name="footer_blog_url" value="<?php echo osc_esc_html(osc_get_preference('footer_blog_url', 'gum_theme') ); ?>"/>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label"><i class="fa fa-facebook"></i> <?php _e('Facebook', 'gum'); ?></label>
                    <div class="form-controls">
                        <input type="text" name="footer_facebook_url" value="<?php echo osc_esc_html(osc_get_preference('footer_facebook_url', 'gum_theme') ); ?>"/>
                        <span class="help-box"><?php _e('Please enter the full url', 'gum'); ?></span>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label"><i class="fa fa-twitter"></i> <?php _e('Twitter', 'gum'); ?></label>
                    <div class="form-controls">
                        <input type="text" name="footer_twitter_url" value="<?php echo osc_esc_html(osc_get_preference('footer_twitter_url', 'gum_theme') ); ?>"/>
                        <span class="help-box"><?php _e('Please enter the full url, like https://twitter.com/osclass', 'gum'); ?></span>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label"><i class="fa fa-google-plus"></i> <?php _e('Google+', 'gum'); ?></label>
                    <div class="form-controls">
                        <input type="text" name="footer_gplus_url" value="<?php echo osc_esc_html(osc_get_preference('footer_gplus_url', 'gum_theme') ); ?>"/>
                        <span class="help-box"><?php _e('Please enter the full url', 'gum'); ?></span>
                    </div>
                </div>

                <div class="form-row">
                    <label class="form-label"><i class="fa fa-pinterest"></i> <?php _e('Pinterest', 'gum'); ?></label>
                    <div class="form-controls">
                        <input type="text" name="footer_pinterest_url" value="<?php echo osc_esc_html(osc_get_preference('footer_pinterest_url', 'gum_theme') ); ?>"/>
                        <span class="help-box"><?php _e('Please enter the full url', 'gum'); ?></span>
                    </div>
                </div>

            </div>

            <div class="form-row">
                <div class="form-label"><?php _e('Copyright text', 'gum'); ?></div>
                <div class="form-controls"><input type="text" class="xlarge" name="footer_copyright" value="<?php echo osc_esc_html( osc_get_preference('footer_copyright', 'gum_theme') ); ?>"></div>
            </div>
            <div class="form-row">
                <div class="form-label"><?php _e('Footer link', 'gum'); ?></div>
                <div class="form-controls">
                    <div class="form-label-checkbox"><input type="checkbox" name="footer_link" value="1" <?php echo (osc_get_preference('footer_link', 'gum_theme') ? 'checked' : ''); ?> > <?php _e('I want to help Osclass by linking to <a href="http://osclass.org/" target="_blank">osclass.org</a> from my site with the following text:', 'gum'); ?></div>
                    <span class="help-box"><?php _e('This website is proudly using the <a title="Osclass web" href="http://osclass.org/">classifieds scripts</a> software <strong>Osclass</strong>', 'gum'); ?></span>
                </div>
            </div>
        </div>
        <h2 class="render-title"><?php _e('Listing page settings', 'gum'); ?></h2>
        <div class="form-horizontal">
            <div class="form-row">
                <div class="form-label"><?php _e('Ad identifier', 'gum'); ?></div>
                <div class="form-controls">
                    <div class="form-label-checkbox"><label><input type="checkbox" name="ad_id" value="1" <?php echo (osc_get_preference('ad_id', 'gum_theme') ? 'checked' : ''); ?> > <?php _e('Show <b>Ad id: 123</b> at listing detail page.', 'gum'); ?></label></div>
                </div>
            </div>
        </div>
        <div class="form-actions">
                <input type="submit" value="<?php _e('Save changes', 'gum'); ?>" class="btn btn-submit">
            </div>
        </div>
    </fieldset>
</form>

<script>
    $('body').on('click', 'input[name="footer_social_networks"]', function (e) {
        toggleSocialLinks();
    });
    $('body').on('click', 'input[name="footer_seo_links"]', function (e) {
        toggleSeoLinks();
    });
    $('body').on('click', 'input[name="search_location_type"]', function (e) {
        toggleLocationPlaceholder();
    });
    $(document).ready(function(){
        toggleSocialLinks();
        toggleSeoLinks();
        toggleLocationPlaceholder();
    });

    function toggleLocationPlaceholder() {
         if( $('input[name="search_location_type"]:checked').val() === 'region' ) {
            $('.location_region_placeholder').show();
        } else {
            $('.location_region_placeholder').hide();
        }
         if( $('input[name="search_location_type"]:checked').val() === 'city' ) {
            $('.location_city_placeholder').show();
        } else {
            $('.location_city_placeholder').hide();
        }
    }

    function toggleSeoLinks() {
         if( $('input[name="footer_seo_links"]:checked').length > 0 ) {
            $('.gum-seo-links').show();
        } else {
            $('.gum-seo-links').hide();
        }
    }
    function toggleSocialLinks() {
        if( $('input[name="footer_social_networks"]:checked').length > 0 ) {
            $('.gum-social-links').show();
        } else {
            $('.gum-social-links').hide();
        }
    }
</script>