<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');
$file = Params::getParam('file');
?>

    <div class="header-title-market">
        <h2><?php _e('Manage Gum theme settings from here.', 'gum'); ?></h2>
    </div>
    <ul class="tabs">
        <li <?php if($file == 'oc-content/themes/gum/admin/settings.php'){ echo 'class="active"';} ?>><a href="<?php echo osc_admin_render_theme_url('oc-content/themes/gum/admin/settings.php'); ?>"><?php _e('Theme settings', 'gum'); ?></a></li>
        <li <?php if($file == 'oc-content/themes/gum/admin/media.php'){ echo 'class="active"';} ?>><a href="<?php echo osc_admin_render_theme_url('oc-content/themes/gum/admin/media.php'); ?>"><?php _e('Logo/Favicon/Media settings', 'gum'); ?></a></li>
        <li <?php if($file == 'oc-content/themes/gum/admin/ad_managment.php'){ echo 'class="active"';} ?>><a href="<?php echo osc_admin_render_theme_url('oc-content/themes/gum/admin/ad_managment.php'); ?>"><?php _e('Ads management', 'gum'); ?></a></li>
        <li <?php if($file == 'oc-content/themes/gum/admin/tip_managment.php'){ echo 'class="active"';} ?>><a href="<?php echo osc_admin_render_theme_url('oc-content/themes/gum/admin/tip_managment.php'); ?>"><?php _e('Tips management', 'gum'); ?></a></li>
        <li <?php if($file == 'oc-content/themes/gum/admin/help.php'){ echo 'class="active"';} ?>><a href="<?php echo osc_admin_render_theme_url('oc-content/themes/gum/admin/help.php'); ?>"><?php _e('Help', 'gum'); ?></a></li>
    </ul>
    <?php
