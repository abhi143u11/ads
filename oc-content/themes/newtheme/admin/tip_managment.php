<?php
$tm = GumModelUsefulTips::newInstance();
$url = osc_admin_render_theme_url('oc-content/themes/gum/admin/tip_managment.php');
switch(Params::getParam('plugin_action')) {
    case 'addedit_tip':
        $id = Params::getParam("tip_id");
        $title = Params::getParam("tip_title");
        $description = Params::getParam("tip_description");

        if(strlen($title)<2 || strlen($description)<2) {
            ob_get_clean();
            osc_add_flash_error_message(__('Title and description are required', 'gum'), 'admin');
            osc_redirect_to($url);
        }

        if($id!='' && is_numeric($id)) {
            // EDIT
            $success = $tm->updateTip($id, $title, $description);
            $msg = __('Tip edited', 'gum');
        } else {
            // ADD
            $success = $tm->insertTip($title, $description);
            $msg = __('Tip added', 'gum');
        }
        ob_get_clean();
        osc_add_flash_ok_message($msg, 'admin');
        osc_redirect_to($url);
        break;
    case 'del_tip':
        $tm->deleteTip(Params::getParam("id"));
        ob_get_clean();
        osc_add_flash_ok_message(__('Tip deleted', 'easy_slider'), 'admin');
        osc_redirect_to($url);
        break;
    case 'up':
        $tm->sort(Params::getParam("id"), -1);
        ob_get_clean();
        osc_add_flash_ok_message(__('Tips sorted', 'easy_slider'), 'admin');
        osc_redirect_to($url);
        break;
    case 'down':
        $tm->sort(Params::getParam("id"), 1);
        ob_get_clean();
        osc_add_flash_ok_message(__('Tips sorted', 'easy_slider'), 'admin');
        osc_redirect_to($url);
        break;
    case 'enable':
        $tm->activate(Params::getParam("id"));
        ob_get_clean();
        osc_add_flash_ok_message(__('Tip enabled', 'easy_slider'), 'admin');
        osc_redirect_to($url);
        break;
    case 'disable':
        $tm->deactivate(Params::getParam("id"));
        ob_get_clean();
        osc_add_flash_ok_message(__('Tip disabled', 'easy_slider'), 'admin');
        osc_redirect_to($url);
        break;
    default:
        break;
}

$tips = $tm->all('all');
?>
<style>
    .tip-wrapper {
        border:1px solid rgb(221, 221, 221);
        border-radius: 3px;
        color: #616161;
    }
    .tip {
        padding:7px 14px 7px 14px;
        position: relative;
    }
    .up-down {
        position: absolute;
        left: 14px;
        border: 1px solid rgb(221, 221, 221);
        border-radius: 3px;
        padding-left: 3px;
    }
    .up-down a:hover{
        color:#616161;
    }
    .tip:first-of-type {
        padding:14px 14px 7px 14px;
    }
    .tip .tip-name,
    .tip .tip-description,
    .tip .tip-action {
        display:block;
        margin-bottom: 8px;
    }
    .tip-label{
        display: inline;
        width: 110px;
        margin-right: 30px;
        float:left;
        text-align: right;
        font-weight: 500;
    }
    .tip .tip-actions {
        display: inline-block;
        margin-left: 140px;
    }
</style>
<h2 class="render-title"><?php _e('Tips management', 'gum'); ?> <span style="font-size: smaller;"><a href="#" id="add_tip_btn"><?php _e('Add new tip', 'gum'); ?></a></span></h2>
<div class="form-row">
    <div class="form-label"></div>
    <div class="form-controls">
        <p><?php _e('In this section you can configure your site to display tips.', 'gum'); ?></p>
        <p><?php _e('Tips are shown at listing page and listing contact page.', 'gum'); ?></p>
    </div>
</div>
<fieldset>
    <div class="form-horizontal tip-wrapper">

        <?php foreach($tips as $key => $tip) { ?>
            <div class="tip">
                <div class="up-down">
                    <span><a class="_btn btn-mini" href="<?php echo $url . "&plugin_action=up&id=" . $tip['pk_i_id']; ?>"><i class="fa fa-caret-up fa-2x"></i></a></span>
                    <br>
                    <span><a class="_btn btn-mini" href="<?php echo $url . "&plugin_action=down&id=" . $tip['pk_i_id']; ?>"><i class="fa fa-caret-down fa-2x"></i></a></span>
                </div>
                <div class="tip-info <?php echo $tip['b_active']==1?'tip-enabled':'tip-disabled'; ?>">
                    <label class="tip-label"><?php _e('Title', 'gum'); ?>:</label><label class="tip-name"><?php echo $tip['s_title']; ?></label>
                    <label class="tip-label"><?php _e('Tip content', 'gum'); ?>:</label><label class="tip-description"><?php echo $tip['s_description']; ?></label>
                    <div class="tip-actions">

                        <?php if($tip['b_active']==0) { ?><span><a class="btn btn-mini" href="<?php echo $url . "&plugin_action=enable&id=" . $tip['pk_i_id']; ?>"><?php _e('enable', 'gum'); ?></a></span><?php }; ?>
                        <?php if($tip['b_active']==1) { ?><span><a class="btn btn-mini" href="<?php echo $url . "&plugin_action=disable&id=" . $tip['pk_i_id']; ?>"><?php _e('disable', 'gum'); ?></a></span><?php }; ?>


                        <span><a class="btn btn-mini" href="javascript:editTip(<?php echo $tip['pk_i_id']; ?>);"><?php _e('Edit', 'gum'); ?></a></span>
                        <span><a class="btn btn-mini" href="javascript:deleteTip(<?php echo $tip['pk_i_id']; ?>);"><?php _e('Delete', 'gum'); ?></a></span>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <?php if($key!=count($tips)-1){ ?><hr><?php } else { ?><br><?php } ?>
        <?php }; ?>
    </div>
</fieldset>


<div id="form_tip_div" class="hide">
    <form id="form_tip" method="post" enctype="multipart/form-data" action="<?php echo $url; ?>">
        <input type="hidden" name="plugin_action" id="plugin_action" value="addedit_tip">
        <input type="hidden" name="tip_id" id="tip_id" value="">
        <div class="_table">
            <div id="tip_error" class="tip_error"></div>

            <p>
                <label class="tip-label"><?php _e('Title', 'gum'); ?></label>
                <input type="text" name="tip_title" value="" id="tip_title">
            </p>

            <p>
                <label class="tip-label"><?php _e('Description', 'gum'); ?></label>
                <textarea rows="7" type="text" name="tip_description" id="tip_description"></textarea>
            </p>

            <div class="_row" id="gum-content-help">
            </div>
        </div>
        <div class="clear"></div>
    </form>
    <div class="action">
        <a href="#" class="btn btn-red" style="margin-left: 20px;" onclick="$('#form_tip_div').dialog('close');" id="form_tip_cancel_btn"><?php _e('Cancel', 'gum'); ?></a>
        <a href="#" class="btn btn-primary" id="form_tip_btn"><?php _e('Save changes', 'gum'); ?></a>
    </div>
</div>

<script type="text/javascript">
    $("#form_tip_div").dialog({
        autoOpen: false,
        modal: true,
        maxWidth:600,
        maxHeight: 400,
        width: 450,
        heigth:"auto",
        title: '<?php echo osc_esc_js( __('Tip', 'gum') ); ?>'
    });
    $("#add_tip_btn").on("click", function() {
        $("#tip_error").html();
        $("#tip_id").attr("value", "");
        $("#tip_title").attr("value", "");
        $("#tip_description").attr("value", "");

        $("#form_tip_div").dialog("open");
    });
    $("#form_tip_btn").on("click", function() {
        var errors = '';
        if($("#tip_title").attr("value").length<2) {
            errors += '<p><?php _e('Title is required', 'gum'); ?></p>';
        }
        if($("#tip_description").val().length<2) {
            errors += '<p><?php _e('Description is required', 'gum'); ?></p>';
        }
        if(errors!="") {
            $("#tip_error").html(errors);
        } else {
            $("#form_tip").submit();
        }

    });

    function deleteTip(id) {
        var x = confirm('<?php _e('Do you want to delete the tip? This action can not be undone.', 'gum'); ?>');
        if(x) {
            window.location = '<?php echo $url; ?>&plugin_action=del_tip&id=' + id;
        }
    }

    function editTip(tipid) {
        $("#tip_error").html();
        $("#tip_id").attr("value", tipid);
        $("#tip_title").attr("value", "");
        $("#tip_description").val();

        $("#form_tip_div").dialog("open");

        $.getJSON(
            '<?php echo osc_admin_base_url(true); ?>',
            {page:'ajax', action:'runhook', hook:'gum_tip', id: tipid},
            function (data) {
                $("#tip_error").html();
                $("#tip_id").attr("value", tipid);
                $("#tip_title").attr("value", data.s_title);
                $("#tip_description").val(data.s_description);
            }
        )
    }


</script>

