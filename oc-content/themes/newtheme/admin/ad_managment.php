<?php
if (Params::getParam('plugin_action') == 'done') {
    osc_set_preference('ad-homepage-sidebar-300x250', trim(Params::getParam('ad-homepage-sidebar-300x250', false, false, false)), 'gum_theme');
    osc_set_preference('ad-homepage-middle-728x90', trim(Params::getParam('ad-homepage-middle-728x90', false, false, false)), 'gum_theme');

    osc_set_preference('sidebar-300x250', trim(Params::getParam('sidebar-300x250', false, false, false)), 'gum_theme');
    osc_set_preference('listing-728x90', trim(Params::getParam('listing-728x90', false, false, false)), 'gum_theme');
    osc_set_preference('search-results-top-728x90', trim(Params::getParam('search-results-top-728x90', false, false, false)), 'gum_theme');
    osc_set_preference('search-results-middle-728x90', trim(Params::getParam('search-results-middle-728x90', false, false, false)), 'gum_theme');
    osc_set_preference('search-results-bottom-728x90', trim(Params::getParam('search-results-bottom-728x90', false, false, false)), 'gum_theme');
    osc_set_preference('search-sidebar-160x600', trim(Params::getParam('search-sidebar-160x600', false, false, false)), 'gum_theme');

    // HACK : This will make possible use of the flash messages ;)
    ob_get_clean();
    osc_add_flash_ok_message(__('Ads updated correctly', 'gum'), 'admin');
    osc_redirect_to( osc_admin_render_theme_url('oc-content/themes/gum/admin/ad_managment.php') );
}
?>
<h2 class="render-title"><?php _e('Ads management', 'gum'); ?></h2>
<form action="<?php echo osc_admin_render_theme_url('oc-content/themes/gum/admin/ad_managment.php'); ?>" method="post" class="nocsrf">
    <input type="hidden" name="plugin_action" value="done" />
    <div class="form-row">
        <div class="form-label"></div>
        <div class="form-controls">
            <p><?php _e('In this section you can configure your site to display ads and start generating revenue.', 'gum'); ?><br/><?php _e('If you are using an online advertising platform, such as Google Adsense, copy and paste here the provided code for ads.', 'gum'); ?></p>
        </div>
    </div>
    <fieldset>
        <div class="form-horizontal">

            <div class="form-row">
                <div class="form-label"><?php _e('Homepage 300x250 / Page not found', 'gum'); ?></div>
                <div class="form-controls">
                    <textarea style="height: 115px; width: 500px;" name="ad-homepage-sidebar-300x250"><?php echo osc_esc_html( osc_get_preference('ad-homepage-sidebar-300x250', 'gum_theme') ); ?></textarea>
                    <br/><br/>
                    <div class="help-box"><?php _e('This ad will be shown on the main site of your website. It will appear both at the top and bottom of your site homepage. Note that the size of the ad has to be 728x90 pixels.', 'gum'); ?></div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-label"><?php _e('Homepage 728x90', 'gum'); ?></div>
                <div class="form-controls">
                    <textarea style="height: 115px; width: 500px;"name="ad-homepage-middle-728x90"><?php echo osc_esc_html( osc_get_preference('ad-homepage-middle-728x90', 'gum_theme') ); ?></textarea>
                    <br/><br/>
                    <div class="help-box"><?php _e('This ad will be shown at the top of your website, next to the site title and above the search results. Note that the size of the ad has to be 728x90 pixels.', 'gum'); ?></div>
                </div>
            </div>


            <div class="form-row">
                <div class="form-label"><?php _e('Search results 728x90 (<b>top</b> of the page)', 'gum'); ?></div>
                <div class="form-controls">
                    <textarea style="height: 115px; width: 500px;" name="search-results-top-728x90"><?php echo osc_esc_html( osc_get_preference('search-results-top-728x90', 'gum_theme') ); ?></textarea>
                    <br/><br/>
                    <div class="help-box"><?php _e('This ad will be shown on top of the search results of your site. Note that the size of the ad has to be 728x90 pixels.', 'gum'); ?></div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-label"><?php _e('Search results 728x90 (<b>middle</b> of the page)', 'gum'); ?></div>
                <div class="form-controls">
                    <textarea style="height: 115px; width: 500px;" name="search-results-middle-728x90"><?php echo osc_esc_html( osc_get_preference('search-results-middle-728x90', 'gum_theme') ); ?></textarea>
                    <br/><br/>
                    <div class="help-box"><?php _e('This ad will be shown among the search results of your site. Note that the size of the ad has to be 728x90 pixels.', 'gum'); ?></div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-label"><?php _e('Search results 728x90 (<b>bottom</b> of the page)', 'gum'); ?></div>
                <div class="form-controls">
                    <textarea style="height: 115px; width: 500px;" name="search-results-bottom-728x90"><?php echo osc_esc_html( osc_get_preference('search-results-bottom-728x90', 'gum_theme') ); ?></textarea>
                    <br/><br/>
                    <div class="help-box"><?php _e('This ad will be shown among the search results of your site. Note that the size of the ad has to be 728x90 pixels.', 'gum'); ?></div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-label"><?php _e('Search sidebar 160x600', 'gum'); ?></div>
                <div class="form-controls">
                    <textarea style="height: 115px; width: 500px;" name="search-sidebar-160x600"><?php echo osc_esc_html( osc_get_preference('search-sidebar-160x600', 'gum_theme') ); ?></textarea>
                    <br/><br/>
                    <div class="help-box"><?php _e('This ad will be shown at the right sidebar of your website, on the product detail page. Note that the size of the ad has to be 160x600 pixels.', 'gum'); ?></div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-label"><?php _e('Listing detail sidebar 300x250', 'gum'); ?></div>
                <div class="form-controls">
                    <textarea style="height: 115px; width: 500px;" name="sidebar-300x250"><?php echo osc_esc_html( osc_get_preference('sidebar-300x250', 'gum_theme') ); ?></textarea>
                    <br/><br/>
                    <div class="help-box"><?php _e('This ad will be shown at the right sidebar of your website, on the product detail page. Note that the size of the ad has to be 300x250 pixels.', 'gum'); ?></div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-label"><?php _e('listing-728x90', 'gum'); ?></div>
                <div class="form-controls">
                    <textarea style="height: 115px; width: 500px;" name="listing-728x90"><?php echo osc_esc_html( osc_get_preference('listing-728x90', 'gum_theme') ); ?></textarea>
                    <br/><br/>
                    <div class="help-box"><?php _e('This ad will be shown at the right sidebar of your website, on the product detail page. Note that the size of the ad has to be 300x250 pixels.', 'gum'); ?></div>
                </div>
            </div>
            <div class="form-actions">
                <input type="submit" value="<?php _e('Save changes', 'gum'); ?>" class="btn btn-submit">
            </div>
        </div>
    </fieldset>
</form>