<?php
    // meta tag robots
    osc_add_hook('header','gum_nofollow_construct');

    osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php') ;
    View::newInstance()->_exportVariableToView('user', User::newInstance()->findByPrimaryKey(osc_logged_user_id()));

?>
<script type="text/javascript">
$(document).ready(function() {
    $('form#change-username').validate({
        rules: {
            s_username: {
                required: true
            }
        },
        messages: {
            s_username: {
                required: '<?php echo osc_esc_js(__("Username: this field is required", "gum")); ?>.'
            }
        },
        errorLabelContainer: "#error_list",
        wrapper: "li",
        invalidHandler: function(form, validator) {
            $('html,body').animate({ scrollTop: $('h1').offset().top }, { duration: 250, easing: 'swing'});
        },
        submitHandler: function(form){
            $('button[type=submit], input[type=submit]').attr('disabled', 'disabled');
            form.submit();
        }
    });

    var cInterval;
    $("#s_username").keydown(function(event) {
        if($("#s_username").val()!='') {
            clearInterval(cInterval);
            cInterval = setInterval(function(){
                $.ajax({
                dataType: "json",
                url: "<?php echo osc_base_url(true); ?>?page=ajax&action=check_username_availability",
                data: {"s_username": $("#s_username").val()},
                beforeSend: function() {
                    $('button[type=submit], input[type=submit]').attr('disabled', 'disabled');
                },
                complete: function(jqXHR, status) {
                },
                success: function(data){
                        clearInterval(cInterval);
                        if(data.exists==0) {
                            $("#available").html('<span class="text-success"><?php echo osc_esc_js(__("The username is available", "gum")); ?></span>');
                            $('button[type=submit], input[type=submit]').removeAttr('disabled');
                        } else {
                            $("#available").html('<span class="text-danger"><?php echo osc_esc_js(__("The username is NOT available", "gum")); ?></span>');
                        }
                    }
              });
            }, 1000);
        }
    });

});
</script>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12 custom-tabs">
                <?php osc_current_web_theme_path('user-sidebar.php'); ?>
                <div class="col-xs-12 col-md-8 col-lg-9">
                    <h1 class="h1-custom col-xs-12"><?php _e('Change username', 'gum'); ?></h1>
                    <form id="change-username" name="register" action="<?php echo osc_base_url(true); ?>" method="post" >
                            <input type="hidden" name="page" value="user" />
                            <input type="hidden" name="action" value="change_username_post" />
                            <div class="clearfix"></div>
                            <ul id="error_list"></ul>

                            <div class="col-xs-12 col-md-6 form-group">
                                <label for="s_username"><?php _e('Username', 'gum'); ?></label>
                                <input class="form-control" type="text" name="s_username" id="s_username" value="" />
                                <p id="available" class="text-danger"></p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary-custom btn-block"><?php _e("Update", 'gum'); ?></button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<?php osc_current_web_theme_path('footer.php') ; ?>