CREATE TABLE `/*TABLE_PREFIX*/t_gum_useful_tips` (
  `pk_i_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `i_order` int(11) DEFAULT NULL,
  `s_title` varchar(150) DEFAULT NULL,
  `s_description` VARCHAR(250) DEFAULT NULL,
  `b_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
