<?php
// meta tag robots
osc_add_hook('header','gum_follow_construct');

osc_current_web_theme_path('header.php');

?>
<div class="container-fluid main">
    <div class="container">
        <div class="row">
            <div class="col-lg-8  col-md-8">
                <div>
                    <h1 class="item-title"><?php echo osc_item_title(); ?></h1>
                    <span class="h4 location pull-left"><?php echo gum_get_item_location(); ?></span>
                    <?php if (osc_price_enabled_at_items()) : ?>
                        <span class="h4 price pull-right"><?php echo osc_item_formated_price(); ?></span>
                        <div class="clearfix"></div>
                    <?php endif; ?>
                </div>
                <div class="custom-tabs">
                    <?php if (osc_count_item_resources() > 0 && GumGeo::_get_listing_location_string(osc_item())!='' ) : ?>
                    <ul id="itemTabs" class="nav nav-tabs">
                        <li role="presentation" class="active"><a href="#images" class="text-center"><i class="fa fa-picture-o"></i> <?php _e('Images', 'gum'); ?></a></li>
                        <li role="presentation"><a href="#map" class="text-center"><i class="fa fa-map-marker"></i> <?php _e('Map', 'gum'); ?></a></li>
                    </ul>
                    <?php endif; ?>

                    <?php if(GumGeo::_get_listing_location_string(osc_item())!='' || osc_count_item_resources() > 0) { ?>
                    <div class="tab-content">
                        <?php if (osc_count_item_resources() > 0) : ?>
                        <div role="tabpanel" class="tab-pane active" id="images">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <ul class="bxslider">
                                    <?php while(osc_has_item_resources()) : ?>
                                        <li><img width="100%" class="img-responsive" src="<?php echo osc_resource_preview_url(); ?>" alt="<?php echo osc_esc_html(osc_item_title()) ; ?>" /></li>
                                    <?php endwhile; ?>
                                    </ul>
                                </div>
                                <div class="col-md-12 text-center item-image-box">
                                    <?php View::newInstance()->_exportVariableToView('resources', ItemResource::newInstance()->getAllResourcesFromItem( osc_item_id() ) );
                                    if (osc_count_item_resources() > 0) : ?>
                                    <div id="bx-pager" class="text-left">
                                        <?php $i = 0; while(osc_has_item_resources()) : ?>
                                        <a data-slide-index="<?php echo $i; ?>" href=""><img src="<?php echo osc_resource_preview_url(); ?>" /></a>
                                    <?php $i++; endwhile; ?>
                                  </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>

                        <?php /*
                         * Si hay location definida, GumGeo::_get_listing_location_string != '' -> enseñar el mapa
                         * sino no enseñar mapa.
                         */?>
                        <?php if(GumGeo::_get_listing_location_string(osc_item())!='') { ?>
                        <div role="tabpanel" class="tab-pane <?php if(osc_count_item_resources()==0) { echo 'active';} ?>" id="map">
                            <div class="text-center">
                                <div id="mapbox"></div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
            </div>

            <div class="pull-right col-lg-4 col-lg-push-0 col-md-4 col-md-push-0 col-sm-6  col-xs-12">
                <div class="row box-border">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="seller-info">
                                <div class="h5"><strong class="clearfix text-capitalize"><?php echo osc_item_contact_name();?></strong></div>
                                <?php if( osc_item_user_id() > 0) {
                                View::newInstance()->_exportVariableToView('user', User::newInstance()->findByPrimaryKey(osc_item_user_id())); ?>
                                <span><?php printf(__('Posting for %s', 'gum'),  gum_elapsed_time(osc_user_regdate(), true) );?></span>
                                <span class="pull-right"><a href="<?php echo osc_search_url(array('sUser' => osc_user_username())); ?>"><?php _e('See all ads', 'gum'); ?></a></span>
                            <?php } ?>
                            </div>
                            <div>
                                <div class="h5"><strong class="clearfix text-capitalize"><?php _e('Contact details', 'gum');?></strong></div>
                                <div class="form-group">
                                <?php if(osc_user_phone()!='') : ?>
                                    <i class="fa fa-phone-square"></i> <span class="phone phone_hide"><?php echo osc_user_phone(); ?></span>

                                    <span class="pull-right"><a class="btn btn-default btn-arrow-left" onclick="reveal_phone($(this));" href="#"><?php _e('Reveal', 'gum'); ?></a></span>
                                <?php endif; ?>
                                </div>
                                <?php if( osc_item_is_expired() ) { ?>
                                    <p class="alert alert-danger">
                                        <?php _e("The listing is expired. You can't contact the publisher.", 'gum'); ?>
                                    </p>
                                <?php } else if( ( osc_logged_user_id() == osc_item_user_id() ) && osc_logged_user_id() != 0 ) { ?>
                                    <p class="alert alert-danger">
                                        <?php _e("It's your own listing, you can't contact the publisher.", 'gum'); ?>
                                    </p>
                                <?php } else if( osc_reg_user_can_contact() && !osc_is_web_user_logged_in() ) { ?>
                                    <div class="alert alert-danger">
                                        <p><?php _e("You must log in or register a new account in order to contact the advertiser", 'gum'); ?></p>
                                        <br>
                                        <div class="col-xs-6 button-nopadding">
                                            <a class="btn btn-secondary-custom btn-block truncate-line" href="<?php echo osc_user_login_url(); ?>">
                                                <strong><?php _e('Login', 'gum') ; ?></strong>
                                            </a>
                                        </div>
                                        <div class="col-xs-6 button-nopadding">
                                            <a href="<?php echo osc_register_account_url(); ?>" class="btn btn-primary-custom btn-block truncate-line" title="<?php echo osc_esc_html(__('Register for a free account', 'gum')) ; ?>">
                                                <strong><?php _e('Register for a free account', 'gum') ; ?></strong>
                                            </a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                <?php } else { ?>
                                <div class="form-group">
                                    <a class="btn btn-primary-custom col-xs-12" href="<?php echo osc_item_contact_url(); ?>">
                                        <i class="fa fa-envelope-o"></i> <strong><?php _e('Email', 'gum') ; ?></strong>
                                    </a>
                                </div>
                                <?php } ?>
                                <div class="clearfix"></div>
                            </div>
                            <hr class="hr-custom">
                            <div>
                                <div class="col-xs-6 button-nopadding">
                                    <?php if(function_exists('watchlist')) { ?>
                                    <a class="btn btn-secondary-custom btn-block watchlist" id="<?php echo osc_item_id(); ?>" href="javascript://">
                                        <i class="fa fa-star-o"></i> <strong><?php _e('Favourite', 'gum') ; ?></strong>
                                    </a>
                                    <?php } ?>
                                </div>
                                <div class="col-xs-6 button-nopadding">
                                    <a href="#" class="btn btn-secondary-custom btn-report btn-block">
                                        <i class="fa fa-exclamation-triangle"></i> <strong><?php _e('Report', 'gum') ; ?></strong> <span class="caret"></span>
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12 button-nopadding">
                                    <ul id="dropdown-menu" class="dropdown-menu">
                                        <li>
                                             <form action="<?php echo osc_base_url(true); ?>" method="post" name="mask_as_form" id="mask_as_form">
                                                <input type="hidden" name="id" value="<?php echo osc_item_id(); ?>" />
                                                <input type="hidden" name="action" value="mark" />
                                                <input type="hidden" name="page" value="item" />
                                                <div class="form-group">
                                                    <input type="radio" name="as" value="spam" id="mark_a_spam"/>
                                                    <label class="clearfix" for="mark_a_spam"><?php _e("Mark as spam", 'gum'); ?></label>
                                                </div>
                                                <div class="form-group">
                                                    <input type="radio" name="as" value="badcat" id="mark_as_badcat"/>
                                                    <label  for="mark_as_badcat"><?php _e("Mark as misclassified", 'gum'); ?></label>
                                                </div>
                                                <div class="form-group">
                                                    <input type="radio" name="as" value="repeated" id="mark_as_repeated"/>
                                                    <label  for="mark_as_repeated"><?php _e("Mark as duplicated", 'gum'); ?></label>
                                                </div>
                                                <div class="form-group">
                                                    <input type="radio" name="as" value="expired" id="mark_as_expired"/>
                                                    <label  for="mark_as_expired"><?php _e("Mark as expired", 'gum'); ?></label>
                                                </div>
                                                <div class="form-group">
                                                    <input type="radio" name="as" value="offensive" id="mark_as_offensive"/>
                                                    <label for="mark_as_offensive"><?php _e("Mark as offensive", 'gum'); ?></label>
                                                </div>
                                                <div class="col-xs-6 button-nopadding">
                                                    <a class="btn btn-block btn-secondary-custom close-mark-as" href="#"><?php _e('Cancel', 'gum') ; ?></a>
                                                </div>
                                                <div class="col-xs-6 button-nopadding">
                                                    <button type="submit" class="form_submit btn btn-block btn-primary-custom" ><?php _e('Send', 'gum'); ?></button>
                                                </div>
                                                <div class="clearfix"></div>
                                            </form>
                                        </li>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 visible-md-block visible-lg-block">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <?php
                            $demo = '<p style="text-align: center;"><img style="padding:0.5em 0;" src="'.osc_current_web_theme_url('images/banner-300x250.gif').'"/></p>';
                            $ad = osc_get_preference('sidebar-300x250', 'gum_theme');
                            echo ($ad=="#demo#") ? $demo : $ad;
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 visible-sm-block visible-md-block visible-lg-block">
                    <hr class="hr-custom">
                    <?php GumViewHelpers::show_social_buttons(); ?>
                    <hr class="hr-custom">
                </div>

                <?php /* Useful tips */ ?>
                <div class="col-sm-12 visible-sm-block visible-md-block visible-lg-block">
                    <div class="row box-border ">
                        <?php GumViewHelpers::show_useful_tips('1'); ?>
                    </div>
                </div>
            </div>

            <div class="pull-left col-lg-8 col-lg-pull-0 col-md-8 col-md-pull-0 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="row listing-info list-unstyled">
                            <li class="col-md-6"><?php _e('Posted', 'gum'); ?><span class="pull-right"><span><?php printf(__('%s ago', 'gum'),  gum_elapsed_time(osc_item_pub_date()) );?></span></span></li>
                            <li class="col-md-12 title-section"><?php _e('Description', 'gum'); ?></li>
                        </ul>
                        <p><?php echo osc_item_description(); ?></p>
                        <?php if(osc_get_preference('ad_id', 'gum_theme')) { ?>
                        <p><?php _e('Ad id:', 'gum'); ?>&nbsp<?php echo osc_item_id(); ?></p>
                        <?php } ?>

                        <?php osc_run_hook('item_detail', osc_item() ); ?>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 visible-xs-block visible-ms-block visible-sm-block">
                <hr class="hr-custom">
                <?php GumViewHelpers::show_social_buttons(); ?>
                <hr class="hr-custom">
            </div>

            <div class="col-xs-12 visible-xs-block visible-ms-block visible-sm-block">
                <div class="row box-border ">
                    <?php GumViewHelpers::show_useful_tips('2'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid main-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <?php
                $demo = '<p style="text-align: center;"><img style="padding:0.5em 0;" src="'.osc_current_web_theme_url('images/banner-728x90.jpg').'"/></p>';
                $ad = osc_get_preference('listing-728x90', 'gum_theme');
                echo ($ad=="#demo#") ? $demo : $ad;
                ?>
            </div>
        </div>
    </div>
</div>

<?php gum_related_listings();
if(osc_count_premiums()>0): ?>
<div class="container-fluid main-premium">
    <div class="container">
        <div class="container-fluid main-premium">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <span class="pull-left lead"><?php _e('Related Ads', 'gum'); ?></span>
                        <ul class="list-inline pull-right">
                                <li>
                                    <a onclick="bxSlider_premium.goToPrevSlide();">
                                         <i class="fa fa-chevron-left fa-2x"></i>
                                    </a>
                                </li>
                                <li >
                                    <a onclick="bxSlider_premium.goToNextSlide();">
                                        <i class="fa fa-chevron-right fa-2x"></i>
                                    </a>
                                </li>
                                </li>
                        </ul>
                    </div>
                </div>

                <div class="row" id="premium-slider-wrapper">
                    <ul class="bxslider_premium">
                    <?php $i = 0; while (osc_has_premiums() ) {
                        echo '<li>';
                        gum_draw_item_premium();
                        echo '</li>';
                        $i++;
                    } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>



<script>
    var bxSlider;
    var bxSlider_premium;
    var bxSlider_tips;
    $(document).ready(function(){
        bxSlider = $('.bxslider').bxSlider({
            pagerCustom: '#bx-pager',
            responsive : true,
            adaptiveHeight: true,
            mode: 'fade'
        });
        bxSlider_premium = $('.bxslider_premium').bxSlider({
            pager: false,
            controls: false,
            minSlides: 2,
            maxSlides: 5,
            slideWidth: 200,
            infiniteLoop: false,
            slideMargin: 0,
            moveSlides: 1
        });
        bxSlider_tips_1 = $('.bxslider_tips_1').bxSlider({
            pager: false,
            controls: false,
            responsive : true,
            adaptiveHeight: true,
            moveSlides: 1
        });
        bxSlider_tips_2 = $('.bxslider_tips_2').bxSlider({
            pager: false,
            controls: false,
            responsive : true,

            moveSlides: 1
        });
        <?php if(GumGeo::_get_listing_location_string(osc_item())!='') { ?>
        initialize();
        <?php } ?>
    });


    $('.form_submit').click(function (e) {
        e.preventDefault();
        $(this).prop('disabled', true);
        $('#mask_as_form').submit();
    });

    $('#itemTabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
        if($(this).attr('href')=="#map") {
            initialize();
            google.maps.event.trigger(map, 'resize');
        }
        if($(this).attr('href')=="#images") {
            bxSlider.reloadSlider();
        }
    });

    <?php if(GumGeo::_get_listing_location_string(osc_item())!='') { ?>
    var map;
    var mapCanvas;
    function initialize() {
        mapCanvas = document.getElementById('mapbox');
        var mapOptions = {
          center: new google.maps.LatLng(<?php echo osc_item_latitude(); ?>, <?php echo osc_item_longitude(); ?>),
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(mapCanvas, mapOptions);

        var iconBase = 'src="http://maps.google.com/mapfiles/kml/paddle/';
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(<?php echo osc_item_latitude(); ?>, <?php echo osc_item_longitude(); ?>),
          map: map,
          icon:  '<?php echo osc_current_web_theme_url('images/marker.png'); ?>'
        });
     }
    <?php } ?>

     function reveal_phone(that) {
        $(that).addClass('disabled');
        $('.phone').removeClass('phone_hide');
        // increase phone view
        increasePhoneView();
     }

     $('.btn-report, .close-mark-as').click(function(event) {
         event.preventDefault();
         $('.btn-report').toggleClass('active');
         $('#dropdown-menu').toggle();
     });


    <?php list($name, $token) = osc_csrfguard_generate_token(); ?>
     function increasePhoneView() {
         $.ajax({
            dataType: "json",
            url: "<?php echo osc_ajax_hook_url('phone_reveal', null); ?>",
            data: {
                "id": '<?php echo osc_item_id(); ?>',
                "CSRFName" : '<?php echo $name; ?>',
                "CSRFToken" : '<?php echo $token; ?>'
            },
            success: function(data){
                    if(data.error==0) {
                        console.log('NO error');
                    } else {
                        console.log('error :(');
                    }
                }
          });
     }
</script>
<?php osc_current_web_theme_path('footer.php'); ?>