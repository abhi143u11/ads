<?php
    // meta tag robots
    osc_add_hook('header','gum_follow_construct');


    osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php') ;
?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 col-sm-12">
                <h1 class="h1-custom"><?php _e('Contact us', 'gum'); ?></h1>
            </div>
            <div class="col-md-offset-2 col-md-8 col-sm-12">
                <div class="row box-border">
                    <div class="">
                        <ul id="error_list"></ul>
                        <form name="contact_form" action="<?php echo osc_base_url(true); ?>" method="post">
                            <input type="hidden" name="page" value="contact" />
                            <input type="hidden" name="action" value="contact_post" />
                            <h2 class="h2-custom underline">&nbsp;<span class="pull-right text-normal">*&nbsp;<?php _e('Required', 'gum'); ?></span></h2>
                            <div class="form-group col-xs-12">
                                <label class="control-label" for="yourName"><?php _e('Your name', 'gum'); ?>*</label>
                                <?php GumContactForm::your_name(); ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-xs-12">
                                <label class="control-label" for="yourEmail"><?php _e('Your email address', 'gum'); ?>*</label>
                                <?php GumContactForm::your_email(); ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-xs-12">
                                <label class="control-label" for="subject"><?php _e('Subject', 'gum'); ?> (<?php _e('optional', 'gum'); ?>)</label>
                                <?php GumContactForm::the_subject(); ?>
                            </div>
                            <div class="form-group col-xs-12">
                                <label class="control-label" for="message"><?php _e('Message', 'gum'); ?>*</label>
                                <?php GumContactForm::your_message(); ?>
                            </div>
                            <?php if (osc_recaptcha_public_key()) { ?>
                            <div class="form-group text-left">
                                <div class="col-xs-6">
                                <?php osc_show_recaptcha(); ?>
                                    <br>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <?php } ?>
                            <div class="form-group">
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-primary-custom"><?php _e("Send", 'gum'); ?></button>
                                </div>
                                <?php osc_run_hook('admin_contact_form'); ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php GumContactForm::js_validation(); ?>
<?php osc_current_web_theme_path('footer.php') ; ?>