<?php
    // meta tag robots
    osc_add_hook('header','gum_follow_construct');

    osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php') ;
?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="h1-custom"><?php _e('Create an account', 'gum'); ?></h1>
            </div>
            <div class="col-xs-12">
                <div class="">
                    <form name="register" class="col-xs-12  col-sm-6 col-md-8 register-social box-border" action="<?php echo osc_base_url(true); ?>" method="post" >
                        <input type="hidden" name="page" value="register" />
                        <input type="hidden" name="action" value="register_post" />
                        <h2 class="h2-custom underline"><?php _e('Your details', 'gum'); ?><span class="pull-right text-normal">*&nbsp;<?php _e('Required', 'gum'); ?></span></h2>
                        <ul id="error_list"></ul>
                        <div class="col-xs-12 form-group">
                            <label for="s_name"><?php _e('Name', 'gum'); ?> *</label>
                            <?php GumUserForm::name_text(); ?>
                        </div>

                        <div class="col-xs-12 col-md-6 col-lg-6 form-group">
                            <label for="s_email"><?php _e('Email address', 'gum'); ?> *</label>
                            <?php GumUserForm::email_text(); ?>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-6 form-group">
                            <label for="s_email_2"><?php _e('Confirm email address', 'gum'); ?> *</label>
                            <?php GumUserForm::check_email_text(); ?>
                        </div>

                        <div class="col-xs-12 col-md-6 col-lg-6 form-group">
                            <label for="s_password"><?php _e('Password', 'gum'); ?> *</label>
                            <?php GumUserForm::password_text(); ?>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-6 form-group">
                            <label for="s_password_2"><?php _e('Repeat password', 'gum'); ?> *</label>
                            <?php GumUserForm::check_password_text(); ?>
                        </div>

                        <div class="col-xs-12 col-md-6 col-lg-6 form-group">
                            <label for="s_phone_mobile"><?php _e('Telephone Number', 'gum'); ?></label>
                            <?php GumUserForm::mobile_text(); ?>
                        </div>

                        <div class="col-xs-12 form-group">
                            <?php osc_run_hook('user_register_form'); ?>
                        </div>
                        <div class="clearfix"></div>
                        <?php if (osc_recaptcha_public_key()) { ?>
                        <div class="form-group">
                            <?php osc_show_recaptcha('register'); ?>
                        </div>
                        <?php } ?>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <button type="submit" class="btn btn-primary-custom pull-left"><?php _e("Create account", 'gum'); ?></button>
                            </div>
                            <div class="col-xs-6">
                                <a href="<?php echo osc_user_login_url(); ?>" class="btn btn-secondary-custom pull-right"><?php _e("Cancel", 'gum'); ?></a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr-custom">
                        <div class="col-xs-12">
                            <p class="text-info"><?php _e('By creating an account or using a social login, you are agreeing to our website Terms & Conditions and Privacy Policy.', 'gum'); ?></p>
                        </div>
                    </form>
                    <?php $class = '';
                    if(function_exists('fbc_button') ) { $class= 'col-sm-8 register-social'; ?>
                    <div class="col-xs-12 visible-xs-block visible-ms-block visible-sm-block">
                        <span class="rule"><span class="rule-text"><?php _e('Or', 'gum'); ?></span></span>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <p class="visible-sm-block visible-md-block visible-lg-block underline"><label><?php _e('Or login', 'gum'); ?></label></p>
                        <p><a class="btn btn-primary-custom btn-block"><i class="fa fa-facebook text-left"></i> <?php _e('Login with facebook', 'gum'); ?></a></p>
                        <div class="col-xs-12 box-border notice">
                            <h2 class="lead"><?php _e('Get the most from your account!', 'gum'); ?></h2>
                            <p class="with-icon-left"><?php _e('<span><i class="fa fa-folder-open-o"></i> With your account, you can manage all your ads in one place.</span>', 'gum'); ?></p>
                            <p class="with-icon-left"><?php _e('<i class="fa fa-pencil"></i> <span>Posting ads is faster and easier with pre-filled contact information.</span>', 'gum'); ?></p>
                            <p class="with-icon-left"><?php _e('<i class="fa fa-bar-chart-o"></i> <span>Ad stats show you how many people have viewed and replied to your ads.</span>', 'gum'); ?></p>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php GumUserForm::js_validation(); ?>
<?php osc_current_web_theme_path('footer.php') ; ?>