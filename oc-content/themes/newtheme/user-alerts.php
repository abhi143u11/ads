<?php
    // meta tag robots
    osc_add_hook('header','gum_nofollow_construct');

    osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php') ;
    View::newInstance()->_exportVariableToView('user', User::newInstance()->findByPrimaryKey(osc_logged_user_id()));

?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12 custom-tabs">
                <?php osc_current_web_theme_path('user-sidebar.php'); ?>
                <div class="col-xs-12 col-md-8 col-lg-9">
                    <h1 class="h1-custom"><?php _e('My alerts', 'gum'); ?></h1>
                    <?php if(osc_count_alerts() == 0) { ?>
                        <p class="empty" ><?php printf(__('Hi %s, you currently have 0 alerts', 'gum'), osc_user_name()); ?></p>
                        <div class="text-center  notice">
                            <p class="lead"><?php _e('Want to save some searches?', 'gum'); ?></p>
                            <p><?php _e('Simply select \'Save this search\' on a search results page.', 'gum'); ?></p>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-info col-xs-12"><?php printf(__('Hi %s, you currently have %d alerts', 'gum'), osc_user_name(), osc_count_alerts()); ?></div>
                        <div class="clearfix"></div>
                    <?php $i = 0; while (osc_has_alerts() ) {
                        gum_draw_alert($i);
                        $i++;
                    } ?>
                    <div class="col-sm-12">
                        <span class="">
                            * <?php _e('You will only receive an alert if we find new listings that match your search', 'gum'); ?>
                        </span>
                    </div>

                    <?php
                    if(osc_rewrite_enabled()){
                        $footerLinks = osc_search_footer_links();
                    ?>
                    <?php } ?>
                <?php } ?>
                </div>
            </div>
            <div class="col-xs-12">
            </div>
        </div>
    </div>
</div>
<?php UserForm::js_validation(); ?>
<?php osc_current_web_theme_path('footer.php') ; ?>