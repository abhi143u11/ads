CREATE TABLE `/*TABLE_PREFIX*/t_item_phone_reveal` (
  `fk_i_item_id` int(10) unsigned NOT NULL,
  `i_views` int(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`fk_i_item_id`),
  KEY `i_views` (`i_views`),
  CONSTRAINT `/*TABLE_PREFIX*/t_item_phone_reveal_ibfk_1` FOREIGN KEY (`fk_i_item_id`) REFERENCES `/*TABLE_PREFIX*/t_item` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `/*TABLE_PREFIX*/t_item_phone_reveal_detail` (
  `pk_i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_i_item_id` int(10) unsigned NOT NULL,
  `dt_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`pk_i_id`),
  KEY `item_date` (`fk_i_item_id`,`dt_date`),
  CONSTRAINT `/*TABLE_PREFIX*/t_item_phone_reveal_detail_ibfk1` FOREIGN KEY (`fk_i_item_id`) REFERENCES `/*TABLE_PREFIX*/t_item` (`pk_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;