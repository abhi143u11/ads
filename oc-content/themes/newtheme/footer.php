<?php
$flocation_active = '';
$fsearch_active = '';
if(osc_get_preference('footer_seo_links', 'gum_theme')  && ( osc_is_home_page() || osc_is_search_page() || osc_is_404()) ){
    $fsearch_active = 'active';
    if(osc_get_preference('footer_seo_links_topsearches', 'gum_theme') && osc_get_preference('footer_seo_links_toplocations', 'gum_theme')) {
        $fsearch_active = 'active';
    } else if(osc_get_preference('footer_seo_links_toplocations', 'gum_theme')) {
        $fsearch_active = '';
        $flocation_active = 'active';
    }
?>
    <div class="container-fluid main-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center seo-links-box custom-tabs">
                    <ul id="topLinks" class="nav nav-tabs">
                        <?php if(osc_get_preference('footer_seo_links_topsearches', 'gum_theme')) { ?>
                        <li role="presentation" class="<?php echo $fsearch_active; ?>"><a href="#top-searches"><i class="fa fa-search"></i> <?php _e('Top searches', 'gum'); ?></a></li>
                        <?php } ?>
                        <?php if(osc_get_preference('footer_seo_links_toplocations', 'gum_theme')) { ?>
                        <li role="presentation" class="<?php echo $flocation_active; ?>"><a href="#top-location"><i class="fa fa-map-marker"></i> <?php _e('Top locations', 'gum'); ?></a></li>
                        <?php }?>
                      </ul>
                    <div class="tab-content" >
                        <?php if(osc_get_preference('footer_seo_links_topsearches', 'gum_theme')) { ?>
                        <div role="tabpanel" class="tab-pane <?php echo $fsearch_active; ?>" id="top-searches">
                            <?php if(osc_save_latest_searches()=='1' ) : ?>
                            <div class="row">
                                <ol class="col-md-3 col-sm-4 col-xs-6">
                                    <?php $i = 1;
                                    osc_get_latest_searches(12);
                                    while(osc_has_latest_searches()) : ?>
                                    <li  class="col-lg-12 text-left border-left"><a class="truncate-line" href="<?php echo osc_search_url(array('sPattern' => osc_latest_search_text())); ?>"><?php echo osc_latest_search_text(); ?></a></li>
                                    <?php if($i>0&&$i%3==0) : ?>
                                            </ol>
                                            <ol class="col-md-3 col-sm-4 col-xs-6" start="<?php echo $i+1; ?>">
                                    <?php endif;
                                        if($i>0&&$i%12==0) { break; }
                                        $i++;
                                    endwhile; ?>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                            <?php endif; ?>
                        </div>
                        <?php } ?>
                        <?php if(osc_get_preference('footer_seo_links_toplocations', 'gum_theme')) { ?>
                        <div role="tabpanel" class="tab-pane <?php echo $flocation_active; ?>" id="top-location">
                            <div class="row">
                                <ol class="col-md-3 col-sm-4 col-xs-6">
                                    <?php  // @todo improve with custom function and use LIMIT on query
                                    // seo links Region links
                                    $i = 1;
                                    if(osc_get_preference('footer_seo_links_toplocations_type','gum_theme')=='region') {
                                    $_regions = Search::newInstance()->listRegions('%%%%', ">", "region_name ASC" );
                                    View::newInstance()->_exportVariableToView('regions', $_regions);
                                    while(osc_has_regions()) : ?>
                                        <li  class="col-lg-12 text-left border-left"><a class="truncate-line" href="<?php echo osc_region_url(); ?>"><?php echo osc_region_name(); ?></a></li>

                                    <?php if($i>0&&$i%3==0) : ?>
                                            </ol>
                                            <ol class="col-md-3 col-sm-4 col-xs-6" start="<?php echo $i+1; ?>">
                                    <?php endif;
                                        if($i>0&&$i%12==0) { break; }
                                        $i++;
                                    endwhile;
                                    }?>

                                    <?php  // @todo improve with custom function and use LIMIT on query
                                    // seo links City links
                                    $i = 1;
                                    if(osc_get_preference('footer_seo_links_toplocations_type', 'gum_theme')=='city') {
                                    $_cities = Search::newInstance()->listCities('%%%%', ">", "city_name ASC" );
                                    View::newInstance()->_exportVariableToView('cities', $_cities);
                                    while(osc_has_cities()) : ?>
                                        <li  class="col-lg-12 text-left border-left"><a class="truncate-line" href="<?php echo osc_city_url(); ?>"><?php echo osc_city_name(); ?></a></li>

                                    <?php if($i>0&&$i%3==0) : ?>
                                            </ol>
                                            <ol class="col-md-3 col-sm-4 col-xs-6" start="<?php echo $i+1; ?>">
                                    <?php endif;
                                        if($i>0&&$i>=12) { break; }
                                        $i++;
                                    endwhile;
                                    }?>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="container-fluid footer">
        <div class="row">
            <div class="container">
                <hr>
                <footer>
                    <?php gum_print_social_links(); ?>
                    <ul class="list-inline links">
                        <?php
                        osc_reset_static_pages();
                        while( osc_has_static_pages() ) { ?>
                            <li>
                                <a href="<?php echo osc_static_page_url(); ?>"><?php echo osc_static_page_title(); ?></a>
                            </li>
                        <?php
                        }
                        ?>
                        <li>
                            <a href="<?php echo osc_contact_url(); ?>"><?php _e('Contact', 'gum'); ?></a>
                        </li>
                    </ul>
                    <?php
                    if( osc_get_preference('footer_link', 'gum_theme') !== '0') {
                        echo '<p class="pull-left">' . sprintf(__('This website is proudly using the <a title="Osclass web" href="%s">classifieds scripts</a> software <strong>Osclass</strong>'), 'http://osclass.org/') . '</p>';
                    }
                    ?>
                    <p class="pull-right"><?php echo osc_get_preference('footer_copyright', 'gum_theme'); ?></p>
                </footer>
            </div> <!-- /container -->
        </div>
    </div> <!-- /container -->
    </div><!-- /filters-mask -->
    <script>
        $('#topLinks a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
    </script>
    <?php osc_run_hook('footer'); ?>
  </body>
</html>