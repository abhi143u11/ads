<?php
// meta tag robots
osc_add_hook('header','gum_follow_construct');
osc_current_web_theme_path('header.php') ;

?>
<div class="container-fluid main">
    <!-- Example row of columns -->
    <div class="container">
        <div class="row">
            <h1><?php echo osc_static_page_title(); ?></h1>
            <?php echo osc_static_page_text(); ?>
        </div>
    </div>
</div>
<?php osc_current_web_theme_path('footer.php') ; ?>