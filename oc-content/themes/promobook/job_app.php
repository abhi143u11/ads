<?php
    /*
     *      Osclass � software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // meta tag robots
    osc_add_hook('header','osclasswizards_nofollow_construct');

    osclasswizards_add_body_class('contact');
    
    osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php');
?>

<form class="form-horizontal">
<fieldset>

<!-- Form Name -->


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="name">Name</label>  
  <div class="col-md-4">
  <input id="name" name="name" type="text" placeholder="Enter Name" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="surname">Surname</label>  
  <div class="col-md-4">
  <input id="surname" name="surname" type="text" placeholder="Enter Surname" class="form-control input-md">
    
  </div>
</div>


<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="gender">Sex</label>
  <div class="col-md-4">
    <select id="gender" name="gender" class="form-control">
      <option value="male">Male</option>
      <option value="female">Female</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Select Basic</label>
  <div class="col-md-4">
    <select id="selectbasic" name="selectbasic" class="form-control">
      <option value="1">Option one</option>
      <option value="2">Option two</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="idcard">Identity Card </label>  
  <div class="col-md-4">
  <input id="idcard" name="idcard" type="text" placeholder="numeric only 13 spaces" class="form-control input-md">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="age">Age</label>
  <div class="col-md-4">
    <select id="age" name="age" class="form-control">
      <option value="18">18</option>
      <option value="19">19</option>
      <option value="20">20</option>
      <option value="21">21</option>
      <option value="22">22</option>
      <option value="23">23</option>
      <option value="24">24</option>
      <option value="25">25</option>
      <option value="26">26</option>
      <option value="27">27</option>
      <option value="28">28</option>
      <option value="29">29</option>
      <option value="30">30</option>
      <option value="31">31</option>
      <option value="32">32</option>
      <option value="33">33</option>
      <option value="34">34</option>
      <option value="35">35</option>
      <option value="36">36</option>
      <option value="37">37</option>
      <option value="38">38</option>
      <option value="39">39</option>
      <option value="40">40</option>
      <option value="41">41</option>
      <option value="42">42</option>
      <option value="43">43</option>
      <option value="44">44</option>
      <option value="45">45</option>
      <option value="46">46</option>
      <option value="47">47</option>
      <option value="48">48</option>
      <option value="49">49</option>
      <option value="50">50</option>
      <option value="51">51</option>
      <option value="52">52</option>
      <option value="53">53</option>
      <option value="54">54</option>
      <option value="55">55</option>
      <option value="56">56</option>
      <option value="57">57</option>
      <option value="58">58</option>
      <option value="59">59</option>
      <option value="60">60</option>
      <option value="61">61</option>
      <option value="62">62</option>
      <option value="63">63</option>
      <option value="64">64</option>
      <option value="65">65</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="visa"> Visa American</label>
  <div class="col-md-4">
    <select id="visa" name="visa" class="form-control">
      <option value="y">Yes</option>
      <option value="n">No</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="status">Marital Status</label>
  <div class="col-md-4">
    <select id="status" name="status" class="form-control">
      <option value="single">single (a)</option>
      <option value="married">married (a)</option>
      <option value="union_libre">Union Libre</option>
      <option value="divorced">Divorced (a)</option>
      <option value="widow">Widow (er)</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="hometown">Hometown:</label>
  <div class="col-md-4">
    <select id="hometown" name="hometown" class="form-control">
      <option value="The Ceiba">The Ceiba</option>
      <option value="San Pedro Sula">San Pedro Sula</option>
      <option value="Choloma">Choloma</option>
      <option value="Puerto Cortes">Puerto Cortes</option>
      <option value="La Lima">La Lima</option>
      <option value="Villanueva">Villanueva</option>
      <option value="Tegucigalpa">Tegucigalpa</option>
      <option value="El Progreso">El Progreso</option>
      <option value="Abroad">Abroad</option>
      <option value="Other">Other</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="area">Area or Cologne:</label>  
  <div class="col-md-4">
  <input id="area" name="area" type="text" placeholder="Area or Cologne" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="phone">Phone</label>  
  <div class="col-md-4">
  <input id="phone" name="phone" type="text" placeholder="8 numeric only spaces" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="mobile"> Mobile</label>  
  <div class="col-md-4">
  <input id="mobile" name="mobile" type="text" placeholder="8 numeric space only" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email"> Email</label>  
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="enter email" class="form-control input-md">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="currentjob">current Job</label>
  <div class="col-md-4">
    <select id="currentjob" name="currentjob" class="form-control">
      <option value="Primary (Sixth Grade)">Primary (Sixth Grade)</option>
      <option value="basic common cycle">basic common cycle</option>
      <option value="trade expert and chartered accountant">trade expert and chartered accountant</option>
      <option value="business secretary">business secretary</option>
      <option value="bilingual secretary">bilingual secretary</option>
      <option value="bach in arts and sciences Agronomo Bach">bach in arts and sciences Agronomo Bach</option>
      <option value="Bach Tecnico Forestal">Bach Tecnico Forestal</option>
      <option value="social promotion Bach">social promotion Bach</option>
      <option value="Bach.... nursing">Bach.... nursing</option>
      <option value="Bach. Hospitality and tourism Bach. Deliniacion Industrial Bach. business Administration Bach. Marketing Bach. customs formalities">Bach. Hospitality and tourism Bach. Deliniacion Industrial Bach. business Administration Bach. Marketing Bach. customs formalities</option>
      <option value="bach. quality control and prudccion">bach. quality control and prudccion</option>
      <option value="bach. Automotive mechanic technician">bach. Automotive mechanic technician</option>
      <option value="bach. Technical metallic structures">bach. Technical metallic structures</option>
      <option value="bach. Technical machines and tools">bach. Technical machines and tools</option>
      <option value="electrician bach">electrician bach</option>
      <option value="bach. electornica coach bach. Computer technician">bach. electornica coach bach. Computer technician</option>
      <option value="Lic. Business Administration Lic. Marketing Lic. Graphic design">Lic. Business Administration Lic. Marketing Lic. Graphic design</option>
      <option value="Lic. Communication and Advertising">Lic. Communication and Advertising</option>
      <option value="Lic. Accounting issues">Lic. Accounting issues</option>
      <option value="Lic. Architecture">Lic. Architecture</option>
      <option value="Lic. Business management">Lic. Business management</option>
      <option value="Lic. International trade">Lic. International trade</option>
      <option value="Lic. Finance Lic . Economy">Lic. Finance Lic . Economy</option>
      <option value="Lic. in customs administration">Lic. in customs administration</option>
      <option value="Lic. Legal Sciences">Lic. Legal Sciences</option>
      <option value="lic. Tourism Business Administration">lic. Tourism Business Administration</option>
      <option value="lic. Hospitality and tourism">lic. Hospitality and tourism</option>
      <option value="lic. Ecotourism">lic. Ecotourism</option>
      <option value="lic. Sociology">lic. Sociology</option>
      <option value="lic. Psychology">lic. Psychology</option>
      <option value="lic. Nursing">lic. Nursing</option>
      <option value="lic. PR">lic. PR</option>
      <option value="lic. Pedagogy and education sciences">lic. Pedagogy and education sciences</option>
      <option value="business education lic">business education lic</option>
      <option value="lic. Systems">lic. Systems</option>
      <option value="lic administrative computing">lic administrative computing</option>
      <option value="dr. Chemicals and pharmaceuticals">dr. Chemicals and pharmaceuticals</option>
      <option value="dr. Microbiology">dr. Microbiology</option>
      <option value="dr. dental surgeon">dr. dental surgeon</option>
      <option value="technician optometrist">technician optometrist</option>
      <option value="ing. Computers">ing. Computers</option>
      <option value="Ing. Sistemas">Ing. Sistemas</option>
      <option value="ing. Industrial">ing. Industrial</option>
      <option value="ing in business">ing in business</option>
      <option value="ing. Commercial">ing. Commercial</option>
      <option value="ing. Financial">ing. Financial</option>
      <option value="ing. industrial relations, ing. Civil">ing. industrial relations, ing. Civil</option>
      <option value="ing. Agronomo">ing. Agronomo</option>
      <option value="ing. Forestry">ing. Forestry</option>
      <option value="agro-food ing">agro-food ing</option>
      <option value="ing Industrial electrician ing mechatronics">ing Industrial electrician ing mechatronics</option>
      <option value="electronics ing">electronics ing</option>
      <option value="ing Industrial mechanic">ing Industrial mechanic</option>
      <option value="ing telecommunications technician Industrial mechanic technician in graphic design electronic technician and electricity">ing telecommunications technician Industrial mechanic technician in graphic design electronic technician and electricity</option>
      <option value="refrigeration technician mechanical automotive industrial mechanic">refrigeration technician mechanical automotive industrial mechanic</option>
      <option value="biker">biker</option>
      <option value="cook">cook</option>
      <option value="welder">welder</option>
      <option value="designer">designer</option>
      <option value="carpenter">carpenter</option>
      <option value="musician">musician</option>
      <option value="carpenter">carpenter</option>
      <option value="gardener">gardener</option>
      <option value="bartender">bartender</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="studying">currently studying</label>  
  <div class="col-md-4">
  <input id="studying" name="studying" type="text" placeholder="currently studying" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="master">Maestrias</label>  
  <div class="col-md-4">
  <input id="master" name="master" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="eng">English</label>
  <div class="col-md-4">
    <select id="eng" name="eng" class="form-control">
      <option value="basic">basic</option>
      <option value="intermediate">intermediate</option>
      <option value="advanced">advanced</option>
      <option value="technical">technical</option>
      <option value="none">none</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="computer">Computer</label>
  <div class="col-md-4">
    <select id="computer" name="computer" class="form-control">
      <option value="None">None</option>
      <option value="Basic">Basic</option>
      <option value="Intermediate">Intermediate</option>
      <option value="Advanced">Advanced</option>
      <option value="Expert">Expert</option>
    </select>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="experience">Experience</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="experience" name="experience"></textarea>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="course">Courses training</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="course" name="course"></textarea>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="availalability"> Availability to work in another city ?</label>
  <div class="col-md-4">
    <select id="availalability" name="availalability" class="form-control">
      <option value="yes">yes</option>
      <option value="no">no</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="situation">Employment Situation </label>
  <div class="col-md-4">
    <select id="situation" name="situation" class="form-control">
      <option value="with work">with work</option>
      <option value="jobless">jobless</option>
      <option value="temporary work">temporary work</option>
      <option value="looking for my first job">looking for my first job</option>
      <option value="student">student</option>
      <option value="business itself">business itself</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="wantapply">People who want to apply to</label>
  <div class="col-md-4">
    <select id="wantapply" name="wantapply" class="form-control">
      <option value="Marketing Division | Sales Finance | accounting | auditing Banking | financial services">Marketing Division | Sales Finance | accounting | auditing Banking | financial services</option>
      <option value="Computers | Internet operations | logistics">Computers | Internet operations | logistics</option>
      <option value="production | engineering | quality management">production | engineering | quality management</option>
      <option value="several">several</option>
      <option value="human resources">human resources</option>
      <option value="maintenance">maintenance</option>
      <option value="office support">office support</option>
      <option value="professional ports">professional ports</option>
      <option value="call center">call center</option>
      <option value="restaurants">restaurants</option>
      <option value="storage">storage</option>
      <option value="advertising | communications | services">advertising | communications | services</option>
      <option value="shopping">shopping</option>
      <option value="health">health</option>
      <option value="telecommunications">telecommunications</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="wage">Wage Pretension</label>
  <div class="col-md-4">
    <select id="wage" name="wage" class="form-control">
      <option value="$ 400 - $ 800">$ 400 - $ 800</option>
      <option value="$ 801 - $ 1200">$ 801 - $ 1200</option>
      <option value="$ 1201 - $ 1600 $ 1601 - $ 2000 $ 2001 - $ 2400">$ 1201 - $ 1600 $ 1601 - $ 2000 $ 2001 - $ 2400</option>
      <option value="$ 2401 - $ 2800">$ 2401 - $ 2800</option>
      <option value="$ 3000 +">$ 3000 +</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="driving">Driving License</label>
  <div class="col-md-4">
    <select id="driving" name="driving" class="form-control">
      <option value="Lightweight">Lightweight</option>
      <option value="heavy">heavy</option>
      <option value="Articulated">Articulated</option>
      <option value="light and heavy">light and heavy</option>
      <option value="Moto">Moto</option>
      <option value="Auto and Moto">Auto and Moto</option>
      <option value="None">None</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="car">Car</label>
  <div class="col-md-4">
    <select id="car" name="car" class="form-control">
      <option value="Tourism">Tourism</option>
      <option value="Pick up">Pick up</option>
      <option value="Truck">Truck</option>
      <option value="Motorcycle">Motorcycle</option>
      <option value="none">none</option>
    </select>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="reference">Personal and business references</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="reference" name="reference"></textarea>
  </div>
</div>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="attach">Attach Photographs</label>
  <div class="col-md-4">
    <input id="attach" name="attach" class="input-file" type="file">
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="applyjob">Apply for job</label>
  <div class="col-md-4">
    <button id="applyjob" name="applyjob" class="btn btn-primary">Apply</button>
  </div>
</div>


</fieldset>
</form>
<?php osc_current_web_theme_path('footer.php') ; ?>