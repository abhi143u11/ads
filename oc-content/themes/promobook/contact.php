<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */

    // meta tag robots
    osc_add_hook('header','osclasswizards_nofollow_construct');

    osclasswizards_add_body_class('contact');
	
    osc_enqueue_script('jquery-validate');
    osc_current_web_theme_path('header.php');
?>

<div class="row">


  <div class="col-sm-7" >
    <div class="wraps">
      <div class="title">
        <h1>
          <?php _e('Contact us', OSCLASSWIZARDS_THEME_FOLDER); ?>
        </h1>
      </div>
      <div class="resp-wrapper">
        <ul id="error_list">
        </ul>
         
        <form name="contact_form" action="<?php echo osc_base_url(true); ?>" method="post" style="background-color:#F5F5F5;" >
      
          <input type="hidden" name="page" value="contact" />
          <input type="hidden" name="action" value="contact_post" />
          <div class="form-group">
            <label class="control-label" for="yourName">
              <?php _e('Your name', OSCLASSWIZARDS_THEME_FOLDER); ?>
              (
              <?php _e('optional', OSCLASSWIZARDS_THEME_FOLDER); ?>
              )</label>
            <div class="controls">
              <?php ContactForm::your_name(); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label" for="yourEmail">
              <?php _e('Your email address', OSCLASSWIZARDS_THEME_FOLDER); ?>
            </label>
            <div class="controls">
              <?php ContactForm::your_email(); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label" for="subject">
              <?php _e('Subject', OSCLASSWIZARDS_THEME_FOLDER); ?>
              (
              <?php _e('optional', OSCLASSWIZARDS_THEME_FOLDER); ?>
              )</label>
            <div class="controls">
              <?php ContactForm::the_subject(); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label" for="message">
              <?php _e('Message', OSCLASSWIZARDS_THEME_FOLDER); ?>
            </label>
            <div class="controls textarea">
              <?php ContactForm::your_message(); ?>
            </div>
          </div>
		   <?php if( osc_recaptcha_items_enabled() ) { ?>
		  <div class="form-group">
            <div class="recap">
			<?php osc_show_recaptcha(); ?>
			</div>
		 </div>
		   <?php } ?>
          <div class="form-group">
            <div class="controls">
              <?php osc_run_hook('contact_form'); ?>
              
              <button type="submit" class="btn btn-success">
              <?php _e("Send", OSCLASSWIZARDS_THEME_FOLDER);?>
              </button>
              <?php osc_run_hook('admin_contact_form'); ?>
            </div>
          </div>
          
        </form>
       
        <?php ContactForm::js_validation(); ?>
      </div>
    </div>
  </div>
  
  <div class="col-md-5">
  <div class="title"> 
  <h1>Address</h1></div>
  <p><b>DrizzleDesigns</b><br>
75, Big Road - Street Name<br>
City 87652<br>
United States
</p>
 <div class="google-map">
                       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951027.7547869263!2d-88.48715460817903!3d14.745039148281712!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f6a751a73b731cf%3A0x7ed1de82b6fb8264!2sHonduras!5e0!3m2!1sen!2sin!4v1455789190167" width="425" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>

                    </div>
               

</div>
</div>
<?php osc_current_web_theme_path('footer.php') ; ?>
