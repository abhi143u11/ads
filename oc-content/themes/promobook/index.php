<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */
	 
/*
Theme Name: promobook
Theme URI: http://www.osclasswizards.com
Description: A simple and clean responsive theme for your classified site.
Version: 2.0.3
Author: promobook
Author URI: http://www.osclasswizards.com
Widgets:  header, footer
Theme update URI: osclasswizards-free-responsive-theme
*/
    function osclasswizards_theme_info() {
        return array(
             'name'        => 'promobook'
            ,'version'     => '1.0.1'
            ,'description' => 'A Design for promobook.com'
            ,'author_name' => 'Digital Sense'
            ,'author_url'  => 'http://www.digitalsense.co.in'
            ,'locations'   => array('header', 'footer')
        );
    }
?>