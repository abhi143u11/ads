<?php
    /*
     *      Osclass – software for creating and publishing online classified
     *                           advertising platforms
     *
     *                        Copyright (C) 2014 OSCLASS
     *
     *       This program is free software: you can redistribute it and/or
     *     modify it under the terms of the GNU Affero General Public License
     *     as published by the Free Software Foundation, either version 3 of
     *            the License, or (at your option) any later version.
     *
     *     This program is distributed in the hope that it will be useful, but
     *         WITHOUT ANY WARRANTY; without even the implied warranty of
     *        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *             GNU Affero General Public License for more details.
     *
     *      You should have received a copy of the GNU Affero General Public
     * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
     */
?>
<style type="text/css">

.corner-ribbon{
  width: 200px;
  background: #e43;
  position: absolute;
  top: 25px;
  left: -50px;
  text-align: center;
  line-height: 25px;
  letter-spacing: 1px;
  color: #6D6D6D;
  transform: rotate(-45deg);
  -webkit-transform: rotate(-45deg);
}

/* Custom styles */

.corner-ribbon.sticky{
  position: absolute;
}

.corner-ribbon.shadow{
  box-shadow: 0 0 3px rgba(0,0,0,.3);
}
.corner-ribbon.top-right{
  top: 25px;
  right: -50px;
  left: auto;
  transform: rotate(45deg);
  -webkit-transform: rotate(45deg);
}
.corner-ribbon.blue{background: #FBFBFB; color:#392420;}

</style>
<?php
$type = 'items';
if(View::newInstance()->_exists('listType')){
    $type = View::newInstance()->_get('listType');
}
$cols = 3;
?>
<div class="listing-card-list listings_grid listings_grids animated  jello" id="listing-card-list">
  <?php
	
	//latest items
	if($type == 'latestItems'){
		$listcount = 1;
		echo '<ul class="row">';
  		while ( osc_has_latest_items() ) {
			
?>
  <?php $size = explode('x', osc_thumbnail_dimensions()); ?>
  <li class="listing-card col-sm-6 col-xs-6 col-md-<?php echo $cols;?> <?php if(osc_item_is_premium()){ echo ' premium'; } ?>">
    <div class="figure">
      <figure>
        <?php if( osc_images_enabled_at_items() ) { ?>
        <?php if(osc_count_item_resources()) { ?>
        <a class="listing-thumb" href="<?php echo osc_item_url() ; ?>" title="<?php echo osc_esc_html(osc_item_title()) ; ?>"><img src="<?php echo osc_resource_thumbnail_url(); ?>" title="" alt="<?php echo osc_esc_html(osc_item_title()) ; ?>" width="<?php echo $size[0]; ?>" height="<?php echo $size[1]; ?>" class="img-responsive"></a>
        <?php } else { ?>
        <a class="listing-thumb" href="<?php echo osc_item_url() ; ?>" title="<?php echo osc_esc_html(osc_item_title()) ; ?>"><img src="<?php echo osc_current_web_theme_url('images/no_photo.gif'); ?>" title="" alt="<?php echo osc_esc_html(osc_item_title()) ; ?>" width="<?php echo $size[0]; ?>" height="<?php echo $size[1]; ?>" class="img-responsive"></a>
        <?php } ?>
        <?php } ?>
        <?php $custom_fields = array();


                                    if( osc_count_item_meta() >= 1 ) { 

                                        while ( osc_has_item_meta() ) { 
                                            if(osc_item_meta_value()!='') {

                                                $custom_fields[osc_item_meta_name()] = osc_item_meta_value();
                                                if(osc_item_meta_name()=="discount")
                                                {
                                                ?>
                                                <div class="corner-ribbon top-right sticky blue shadow" style="z-index: 10;">
                                                Discount <?php echo osc_item_meta_value();?>%</div>
                                                <?php
                                                }   

                                            }
                                        }
                                } ?> 
            <?php countdown_splash(); ?>
        </figure>
    </div>
    <div class="listing-attr">
      <h4><a href="<?php echo osc_item_url() ; ?>" title="<?php echo osc_esc_html(osc_item_title()) ; ?>"><?php echo osc_highlight(strip_tags(osc_item_title()),40) ; ?></a></h4>
      <article> <span class="category"><i class="fa fa-<?php echo osclasswizards_category_icon( osc_item_category_id() ); ?>"></i> <?php echo osc_item_category() ; ?></span> <span class="location"> <i class="fa fa-map-marker"></i> <?php echo osc_item_city(); ?>
        <?php if( osc_item_region()!='' ) { ?>
        (<?php echo osc_item_region(); ?>)
        <?php } ?>
        </span> <span class="date"> <i class="fa fa-clock-o"></i> <?php echo osc_format_date(osc_item_pub_date()); ?> </span><br/> By <a href="<?php echo osc_user_public_profile_url(osc_item_user_id()); ?>" target="_blank"><?php
        echo osc_item_contact_name();
    ?></a>  </article>
      <?php if( osc_price_enabled_at_items() ) { ?>
      <!-- <?php echo osc_format_price(osc_item_price()); ?>-->
      
      <?php } ?>
      <?php $admin = false; ?>
      <?php if($admin){ ?>
      <span class="admin-options"> <a href="<?php echo osc_item_edit_url(); ?>" rel="nofollow">
      <?php _e('Edit item', OSCLASSWIZARDS_THEME_FOLDER); ?>
      </a> <span>|</span> <a class="delete" onclick="javascript:return confirm('<?php echo osc_esc_js(__('This action can not be undone. Are you sure you want to continue?', OSCLASSWIZARDS_THEME_FOLDER)); ?>')" href="<?php echo osc_item_delete_url();?>" >
      <?php _e('Delete', OSCLASSWIZARDS_THEME_FOLDER); ?>
      </a>
      <?php if(osc_item_is_inactive()) {?>
      <span>|</span> <a href="<?php echo osc_item_activate_url();?>" >
      <?php _e('Activate', OSCLASSWIZARDS_THEME_FOLDER); ?>
      </a>
      <?php } ?>
      </span>
      <?php } ?>
    </div>
   
    
  </li>
  <?php	
		if($listcount%4 == 0)
		{
			echo '</ul><ul class="row">';
		}
		$listcount++;
        }
		echo'</ul>';
    } 
?>
</div>
